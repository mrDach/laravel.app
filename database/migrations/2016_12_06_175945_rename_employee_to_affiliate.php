<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameEmployeeToAffiliate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function(Blueprint $table)
        {
            $table->renameColumn('employee_group_id', 'affiliate_status_id');
            $table->renameColumn('branch', 'branch_id');
            $table->renameColumn('department', 'department_id');
        });
        Schema::rename('employees', 'affiliates');

        Schema::table('employee_awards', function(Blueprint $table)
        {
            $table->renameColumn('employee_id', 'affiliate_id');
        });
        Schema::rename('employee_awards', 'affiliate_awards');

        Schema::rename('employee_groups', 'affiliate_groups');

        Schema::rename('employee_status', 'affiliate_status');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Schema::table('affiliates', function(Blueprint $table)
        {
            $table->renameColumn('affiliate_status_id', 'employee_group_id');
            $table->renameColumn('branch_id', 'branch');
            $table->renameColumn('department_id', 'department');
        });
        Schema::rename('affiliates', 'employees');

        Schema::table('affiliate_awards', function(Blueprint $table)
        {
            $table->renameColumn('affiliate_id', 'employee_id');
        });
        Schema::rename('affiliate_awards', 'employee_awards');

        Schema::rename('affiliate_groups', 'employee_groups');

        Schema::rename('affiliate_status', 'employee_status');

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
