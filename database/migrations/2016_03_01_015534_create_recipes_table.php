<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('link_type')->index();
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->integer('media_id')->unsigned()->nullable()->index();
            $table->foreign('media_id')->references('id')->on('attachments')->onDelete('SET NULL');
            $table->string('name');
            $table->text('description');
            $table->string('slug')->index();
            $table->string('type')->index();
            $table->string('status')->index();
            $table->datetime('status_date')->index();
            $table->integer('calories')->unsigned()->index();
            $table->tinyInteger('gluten_free')->unsigned()->index();
            $table->string('gluten_free_notes');
            $table->integer('prep_time')->unsigned()->index();
            $table->integer('cook_time')->unsigned()->index();
            $table->integer('total_time')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('recipes');
    
        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}



