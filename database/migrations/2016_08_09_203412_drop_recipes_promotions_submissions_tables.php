<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropRecipesPromotionsSubmissionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //Drop Recipes
        $class = new CreateRecipesTable();
        $class->down();

        //Drop Recipe Ingredients
        $class = new CreateRecipeIngredientsTable();
        $class->down();

        //Drop Recipe Directions
        $class = new CreateRecipeDirectionsTable();
        $class->down();

        //Drop Recipes Serveware
        $class = new CreateRecipesServewareTable();
        $class->down();

        //Drop Recipes Sides
        $class = new CreateRecipesSidesTable();
        $class->down();
    
        //Drop RedFlag
        $class = new CreateRedflagTable();
        $class->down();

        //Drop Recipe Attachment
        $class = new CreateRecipeAttachmentTable();
        $class->down();

        //Drop Promotions
        $class = new CreatePromotionsTable();
        $class->down();

        //Drop Submissions
        $class = new CreateSubmissionsTable();
        $class->down();

        //Drop Submission Management Promotion
        $class = new CreateSubmissionManagementPromoTable();
        $class->down();

        //Drop Submission Customer Promo
        $class = new CreateSubmissionCustomerPromoTable();
        $class->down();

        //Drop Submission QSA
        $class = new CreateSubmissionQsaTable();
        $class->down();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        //Create Recipes
        $class = new CreateRecipesTable();
        $class->up();

        //Create Recipe Ingredients
        $class = new CreateRecipeIngredientsTable();
        $class->up();

        //Create Recipe Directions
        $class = new CreateRecipeDirectionsTable();
        $class->up();

        //Create Recipes Serveware
        $class = new CreateRecipesServewareTable();
        $class->up();

        //Create Recipes Sides
        $class = new CreateRecipesSidesTable();
        $class->up();
    
        //Create RedFlag
        $class = new CreateRedflagTable();
        $class->up();

        //Create Recipe Attachment
        $class = new CreateRecipeAttachmentTable();
        $class->up();

        //Create Promotions
        $class = new CreatePromotionsTable();
        $class->up();

        //Create Submissions
        $class = new CreateSubmissionsTable();
        $class->up();

        //Create Submission Management Promotion
        $class = new CreateSubmissionManagementPromoTable();
        $class->up();

        //Create Submission Customer Promo
        $class = new CreateSubmissionCustomerPromoTable();
        $class->up();

        //Create Submission QSA
        $class = new CreateSubmissionQsaTable();
        $class->up();
        
    }
}
