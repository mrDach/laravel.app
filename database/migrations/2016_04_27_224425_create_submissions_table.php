<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('logout_id')->unsigned()->index();
            $table->foreign('logout_id')->references('id')->on('logouts')->onDelete('cascade');
            $table->string('zone')->index();
            $table->decimal('total_sales', 8 , 2 )->unsigned()->nullable()->index();
            $table->decimal('food_sales', 8 , 2 )->unsigned()->nullable()->index();
            $table->decimal('foh', 5 , 2 )->unsigned()->nullable()->index();
            $table->decimal('boh', 5 , 2 )->unsigned()->nullable()->index();
            $table->text('flow')->nullable();
            $table->text('people')->nullable();
            $table->text('line_check')->nullable();
            $table->text('deletes')->nullable();
            $table->text('notes')->nullable();
            $table->text('incidents')->nullable();
            $table->text('fresh_outs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('submissions');
    
        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
