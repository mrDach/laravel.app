<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class CreateEmployeeGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
        });

        if( Role::find(1) ){

            //An Array of the Created Permissions
            $permissions = [

                //View Employee List Permission
                Permission::create([
                    'name'          => 'Employee Groups - List',
                    'slug'          => 'employee_groups',
                    'description'   => 'Permission to Access the Employee Groups List'
                ]),

                //Create Employee Permission
                Permission::create([
                    'name'          => 'Employee Groups - Create',
                    'slug'          => 'employee_groups.create',
                    'description'   => 'Permission to Create new Employee Groups'
                ]),

                //Delete Logout Permission
                Permission::create([
                    'name'          => 'Employee Groups - Delete',
                    'slug'          => 'employee_groups.delete',
                    'description'   => 'Permission to Delete Employee Groups'
                ])

            ];

            //Attach the Permissions to the Admin Role
            foreach( $permissions as $permission ){

                Role::find(1)->attachPermission( $permission );

            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('employee_groups');

        //Delete the Permissions
        Permission::whereIn( 'slug' , [ 'employee_groups' , 'employee_groups.create' , 'employee_groups.delete' ] )->delete();

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
