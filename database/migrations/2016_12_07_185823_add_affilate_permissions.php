<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class AddAffilatePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( Role::find(1) ){

            //An Array of the Created Permissions
            $permissions = [

                //View Affiliate List Permission
                Permission::create([
                    'name'          => 'Affiliate - List',
                    'slug'          => 'affiliate',
                    'description'   => 'Permission to Access the Affiliate List'
                ]),

                //Create Affiliate Permission
                Permission::create([
                    'name'          => 'Affiliate - Create',
                    'slug'          => 'affiliate.create',
                    'description'   => 'Permission to Create new Affiliate'
                ]),

                //Delete Affiliate Permission
                Permission::create([
                    'name'          => 'Affiliate - Delete',
                    'slug'          => 'affiliate.delete',
                    'description'   => 'Permission to Delete Affiliate'
                ]),

            ];

            //Attach the Permissions to the Admin Role
            foreach( $permissions as $permission ){

                Role::find(1)->attachPermission( $permission );

            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Delete the Permissions
        Permission::whereIn( 'slug' , [ 'affiliate' , 'affiliate.create' , 'affiliate.delete' ] )->delete();

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
