<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('address');
            $table->string('city');
            $table->string('province');
            $table->string('postalcode');
            $table->string('phone');
            $table->string('latitude');
            $table->string('longitude');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('stores');
    
        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
