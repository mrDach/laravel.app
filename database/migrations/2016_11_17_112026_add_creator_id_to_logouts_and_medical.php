<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatorIdToLogoutsAndMedical extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Update Logouts Table
        Schema::table('logouts', function (Blueprint $table) {

            //add column `creator_id` int(10) UNSIGNEDкуыуе not null
            $table->integer('creator_id')->unsigned()->index()->after('id');

        });

        //Update Medical Table
        Schema::table('medical', function (Blueprint $table) {

            //add column `creator_id` int(10) UNSIGNED not null
            $table->integer('creator_id')->unsigned()->index()->after('id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Update Logouts Table
        Schema::table('logouts', function (Blueprint $table) {

            //dell column `creator_id`
            $table->dropColumn('creator_id');

        });

        //Update Medical Table
        Schema::table('medical', function (Blueprint $table) {

            //dell column `creator_id`
            $table->dropColumn('creator_id');

        });
    }
}
