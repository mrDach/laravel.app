<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollSubmissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_submission', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('payroll_id')->unsigned()->nullable()->index();
            $table->foreign('payroll_id')->references('id')->on('payrolls')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('position_id')->unsigned()->nullable()->index();
            $table->foreign('position_id')->references('id')->on('positions')->onDelete('SET NULL');
            $table->date('date')->nullable()->index();
            $table->decimal('hours', 6 , 2)->unsigned()->nullable()->index();
            $table->decimal('rate', 6 , 2)->unsigned()->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('payroll_submission');
    
        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
