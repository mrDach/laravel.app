<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class CreateTechTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tech', function (Blueprint $table) {

            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('attachment_id')->unsigned()->index();
            $table->foreign('attachment_id')->references('id')->on('attachments')->onDelete('cascade');
            $table->string('name');
            $table->text('notes');
            $table->timestamps();

        });        

        if( Role::find(1) ){

            //An Array of the Created Permissions
            $permissions = [

                //View Logout List Permission
                Permission::create([
                    'name'          => 'Tech Talk - List',
                    'slug'          => 'tech',
                    'description'   => 'Permission to Access the Tech Talk List'
                ]),

                //Create Logout Permission
                Permission::create([
                    'name'          => 'Tech Talk - Create',
                    'slug'          => 'tech.create',
                    'description'   => 'Permission to Create new Tech Talk'
                ]),

                //Edit Logout Permission
                Permission::create([
                    'name'          => 'Tech Talk - Edit',
                    'slug'          => 'tech.edit',
                    'description'   => 'Permission to Edit Tech Talk'
                ]),

                //Delete Logout Permission
                Permission::create([
                    'name'          => 'Tech Talk - Delete',
                    'slug'          => 'tech.delete',
                    'description'   => 'Permission to Delete Tech Talk'
                ])

            ];

            //Attach the Permissions to the Admin Role
            foreach( $permissions as $permission ){

                Role::find(1)->attachPermission( $permission );

            }

        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('tech');

        //Delete the Permissions
        Permission::whereIn( 'slug' , [ 'tech' , 'tech.create' , 'tech.edit' , 'tech.delete' , 'tech.view' ] )->delete();
    
        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
