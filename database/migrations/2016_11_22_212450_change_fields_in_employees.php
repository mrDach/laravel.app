<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldsInEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function(Blueprint $table)
        {
            $table->renameColumn('payroll_status_id', 'employee_status_id');
            $table->integer('employee_group_id')->unsigned()->index();
            $table->string('payroll_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Schema::table('employees', function(Blueprint $table)
        {
            $table->renameColumn('employee_status_id', 'payroll_status_id');
            $table->dropColumn(['employee_group_id', 'payroll_status']);
        });

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
