<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class CreateAffiliateMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('affiliate_id')->unsigned()->index();
            $table->text('text');
            $table->timestamps();
        });

        if( Role::find(1) ){

            //An Array of the Created Permissions
            $permissions = [

                Permission::create([
                    'name'          => 'Affiliate Message - List',
                    'slug'          => 'affiliate.message',
                    'description'   => 'Permission to Access the Affiliate Messages List'
                ]),

                Permission::create([
                    'name'          => 'Affiliate Message - Create',
                    'slug'          => 'affiliate.message.create',
                    'description'   => 'Permission to Create the Affiliate Messages'
                ]),

            ];

            //Attach the Permissions to the Admin Role
            foreach( $permissions as $permission ){

                Role::find(1)->attachPermission( $permission );

            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('employees');

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
