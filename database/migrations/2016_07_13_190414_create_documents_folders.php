<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class CreateDocumentsFolders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //Create the Table
        Schema::create('document_folders', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('parent_id')->unsigned()->nullable()->index();
            $table->foreign('parent_id')->references('id')->on('document_folders')->onDelete('SET NULL');
            $table->integer('depth')->unsigned()->index();
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('description');
            $table->timestamps();
        });

        //An Array of the Created Permissions
        if( Role::find(1) ){

            $permissions = [

                //View All Folders Permission
                Permission::create([
                    'name'          => 'Documents - Folders - List',
                    'slug'          => 'folders',
                    'description'   => 'Permission to Access the Documents Folders List'
                ]),

                //Create Folder Permission
                Permission::create([
                    'name'          => 'Documents - Folders - Create',
                    'slug'          => 'folders.create',
                    'description'   => 'Permission to Create new Documents Folders'
                ]),

                //Edit Folder Permission
                Permission::create([
                    'name'          => 'Documents - Folders - Edit',
                    'slug'          => 'folders.edit',
                    'description'   => 'Permission to Edit Documents Folders'
                ]),

                //Delete Folder Permission
                Permission::create([
                    'name'          => 'Documents - Folders - Delete',
                    'slug'          => 'folders.delete',
                    'description'   => 'Permission to Delete Documents Folders'
                ])

            ];

            //Attach the Permissions to the Admin Role
            foreach( $permissions as $permission ){

                Role::find(1)->attachPermission( $permission );

            }

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('document_folders');

        //Delete the Permissions
        Permission::whereIn( 'slug' , [ 'folders' , 'folders.create' , 'folders.edit' , 'folders.delete' ] )->delete();
    
        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
