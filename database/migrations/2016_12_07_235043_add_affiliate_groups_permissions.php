<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class AddAffiliateGroupsPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( Role::find(1) ){

            //An Array of the Created Permissions
            $permissions = [

                //Add Affiliate Groups - List Permission
                Permission::create([
                    'name'          => 'Affiliate Groups - List',
                    'slug'          => 'affiliate.groups',
                    'description'   => 'Permission to Access the Affiliate Groups List'
                ]),

                //Add Affiliate GRoups - Create Permission
                Permission::create([
                    'name'          => 'Affiliate Groups - Create',
                    'slug'          => 'affiliate.groups.create',
                    'description'   => 'Permission to Create new Affiliate Groups'
                ]),

                //Add Award to Affiliate Permission
                Permission::create([
                    'name'          => 'Affiliate Groups - Delete',
                    'slug'          => 'affiliate.groups.delete',
                    'description'   => 'Permission to Delete Affiliate Groups'
                ]),

            ];

            //Attach the Permissions to the Admin Role
            foreach( $permissions as $permission ){

                Role::find(1)->attachPermission( $permission );

            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Delete the Permissions
        Permission::whereIn( 'slug' , [ 'affiliate.groups' , 'affiliate.groups.create' , 'affiliate.groups.delete' ] )->delete();

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
