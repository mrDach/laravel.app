<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class AddEmployeePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( Role::find(1) ){

            //An Array of the Created Permissions
            $permissions = [

                //View Employee List Permission
                Permission::create([
                    'name'          => 'Employee - List',
                    'slug'          => 'employee',
                    'description'   => 'Permission to Access the Employee List'
                ]),

                //Create Employee Permission
                Permission::create([
                    'name'          => 'Employee - Create',
                    'slug'          => 'employee.create',
                    'description'   => 'Permission to Create new Employee'
                ]),

                //Delete Logout Permission
                Permission::create([
                    'name'          => 'Employee - Delete',
                    'slug'          => 'employee.delete',
                    'description'   => 'Permission to Delete Employee'
                ])

            ];

            //Attach the Permissions to the Admin Role
            foreach( $permissions as $permission ){

                Role::find(1)->attachPermission( $permission );

            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Delete the Permissions
        Permission::whereIn( 'slug' , [ 'employee' , 'employee.create' , 'employee.delete' ] )->delete();

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
