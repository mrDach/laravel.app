<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBuyerslogoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Update Buyers Logout Table
        Schema::table('buyerslogouts', function (Blueprint $table) {

            $table->decimal('lymtd', 8 , 2 )->unsigned()->nullable()->index()->after('recap');
            $table->decimal('mtd', 8 , 2 )->unsigned()->nullable()->index()->after('lymtd');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Update the Buyers Logout Table
        Schema::table('buyerslogouts', function (Blueprint $table) {

            //Drop Columns
            $table->dropColumn('lymtd');
            $table->dropColumn('mtd');

        });
    }
}
