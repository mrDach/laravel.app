<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class CreateEmailRequestPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( Role::find(1) ){
            $permission = Permission::create([
                'name'          => 'New Email Request - Send',
                'slug'          => 'emailrequest',
                'description'   => 'Allow the user to request a new email address'
            ]);
            Role::find(1)->attachPermission( $permission );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Delete the Permissions
        Permission::whereIn( 'slug' , [ 'emailrequest' ] )->delete();

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
