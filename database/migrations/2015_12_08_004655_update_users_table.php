<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //Update Users Table
        Schema::table('users', function (Blueprint $table) {

            //Add the Username Column
            $table->string('username')->nullable()->after('name');

            //Add the Last Active Column
            $table->timestamp('active_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        //Drop the Last Active Column
        Schema::table('users', function (Blueprint $table) {

            //Remove the Username Column
            $table->dropColumn('username');

            //Remove the Last Active Column
            $table->dropColumn('active_at');

        });

    }
}
