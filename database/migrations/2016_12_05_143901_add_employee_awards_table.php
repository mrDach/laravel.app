<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeeAwardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_awards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('award_id')->unsigned();
            $table->integer('employee_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('awards', function(Blueprint $table)
        {
            $table->dropColumn(['employee_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('employee_awards');

        Schema::table('awards', function(Blueprint $table)
        {
            $table->integer('employee_group_id')->unsigned()->nullable();
        });

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
