<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyerslogoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyerslogouts', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('store_id')->unsigned()->index();
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
            $table->dateTime('start')->nullable()->index();
            $table->dateTime('end')->nullable()->index();
            $table->string('location')->nullable()->index();
            $table->text('recap')->nullable();
            $table->decimal('sales', 8 , 2 )->unsigned()->nullable()->index();
            $table->tinyInteger('published')->unsigned()->index();
            $table->dateTime('submitted')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('buyerslogouts');
    
        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
