<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLogoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Update Logouts Table
        Schema::table('logouts', function (Blueprint $table) {

            //Add Columns
            $table->decimal('lymtd' , 8 , 2)->after('recap');
            $table->decimal('mtd' , 8 , 2)->after('lymtd');
            $table->decimal('returns' , 8 , 2)->after('sales');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Update the Logouts Table
        Schema::table('logouts', function (Blueprint $table) {

            //Drop Columns
            $table->dropColumn('lymtd');
            $table->dropColumn('mtd');
            $table->dropColumn('returns');

        });
    }
}
