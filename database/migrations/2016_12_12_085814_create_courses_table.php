<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->decimal('passing_score', 5, 2)->unsigned();
            $table->timestamps();
        });

        Schema::create('course_stores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->integer('store_id')->unsigned();
        });

        Schema::create('course_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->integer('role_id')->unsigned();
        });

        if( Role::find(1) ){

            //An Array of the Created Permissions
            $permissions = [

                //View Course List Permission
                Permission::create([
                    'name'          => 'Course - List',
                    'slug'          => 'course',
                    'description'   => 'Permission to Access the Course List'
                ]),

                //Create Course Permission
                Permission::create([
                    'name'          => 'Course - Create',
                    'slug'          => 'course.create',
                    'description'   => 'Permission to Create new Course'
                ]),

                //Edit Course Permission
                Permission::create([
                    'name'          => 'Course - Edit',
                    'slug'          => 'course.edit',
                    'description'   => 'Permission to Edit Course'
                ]),

                //Delete Course Permission
                Permission::create([
                    'name'          => 'Course - Delete',
                    'slug'          => 'course.delete',
                    'description'   => 'Permission to Delete Course'
                ])

            ];

            //Attach the Permissions to the Admin Role
            foreach( $permissions as $permission ){

                Role::find(1)->attachPermission( $permission );

            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('courses');
        Schema::drop('course_stores');
        Schema::drop('course_roles');

        //Delete the Permissions
        Permission::whereIn( 'slug' , [ 'course' , 'course.create' , 'course.edit' , 'course.delete' ] )->delete();

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
