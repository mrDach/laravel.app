<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RanameEmployeeStatusIdToAffilateStatusIdInAffilate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliates', function(Blueprint $table)
        {
            $table->renameColumn('affiliate_status_id', 'affiliate_group_id');
            $table->renameColumn('employee_status_id', 'affiliate_status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Schema::table('affiliates', function(Blueprint $table)
        {
            $table->renameColumn('affiliate_status_id', 'employee_status_id');
            $table->renameColumn('affiliate_group_id', 'employee_group_id');
        });

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
