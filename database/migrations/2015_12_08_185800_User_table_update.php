<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTableUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function(Blueprint $table) {

            //Drop Name Column
            $table->dropColumn('name');

            //Add New Columns
            $table->string('firstname',100)->after('id');
            $table->string('lastname',100)->after('firstname');
            $table->date('dob')->nullable()->after('lastname');
            $table->string('city',100)->nullable()->after('password');
            $table->string('province',100)->nullable()->after('city');
            $table->string('phone',100)->nullable()->after('province');

            //Make the Username Column Unique
            $table->unique('username');

            //Add the Indexes
            $table->index('firstname');
            $table->index('lastname');
            $table->index('city');
            $table->index('province');
            $table->index('dob');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        //Remove Added Tables, Re-add "Name" Column
        Schema::table('users', function(Blueprint $table) {

            //Re-Add Name Column
            $table->string('name');

            //Drop Columns
            $table->dropColumn([ 'firstname' , 'lastname' , 'dob' , 'city' , 'province' , 'phone' ]);

            //Remove the Username Unique Column
            $table->dropUnique('users_username_unique');

        });

    }
}
