<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReminderAddRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
        * Add role_id to reminders table
        */
        Schema::table('reminders', function (Blueprint $table) {
            $table->integer('role_id')->nullable()->index()->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Drop Columns
        Schema::table('reminders', function (Blueprint $table) {
            $table->dropColumn('role_id');
        });
    }
}
