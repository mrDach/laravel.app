<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;
use App\Models\User;

class UpdateUsersTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            //Remove the Incidents Table
            $table->string('slug')->index()->after('username');

        });

        //Get all Users
        $users = User::all();

        //Loop throug heach User
        foreach( $users as $user ){

            //Add the User Slug
            $user->slug = Str::slug( $user->firstname . ' ' . $user->lastname . ' ' . $user->username );

            //Save the Slug
            $user->save();

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('slug');

        });
    }
}
