<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->integer('employee_id')->unsigned();
            $table->dateTime('date');
            $table->float('spent', 8, 2)->unsigned();
            $table->timestamps();
        });

        if( Role::find(1) ){

            //An Array of the Created Permissions
            $permissions = [

                //View Customer List Permission
                Permission::create([
                    'name'          => 'Customer - List',
                    'slug'          => 'customer',
                    'description'   => 'Permission to Access the Customer List'
                ]),

                //Create Customer Permission
                Permission::create([
                    'name'          => 'Customer - Create',
                    'slug'          => 'customer.create',
                    'description'   => 'Permission to Create new Customer'
                ]),

                //Delete Customer Permission
                Permission::create([
                    'name'          => 'Customer - Delete',
                    'slug'          => 'customer.delete',
                    'description'   => 'Permission to Delete Customer'
                ])

            ];

            //Attach the Permissions to the Admin Role
            foreach( $permissions as $permission ){

                Role::find(1)->attachPermission( $permission );

            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('customers');

        //Delete the Permissions
        Permission::whereIn( 'slug' , [ 'customer' , 'customer.create' , 'customer.delete' ] )->delete();

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
