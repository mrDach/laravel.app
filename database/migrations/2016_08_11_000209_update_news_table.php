<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Update News Table
        Schema::table('news', function (Blueprint $table) {

            $table->integer('event_id')->nullable()->unsigned()->index()->after('user_id');
            $table->foreign('event_id')->references('id')->on('event')->onDelete('SET NULL');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Update the News Table
        Schema::table('news', function (Blueprint $table) {

            //Drop Columns
            $table->dropForeign(['event_id']);
            $table->dropColumn('event_id');

        });
    }
}
