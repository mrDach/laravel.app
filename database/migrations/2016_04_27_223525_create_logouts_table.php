<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logouts', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('store_id')->unsigned()->index();
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
            $table->dateTime('start')->nullable()->index();
            $table->dateTime('end')->nullable()->index();
            $table->text('recap')->nullable();
            $table->decimal('sales', 8 , 2 )->unsigned()->nullable()->index();
            $table->text('staff')->nullable();
            $table->integer('traffic')->nullable()->index();
            $table->decimal( 'conversions' , 8 , 2 )->unsigned()->nullable()->index();
            $table->integer('insoles')->nullable()->index();
            $table->text('notes')->nullable();
            $table->tinyInteger('published')->unsigned()->index();
            $table->dateTime('submitted')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('logouts');
    
        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
