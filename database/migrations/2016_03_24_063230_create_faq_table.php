<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('link_id')->index();
            $table->string('link_type')->index();
            $table->string('category');
            $table->string('question');
            $table->text('answer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('faq');
    
        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
