<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;


class CreateAwardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_group_id')->unsigned();
            $table->string('location');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->integer('customers')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->timestamps();
        });

        if( Role::find(1) ){

            //An Array of the Created Permissions
            $permissions = [

                //View Award List Permission
                Permission::create([
                    'name'          => 'Award - List',
                    'slug'          => 'award',
                    'description'   => 'Permission to Access the Award List'
                ]),

                //Create Award Permission
                Permission::create([
                    'name'          => 'Customer - Create',
                    'slug'          => 'award.create',
                    'description'   => 'Permission to Create new Award'
                ]),

                //Edit Award Permission
                Permission::create([
                    'name'          => 'Customer - Edit',
                    'slug'          => 'award.edit',
                    'description'   => 'Permission to Create new Award'
                ]),

                //Delete Award Permission
                Permission::create([
                    'name'          => 'Customer - Delete',
                    'slug'          => 'award.delete',
                    'description'   => 'Permission to Delete Award'
                ])

            ];

            //Attach the Permissions to the Admin Role
            foreach( $permissions as $permission ){

                Role::find(1)->attachPermission( $permission );

            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Disable Foreign_Key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Drop the Table
        Schema::drop('awards');

        //Delete the Permissions
        Permission::whereIn( 'slug' , [ 'awards' , 'awards.create' , 'awards.edit' , 'awards.delete' ] )->delete();

        //Re-enable foreign_key_Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
