<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //Update Users Table
        Schema::table('documents', function (Blueprint $table) {

            //Add the Username Column
            $table->integer('folder_id')->unsigned()->nullable()->after('id');
            $table->foreign('folder_id')->references('id')->on('document_folders')->onDelete('SET NULL');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        //Drop the Last Active Column
        Schema::table('documents', function (Blueprint $table) {

            //Remove the Column
            $table->dropForeign(['folder_id']);
            $table->dropColumn('folder_id');

        });
    }
}
