<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReminder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
        * Update reminders table
        */
        Schema::table('reminders', function (Blueprint $table) {
            $table->string('type')->after('timecount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reminders', function (Blueprint $table) {
            /**
            * Drop column, type
            * 
            */
            $table->dropColumn('type');
        });
    }
}
