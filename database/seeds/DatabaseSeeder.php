<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //Seed Users
        $this->call( UserTableSeeder::class );

        //Seed Roles
        $this->call( RolesTableSeeder::class );

        //Seed Users Roles
        $this->call( UserRoleTableSeeder::class );

        //Seed Permissions
        $this->call( PermissionTableSeeder::class );

        //Seed Permissions Roles
        $this->call( PermissionRoleTableSeeder::class );

        //Seed Search
        $this->call( SearchTableSeeder::class );

        //Seed Permission Roles
        $this->call( SearchPermissionTableSeeder::class );

        //Seed Mimes
        $this->call( MimesTableSeeder::class );

        Model::reguard();

    }
}
