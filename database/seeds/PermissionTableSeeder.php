<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Temporarily disable checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Truncate Table
    	DB::table('permissions')->truncate();
        
        //Insert the Default Admin User
        DB::table('permissions')->insert([

        	//Users
            [ 'name' => 'Users - List',                            'slug' => 'users', 	                        'description' => 'Permission to Access the Users List', 	                                                    'created_at' => date('Y-m-d H:i:s'),     'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Users - Create',	                       'slug' => 'users.create', 	                'description' => 'Permission to Create new Users', 	                                                            'created_at' => date('Y-m-d H:i:s'),     'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Users - Edit', 	                       'slug' => 'users.edit', 	                    'description' => 'Permission to Edit Users', 		                                                            'created_at' => date('Y-m-d H:i:s'),     'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Users - Delete', 	                       'slug' => 'users.delete', 	                'description' => 'Permission to Delete Users', 		                                                            'created_at' => date('Y-m-d H:i:s'),     'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Users - Birthday Notifications',          'slug' => 'users.recipient',                 'descirption' => 'Permission to Receive Birthday Notifications of Users',                                       'created_at' => date('Y-m-d H:i:s'),     'updated_at' => date('Y-m-d H:i:s') ],

        	//Roles
            [ 'name' => 'Roles - List',                            'slug' => 'roles',      	                    'description' => 'Permission to Access the Roles List', 	                                                    'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Roles - Create',	                       'slug' => 'roles.create', 	                'description' => 'Permission to Create new Roles', 	                                                            'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Roles - Edit', 	                       'slug' => 'roles.edit', 	                    'description' => 'Permission to Edit Roles', 		                                                            'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Roles - Delete', 	                       'slug' => 'roles.delete', 	                'description' => 'Permission to Delete Roles', 		                                                            'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],   


            //Stores
            [ 'name' => 'Stores - List',                          'slug' => 'stores',                           'description' => 'Permission to Access the Stores List',                                                        'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Stores - Create',                        'slug' => 'stores.create',                    'description' => 'Permission to Create new Stores',                                                             'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Stores - Edit',                          'slug' => 'stores.edit',                      'description' => 'Permission to Edit Stores',                                                                   'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Stores - Delete',                        'slug' => 'stores.delete',                    'description' => 'Permission to Delete Stores',                                                                 'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],            
        

            //News
            [ 'name' => 'News - List',                            'slug' => 'news',                             'description' => 'Permission to Access the News List',                                                          'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'News - View',                            'slug' => 'news.view',                        'description' => 'Permission to View the News',                                                                 'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'News - Create',                          'slug' => 'news.create',                      'description' => 'Permission to Create new News',                                                               'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'News - Edit',                            'slug' => 'news.edit',                        'description' => 'Permission to Edit News',                                                                     'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'News - Delete',                          'slug' => 'news.delete',                      'description' => 'Permission to Delete News',                                                                   'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],            
        

            //Calendar / Events
            [ 'name' => 'Events - List',                          'slug' => 'events',                           'description' => 'Permission to Access the Events List',                                                        'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Events - View',                          'slug' => 'events.view',                      'description' => 'Permission to View the News',                                                                 'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Events - Create',                        'slug' => 'events.create',                    'description' => 'Permission to Create new Events',                                                             'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Events - Edit',                          'slug' => 'events.edit',                      'description' => 'Permission to Edit Events',                                                                   'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Events - Delete',                        'slug' => 'events.delete',                    'description' => 'Permission to Delete Events',                                                                 'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],            
        
            //Search
            [ 'name' => 'Search - List',                          'slug' => 'search',                           'description' => 'Permission to Access the Search',                                                             'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],

            //Feedback
            [ 'name' => 'Feedback Recipient',                     'slug' => 'feedback.receive',                 'description' => 'Permission to Receive Feedback Emails',                                                       'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],            
        
            //Marketing & Promos
            [ 'name' => 'Marketing & Promos - List',              'slug' => 'promos',                           'description' => 'Permission to Access the Marketing & Promos List',                                            'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Marketing & Promos - View',              'slug' => 'promos.view',                      'description' => 'Permission to View the Marketing & Promos',                                                   'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Marketing & Promos - Create',            'slug' => 'promos.create',                    'description' => 'Permission to Create new Marketing & Promos',                                                 'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Marketing & Promos - Edit',              'slug' => 'promos.edit',                      'description' => 'Permission to Edit Marketing & Promos',                                                       'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Marketing & Promos - Delete',            'slug' => 'promos.delete',                    'description' => 'Permission to Delete Marketing & Promos',                                                     'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],            
         
            //Documents
            [ 'name' => 'Documents - List',                       'slug' => 'documents',                        'description' => 'Permission to Access the Documents List',                                                     'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Documents - Manage',                     'slug' => 'documents.manage',                 'description' => 'Permission to Edit Documents',                                                                'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
                   
            //Weekly Reports
            [ 'name' => 'Weekly Reports - List',                  'slug' => 'reports',                          'description' => 'Permission to Access the Weekly Reports List',                                                'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Weekly Reports - View',                  'slug' => 'reports.view',                     'description' => 'Permission to View the Weekly Reports on the Dashboard',                                      'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Weekly Reports - Create',                'slug' => 'reports.create',                   'description' => 'Permission to Create new Weekly Reports',                                                     'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Weekly Reports - Edit',                  'slug' => 'reports.edit',                     'description' => 'Permission to Edit Weekly Reports',                                                           'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Weekly Reports - Delete',                'slug' => 'reports.delete',                   'description' => 'Permission to Delete Weekly Reports',                                                         'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
         
            //Logouts
            [ 'name' => 'Logouts - List',                         'slug' => 'logouts',                          'description' => 'Permission to Access the Logouts List',                                                       'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Logouts - View',                         'slug' => 'logouts.view',                     'description' => 'Permission to View the Logouts on the Dashboard',                                             'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Logouts - Create',                       'slug' => 'logouts.create',                   'description' => 'Permission to Create new Logouts',                                                            'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Logouts - Edit',                         'slug' => 'logouts.edit',                     'description' => 'Permission to Edit Logouts',                                                                  'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Logouts - Delete',                       'slug' => 'logouts.delete',                   'description' => 'Permission to Delete Logouts',                                                                'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],     
            [ 'name' => 'Logouts - Report',                       'slug' => 'logouts.report',                   'description' => 'Permission to View Logout Reports',                                                           'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],     
            [ 'name' => 'Logouts - All Recipient',                'slug' => 'logouts.recipients.all',           'description' => 'Permission to Receive All Logout Emails',                                                     'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],     
            [ 'name' => 'Logouts - Single Recipient',             'slug' => 'logouts.recipients.single',        'description' => 'Permission to Receive Logout Emails for Stores they\'re assigned to',                         'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],     
                 
            //Buyer Logouts
            [ 'name' => 'Buyers Logouts - List',                  'slug' => 'buyerslogouts',                    'description' => 'Permission to Access the Buyer Logouts List',                                                 'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Buyers Logouts - View',                  'slug' => 'buyerslogouts.view',               'description' => 'Permission to View the Buyer Logouts on the Dashboard',                                       'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Buyers Logouts - Create',                'slug' => 'buyerslogouts.create',             'description' => 'Permission to Create new Buyer Logouts',                                                      'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Buyers Logouts - Edit',                  'slug' => 'buyerslogouts.edit',               'description' => 'Permission to Edit Buyer Logouts',                                                            'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Buyers Logouts - Delete',                'slug' => 'buyerslogouts.delete',             'description' => 'Permission to Delete Buyer Logouts',                                                          'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],     
            [ 'name' => 'Buyers Logouts - Report',                'slug' => 'buyerslogouts.report',             'description' => 'Permission to View Logout Reports',                                                           'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],     
            [ 'name' => 'Buyers Logouts - All Recipient',         'slug' => 'buyerslogouts.recipients.all',     'description' => 'Permission to Receive All Logout Emails',                                                     'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],     
            [ 'name' => 'Buyers Logouts - Single Recipient',      'slug' => 'buyerslogouts.recipients.single',  'description' => 'Permission to Receive Logout Emails for Stores they\'re assigned to',                         'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],     
            
            //Positions
            [ 'name' => 'Positions - List',                       'slug' => 'positions',                        'description' => 'Permission to Access the Position List',                                                      'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Positions - Create',                     'slug' => 'positions.create',                 'description' => 'Permission to Create new Positions',                                                          'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Positions - Edit',                       'slug' => 'positions.edit',                   'description' => 'Permission to Edit Positions',                                                                'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Positions - Delete',                     'slug' => 'positions.delete',                 'description' => 'Permission to Delete Positions',                                                              'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],          
            
            //Payroll
            [ 'name' => 'Payroll - List',                         'slug' => 'payrolls',                         'description' => 'Permission to Access the Payroll List',                                                       'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Payroll - Create',                       'slug' => 'payrolls.create',                  'description' => 'Permission to Create new Payrolls',                                                           'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Payroll - Edit',                         'slug' => 'payrolls.edit',                    'description' => 'Permission to Edit Payrolls',                                                                 'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Payroll - Delete',                       'slug' => 'payrolls.delete',                  'description' => 'Permission to Delete Payrolls',                                                               'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],   
            [ 'name' => 'Payroll - View',                         'slug' => 'payrolls.view',                    'description' => 'Permission to View Payrolls',                                                                 'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],  
            [ 'name' => 'Payroll - All Recipient',                'slug' => 'payrolls.recipients.all',          'description' => 'Permission to Receive All Payroll Emails',                                                    'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],     
            [ 'name' => 'Payroll - Single Recipient',             'slug' => 'payrolls.recipients.single',       'description' => 'Permission to Receive Payroll Emails for Stores they\'re assigned to',                        'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],  
            
            //Sick Days
            [ 'name' => 'Sick Days - List',                       'slug' => 'sickdays',                         'description' => 'Permission to Access the Position List',                                                      'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Sick Days - Create',                     'slug' => 'sickdays.create',                  'description' => 'Permission to Create new Sick Days',                                                          'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Sick Days - Edit',                       'slug' => 'sickdays.edit',                    'description' => 'Permission to Edit Sick Days',                                                                'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Sick Days - Delete',                     'slug' => 'sickdays.delete',                  'description' => 'Permission to Delete Sick Days',                                                              'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],          
            [ 'name' => 'Sick Days - View',                       'slug' => 'sickdays.view',                    'description' => 'Permission to View Sick Days',                                                                'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],  
            [ 'name' => 'Sick Days - All Recipient',              'slug' => 'sickdays.recipients.all',          'description' => 'Permission to Receive All Sick Days Emails',                                                  'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],     
            [ 'name' => 'Sick Days - Single Recipient',           'slug' => 'sickdays.recipients.single',       'description' => 'Permission to Receive Sick Days Emails for Stores they\'re assigned to',                      'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],  

            //Medical Referrals
            [ 'name' => 'Medical Referrals - List',               'slug' => 'medical',                          'description' => 'Permission to Access the Medical Referrals List',                                             'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Medical Referrals - View',               'slug' => 'medical.view',                     'description' => 'Permission to View the Medical Referrals on the Dashboard',                                   'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Medical Referrals - Create',             'slug' => 'medical.create',                   'description' => 'Permission to Create new Medical Referrals',                                                  'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Medical Referrals - Edit',               'slug' => 'medical.edit',                     'description' => 'Permission to Edit Medical Referrals',                                                        'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Medical Referrals - Delete',             'slug' => 'medical.delete',                   'description' => 'Permission to Delete Medical Referrals',                                                      'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],   
            [ 'name' => 'Medical Referrals - All Recipient',      'slug' => 'medical.recipients.all',           'description' => 'Permission to Receive All Medical Referral Emails',                                           'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],     
            
            //Doctors
            [ 'name' => 'Doctors - List',                         'slug' => 'doctors',                          'description' => 'Permission to Access the Doctors List',                                                       'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Doctors - Create',                       'slug' => 'doctors.create',                   'description' => 'Permission to Create new Doctors',                                                            'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Doctors - Edit',                         'slug' => 'doctors.edit',                     'description' => 'Permission to Edit Doctors',                                                                  'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Doctors - Delete',                       'slug' => 'doctors.delete',                   'description' => 'Permission to Delete Doctors',                                                                'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],   

            //Tech Talk
            [ 'name' => 'Tech Talk - List',                       'slug' => 'tech',                             'description' => 'Permission to Access the Tech Talk List',                                                     'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Tech Talk - Create',                     'slug' => 'tech.create',                      'description' => 'Permission to Create new Tech Talk',                                                          'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Tech Talk - Edit',                       'slug' => 'tech.edit',                        'description' => 'Permission to Edit Tech Talk',                                                                'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Tech Talk - Delete',                     'slug' => 'tech.delete',                      'description' => 'Permission to Delete Tech Talk',                                                              'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            
            //Email Request
            [ 'name' => 'New Email Request - Send',               'slug' => 'emailrequest',                     'description' => 'Allow the user to request a new email address',                                               'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],

            //Affiliate
            [ 'name' => 'Affiliate - List',                       'slug' => 'affiliate',                        'description' => 'Permission to Access the Affiliate List',                                                     'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Affiliate - Create',                     'slug' => 'affiliate.create',                 'description' => 'Permission to Create new Affiliate',                                                          'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Affiliate - Delete',                     'slug' => 'affiliate.delete',                 'description' => 'Permission to Delete Affiliate',                                                              'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Affiliate - Award',                      'slug' => 'affiliate.award',                  'description' => 'Permission to Set Affiliate Awards',                                                          'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],

            //Affiliate Groups
            [ 'name' => 'Affiliate Groups - List',                'slug' => 'affiliate.groups',                 'description' => 'Permission to Access the Affiliate Groups List',                                              'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Affiliate Groups - Create',              'slug' => 'affiliate.groups.create',          'description' => 'Permission to Create new Affiliate Groups',                                                   'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Affiliate Groups - Delete',              'slug' => 'affiliate.groups.delete',          'description' => 'Permission to Delete Affiliate Groups',                                                       'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],

            //Customer
            [ 'name' => 'Customer - List',                       'slug' => 'customer',                          'description' => 'Permission to Access the Customer List',                                                      'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Customer - Create',                     'slug' => 'customer.create',                   'description' => 'Permission to Create new Customer',                                                           'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Customer - Delete',                     'slug' => 'customer.delete',                   'description' => 'Permission to Delete Customer',                                                               'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],

            //Course
            [ 'name' => 'Course - List',                         'slug' => 'course',                          'description' => 'Permission to Access the Course List',                                                          'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Course - Create',                       'slug' => 'course.create',                   'description' => 'Permission to Create new Course',                                                               'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Course - Edit',                         'slug' => 'course.edit',                     'description' => 'Permission to Edit Course',                                                                     'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Course - Delete',                       'slug' => 'course.delete',                   'description' => 'Permission to Delete Course',                                                                   'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],

            //Report
            [ 'name' => 'Report - List',                         'slug' => 'affiliate.reports',                 'description' => 'Permission to Access the Reports List',                                                       'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],

            //Award
            [ 'name' => 'Award - List',                          'slug' => 'award',                             'description' => 'Permission to Access the Award List',                                                         'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Award - Create',                        'slug' => 'award.create',                      'description' => 'Permission to Create new Award',                                                              'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Award - Edit',                          'slug' => 'award.edit',                        'description' => 'Permission to Edit Award',                                                                    'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Award - Delete',                        'slug' => 'award.delete',                      'description' => 'Permission to Delete Award',                                                                  'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],

            //AffiliateMessageSystem
            [ 'name' => 'Affiliate Message - List',              'slug' => 'affiliate.message',                 'description' => 'Permission to Access the Affiliate Messages List',                                            'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],
            [ 'name' => 'Affiliate Message - Create',            'slug' => 'affiliate.message.create',          'description' => 'Permission to Access the Affiliate Messages create',                                          'created_at' => date('Y-m-d H:i:s'),    'updated_at' => date('Y-m-d H:i:s') ],

        ]);

        //Reset Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
    }
}
