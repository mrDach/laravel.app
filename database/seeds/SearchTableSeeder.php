<?php

use Illuminate\Database\Seeder;

class SearchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Temporarily disable checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Truncate Table
        DB::table('search')->truncate();
        
        //Insert the Default Admin User
        DB::table('search')->insert([
            [ 'link_id' => '1', 'link_type' => 'App\Models\User', 'title' => 'admin', 'query' => 'admin', 'url' => '/admin/users/edit/1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ],
            [ 'link_id' => '1', 'link_type' => 'App\Models\Role', 'title' => 'Administration', 'query' => 'administration', 'url' => '/admin/roles/edit/1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ]
        ]);

        //Reset Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
    }
}
