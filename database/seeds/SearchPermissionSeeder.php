<?php

use Illuminate\Database\Seeder;

class SearchPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Temporarily disable checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Truncate Table
        DB::table('search_permission')->truncate();
        
        //Insert the Default Admin User
        DB::table('search_permission')->insert([
            [ 'search_id' => 1, 'permission_id' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ],
            [ 'search_id' => 2, 'permission_id' => 7, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ]
        ]);

        //Reset Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
    }
}
