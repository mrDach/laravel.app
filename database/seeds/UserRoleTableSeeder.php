<?php

use Illuminate\Database\Seeder;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Temporarily disable checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Truncate Table
        DB::table('role_user')->truncate();

        //Insert the Default Admin User
        DB::table('role_user')->insert([
            [ 'role_id' => 1, 'user_id' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ]
        ]);

        //Reset Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            
    }
}
