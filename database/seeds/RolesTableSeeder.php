<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Temporarily disable checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Truncate Table
        DB::table('roles')->truncate();
        
        //Insert the Default Admin User
        DB::table('roles')->insert([
            [ 'name' => 'Administrator', 'slug' => 'admin', 'description' => 'Administrator Role', 'level' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ]
        ]);

        //Reset Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
    }
}
