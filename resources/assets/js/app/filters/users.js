(function(){
	
	var app 		= angular.module('System.Filters');
	



	/**
	*
	*	FILTER: options
	*		- filtering all options with already selected options
	*
	* 	USAGE:
	* 		{{ options:logout.selected_options:(index of select) }}
	*
	**/
	app.filter('options', [function(){

		return function( options, selected_options, index) {
			var filter_options = [];
			angular.forEach(options, function (option) {
				if(selected_options.length > 0) {
					// check existing options in selected_options array
					var added_options_array = [];
					angular.forEach(selected_options, function (st, i) {
						if(i!==index && st.id==option.id) {
							added_options_array.push(option);
						}
					});
					if(added_options_array.length === 0) {
						filter_options.push(option);
					}
				} else {
					filter_options.push(option);
				}
			});
			return filter_options;
		};

	}]);

})();