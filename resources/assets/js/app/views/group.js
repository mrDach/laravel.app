(function(){


    var app = angular.module('System.Views');

    app.config([ '$stateProvider' , '$urlRouterProvider' , function( $stateProvider , $urlRouterProvider ){

        $stateProvider.state({

            name: 			'group-add',

            url: 			'^/affiliate/group/add',

            title: 			'Affiliate Group :: Add',

            templateUrl: 	':api/layout/components/manage-group',

            controller: 	'AffiliateGroupController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate' } , { icon:'users' , url: '/affiliate/group' , name: 'Affiliate Group' } , { icon:'user-plus' , url: '/affiliate/add' , name: 'Add' } ]

        }).state({

            name: 			'group-list',

            url: 			'^/affiliate/group/{page:int}',

            title: 			'Affiliate Group :: List',

            params: 		{ page: { value:null , squash:true } },

            templateUrl: 	':api/layout/controllers/group',

            controller: 	'AffiliateGroupController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate' } , { icon:'users' , url: '/affiliate/group' , name: 'Affiliate Group' } ]

        });

    }]);




})();