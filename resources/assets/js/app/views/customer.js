(function(){


    var app = angular.module('System.Views');

    app.config([ '$stateProvider' , '$urlRouterProvider' , function( $stateProvider , $urlRouterProvider ){

        $stateProvider.state({

            name: 			'customer-add',

            url: 			'^/affiliate/customer/add',

            title: 			'Affiliate :: Add Customer',

            templateUrl: 	':api/layout/components/manage-customer',

            controller: 	'CustomerController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate' } , { icon:'user-o' , url: '/affiliate/customer' , name: 'Customers' } , { icon:'plus-square-o' , url: '/affiliate/customers/add' , name: 'Add Customer' } ]

        }).state({

            name: 			'customers-list',

            url: 			'^/affiliate/customer/{page:int}',

            title: 			'Affiliate :: Customers List',

            params: 		{ page: { value:null , squash:true } },

            templateUrl: 	':api/layout/controllers/customer',

            controller: 	'CustomerController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate' } , { icon:'user-o' , url: '/affiliate/customer' , name: 'Customers' } ]

        });

    }]);



})();