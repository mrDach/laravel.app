(function(){

    var app = angular.module('System.Views');

    app.config([ '$stateProvider' , '$urlRouterProvider' , function( $stateProvider , $urlRouterProvider ){

        $stateProvider.state({

            name: 			'course-add',

            url: 			'^/course/add',

            title: 			'Course :: Add',

            templateUrl: 	':api/layout/components/manage-course',

            controller: 	'CourseController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'university' , url: '/course' , name: 'Courses' } , { icon:'plus-circle' ,  url: '/course/add' , name: 'Add Course' } ]

        }).state({

            name: 			'course-edit',

            url: 			'^/course/edit/{courseid:int}',

            title: 			'Course :: Edit',

            params: 		{ courseid: { value:null , squash:true } },

            templateUrl: 	':api/layout/components/manage-course',

            controller: 	'CourseController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'university' , url: '/course' , name: 'Courses' } , { icon:'pencil-square' , url: '/courses/edit/:courseid' , name: 'Edit Course' } ]

        }).state({

            name: 			'course-take',

            url: 			'^/course/take/{courseid:int}',

            title: 			'Course :: Take',

            params: 		{ courseid: { value:null , squash:true } },

            templateUrl: 	':api/layout/components/course-page',

            controller: 	'CourseController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'university' , url: '/course' , name: 'Courses' } , { icon:'pencil-square' , url: '/courses/take/:courseid' , name: 'Take Course' } ]

        }).state({

            name: 			'course-list',

            url: 			'^/course/{page:int}',

            title: 			'Courses',

            params: 		{ page: { value:null , squash:true } },

            templateUrl: 	':api/layout/controllers/course',

            controller: 	'CourseController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'university' , url: '/course' , name: 'Courses' } ]

        });

    }]);

})();