(function(){


    var app = angular.module('System.Views');

    app.config([ '$stateProvider' , '$urlRouterProvider' , function( $stateProvider , $urlRouterProvider ){

        $stateProvider.state({

            name: 			'message-add',

            url: 			'^/affiliate/message/add',

            title: 			'Affiliate Message :: Add',

            templateUrl: 	':api/layout/components/manage-affiliate-messages',

            controller: 	'AffiliateMessageSystemController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate' } , { icon:'users' , url: '/affiliate/message' , name: 'Affiliate Messages' } , { icon:'user-plus' , url: '/affiliate/message/add' , name: 'Add' } ]

        }).state({

            name: 			'message-list',

            url: 			'^/affiliate/message/{page:int}',

            title: 			'Affiliate Message :: List',

            params: 		{ page: { value:null , squash:true } },

            templateUrl: 	':api/layout/controllers/affiliate-messages',

            controller: 	'AffiliateMessageSystemController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate' } , { icon:'users' , url: '/affiliate/message' , name: 'Affiliate Messages' } ]

        });

    }]);




})();