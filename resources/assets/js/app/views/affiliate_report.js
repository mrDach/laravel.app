(function(){
	

	var app = angular.module('System.Views');

	app.config([ '$stateProvider' , '$urlRouterProvider' , function( $stateProvider , $urlRouterProvider ){

		$stateProvider.state({

			name: 			'affiliate-report-list',

			url: 			'^/affiliate/reports/{page:int}',

			title: 			'Reports',
			
			params: 		{ page: { value:null , squash:true } },

			templateUrl: 	':api/layout/controllers/affiliate_reports',
			
			controller: 	'AffiliateReportController',

			breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate' } , { icon:'area-chart' , url: '/affiliate/reports' , name: 'Reports' } ]
		
		});

	}]);

	


})();