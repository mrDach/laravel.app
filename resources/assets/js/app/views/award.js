(function(){


    var app = angular.module('System.Views');

    app.config([ '$stateProvider' , '$urlRouterProvider' , function( $stateProvider , $urlRouterProvider ){

        $stateProvider.state({

            name: 			'award-add',

            url: 			'^/affiliate/award/add',

            title: 			'Award :: Add',

            templateUrl: 	':api/layout/components/manage-award',

            controller: 	'AwardController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate' } , { icon:'ticket' , url: '/affiliate/award' , name: 'Awards' } , { icon:'plus-circle' ,  url: '/affiliate/award/add' , name: 'Add Award' } ]

        }).state({

            name: 			'award-edit',

            url: 			'^/affiliate/award/edit/{awardid:int}',

            title: 			'Award :: Edit',

            params: 		{ awardid: { value:null , squash:true } },

            templateUrl: 	':api/layout/components/manage-award',

            controller: 	'AwardController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate' } , { icon:'ticket' , url: '/affiliate/award' , name: 'Awards' } , { icon:'pencil-square' , url: '/affiliate/awards/edit/:awardid' , name: 'Edit Award' } ]

        }).state({

            name: 			'award-list',

            url: 			'^/affiliate/award/{page:int}',

            title: 			'Awards',

            params: 		{ page: { value:null , squash:true } },

            templateUrl: 	':api/layout/controllers/award',

            controller: 	'AwardController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate' } , { icon:'ticket' , url: '/affiliate/award' , name: 'Awards' } ]

        });

    }]);




})();