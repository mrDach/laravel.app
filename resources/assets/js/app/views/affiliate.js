(function(){


    var app = angular.module('System.Views');

    app.config([ '$stateProvider' , '$urlRouterProvider' , function( $stateProvider , $urlRouterProvider ){

        $stateProvider.state({

            name: 			'affiliate-add',

            url: 			'^/affiliate/add',

            title: 			'Affiliate :: Add',

            templateUrl: 	':api/layout/components/manage-affiliate',

            controller: 	'AffiliateController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate List' } , { icon:'user-plus' , url: '/affiliate/add' , name: 'Add' } ]

        }).state({

            name: 			'affiliate-award',

            url: 			'^/affiliate/{affiliateid:int}/award',

            title: 			'Affiliate :: Awards',

            params: 		{ affiliateid: { value:null , squash:true } },

            templateUrl: 	':api/layout/components/manage-affiliate-award',

            controller: 	'AffiliateController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'Affiliate' } , { icon:'user' , url: '' , name: 'Awards' } ]

        }).state({

            name: 			'affiliate-list',

            url: 			'^/affiliate/{page:int}',

            title: 			'Affiliate :: List',

            params: 		{ page: { value:null , squash:true } },

            templateUrl: 	':api/layout/controllers/affiliate',

            controller: 	'AffiliateController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'user' , url: '/affiliate' , name: 'List' } ]

        });

    }]);




})();