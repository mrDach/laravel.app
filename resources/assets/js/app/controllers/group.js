(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('AffiliateGroupController', [ '$window' , '$stateParams' , '$scope' , '$http' , '$location' , 'Config' , 'User' , 'Page' , 'Loader' , function( $window , $stateParams , $scope , $http , $location , Config , User , Page , Loader ){
        User.ready(function(){

            if( Page.is( /^\/affiliate\/group\/add/ ) &&  User.hasPermission([ 'affiliate.groups' , 'affiliate.groups.create' ])  ){


                /**
                 *
                 *	ROUTE: /group/add
                 *		- add the Affiliate Group
                 *
                 * 	Params (URL):
                 * 		n/a
                 *
                 **/

                Page.loading.start();


                (function(){

                    Page.loading.end();


                    $scope.sending 			= false;
                    $scope.running 			= 'Saving';
                    $scope.action 			= 'insert';
                    $scope.title 			= 'Create new Group';
                    $scope.button 			= 'Add Group';
                    $scope.icon 			= 'pencil-square';
                    $scope.hasError 		= Page.hasError;
                    $scope.errors 			= [];

                    //On Form Submit
                    $scope.submit 	= function( form ){
                        if( form.$valid && !$scope.sending ){

                            $scope.sending = true;

                            $http.put( ':api/group' , $scope.group ).then(function( response ){
                                if( !response.data.result ){

                                    $scope.sending 	= false;
                                    $scope.success 	= '';
                                    $scope.errors 	= response.data.errors;

                                    $window.scrollTo(0,0);

                                }else{

                                    form.$setPristine();

                                    $scope.sending 	= false;
                                    $scope.errors 	= [];
                                    $scope.success 	= 'The Group have been saved';
                                    $scope.group    = {};

                                    $window.scrollTo(0,0);

                                }
                            });

                        }
                    };


                }());

                Page.loading.end();








            }else
            if( Page.is( /^\/affiliate\/group(\/[0-9]+)?$/ ) && User.hasPermission([ 'affiliate.groups' ])  ){

                /**
                 *
                 *	ROUTE: /group/([0-9]+)?
                 *		- The Affiliates Group List
                 *
                 * 	Params (URL):
                 * 		- page: 		(INT) Affiliates Group Listing Pagination
                 *
                 **/
                (function(){

                    $scope.page.data 		= [];
                    $scope.list 			= {
                        groups:             null,
                        loading: 	        false,
                    };

                    //On Page Load
                    $scope.load 			= function( page ){

                        var queries 		= [];
                        var limit 			= $scope.page.showing;

                        Loader.get( ':api/group' + ( limit ? '/' + limit : '' ) + ( page ? '/' + page : '' ) + ( queries.length > 0 ? '?' + queries.join('&') : '' ) , function( list ){
                            if( list ){

                                if( list.data.data.length > 0 || page == 1 ){

                                    if( $scope.page.current != page ){

                                        $location.path( '/affiliate/group' + ( page > 1 ? '/' + page : '' ) );

                                    }

                                    $.extend( $scope.page , { current: page }, list.data );

                                }else{

                                    Page.error(404);

                                }

                                Page.loading.end();

                                $scope.list.loading = false;

                            }
                        });

                    };

                    //On "Click Delete"
                    $scope.delete 		= function( group ){
                        if( User.hasPermission([ 'affiliate.groups.delete' ]) ){

                            jQuery.confirm({
                                title: 				'Are you sure?',
                                content: 			'Are you sure you want to delete group ' + group.name,
                                confirmButton: 		'Delete group',
                                confirmButtonClass: 'btn-danger',
                                cancelButton: 		'Cancel',
                                cancelButtonClass: 	'btn bgaaa coloraaa hoverbg555 hovercolorfff',
                                confirm: 			function(){

                                    $http.delete( ':api/group/' + group.id ).then(function( response ){
                                        if( !response.data.result ){

                                            $scope.success 	= '';
                                            $scope.errors 	= response.data.errors;

                                            $window.scrollTo(0,0);

                                        }else{

                                            $scope.errors 	= [];
                                            $scope.success 	= 'group ' + group.name + ' has been deleted.';

                                            $scope.load( 1 );

                                            $window.scrollTo(0,0);

                                        }
                                    });

                                }
                            });
                        }

                    }

                    //Load the First Page
                    $scope.load( ( $stateParams.page || 1 ) );

                })();




            }else{


                Page.loading.end();
                Page.error(404);


            }

        });
    }]);


})();