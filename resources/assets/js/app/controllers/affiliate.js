(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('AffiliateController', [ '$window' , '$stateParams' , '$scope' , '$http' , '$location' , 'Config' , 'User' , 'Page' , 'Loader' , 'Upload' , function( $window , $stateParams , $scope , $http , $location , Config , User , Page , Loader , Upload){
        User.ready(function(){

            if( Page.is( /^\/affiliate\/add/ ) &&  User.hasPermission([ 'affiliate' , 'affiliate.create' ])  ){


                /**
                 *
                 *	ROUTE: /affiliate/add
                 *		- add Affiliate
                 *
                 * 	Params (URL):
                 * 		n/a
                 *
                 **/

                Page.loading.start();


                Loader.get( ':api/group/all'  , function( data ){

                    Page.loading.end();


                    $scope.sending 			= false;
                    $scope.running 			= 'Updating';
                    $scope.action 			= 'insert';
                    $scope.title 			= 'Manage list';
                    $scope.button 			= 'Update list';
                    $scope.icon 			= 'pencil-square';
                    $scope.files 			= [];
                    $scope.errors 			= [];
                    $scope.affiliate_groups  = data.data.groups;

                    $scope.$watch('file',function(){
                        $scope.upload( $scope.files );
                    });

                    $scope.remove = function( file , index ){
                        $scope.affiliate.files.splice( index , 1 );
                        if( file.checked && $scope.affiliate.files.length > 0 ){
                            $scope.affiliate.files[0].checked = true;
                            $scope.affiliate.attachment_id 	= $scope.affiliate.files[0].attachment_id;
                        }else{
                            $scope.affiliate.attachment_id 	= null;
                        }
                    };

                    //On add file
                    $scope.upload = function( files ){
                        if( files && files.length ){

                            for( var i=0; i < files.length; i++ ){
                                if( !files[i].status ){
                                    (function( file ){

                                        Upload.upload({

                                            url: 	':api/affiliate/attach',

                                            data: 	{ 'file': file },

                                            method: 'POST',

                                        }).then(function(resp){
                                            if( resp.data.result ){

                                                file.status 		= 'success';
                                                file.attachment_id 	= resp.data.attachment_id;
                                                file.custom_name 	= file.name;

                                            }else{

                                                file.status = 'error';

                                            }
                                        },function(resp){

                                            file.status = 'error';

                                        },function(evt){

                                            file.status 	= 'running';
                                            file.progress 	= Math.min(100 , parseInt( 100 * evt.loaded / evt.total ) );

                                        });

                                    })( files[i] );
                                }
                            }

                        }
                    };

                    //TODO: add feedback of submit
                    //On Form Submit
                    $scope.submit 	= function( form ){
                        if( form.$valid && !$scope.sending ){

                            $scope.sending = true;

                            $http.post( ':api/affiliate' , $scope.affiliate ).then(function( response ){

                                $scope.sending 	= false;

                                if( !response.data.result ){

                                    $scope.success 	= '';

                                }else{

                                    form.$setPristine();

                                    $scope.success 	= 'The Affiliate have been saved';

                                }

                                $scope.errors 	= response.data.errors;

                                $window.scrollTo(0,0);

                            });

                        }
                    };


                });

                Page.loading.end();








            }else
            if( Page.is( /^\/affiliate(\/[0-9]+)?$/ ) && User.hasPermission([ 'affiliate' ])  ){

                /**
                 *
                 *	ROUTE: /affiliate/([0-9]+)?
                 *		- The Affiliates List
                 *
                 * 	Params (URL):
                 * 		- page: 		(INT) Affiliates Listing Pagination
                 *
                 **/
                (function(){

                    $scope.page.data 		= [];
                    $scope.list 			= {
                        query: 		        ( $location.search().q || null ),
                        sort: 		        ( $location.search().sort || null ),
                        group: 	            ( $location.search().group || null ),
                        groups:             null,
                        loading: 	        false,
                    };

                    //On Page Load
                    $scope.load 			= function( page , searching ){

                        var queries 		= [];
                        var limit 			= $scope.page.showing;

                        if( $scope.list.query ) 	queries.push( 'q=' + encodeURIComponent( $scope.list.query ) );
                        if( $scope.list.sort ) 		queries.push( 'sort=' + encodeURIComponent( $scope.list.sort ) );
                        if( $scope.list.group) 	    queries.push( 'group=' + encodeURIComponent( $scope.list.group ) );

                        if( !searching ){

                            Page.loading.start();

                        }else{

                            $scope.list.loading = true;

                        }

                        Loader.get( ':api/affiliate' + ( limit ? '/' + limit : '' ) + ( page ? '/' + page : '' ) + ( queries.length > 0 ? '?' + queries.join('&') : '' ) , function( list ){
                            if( list ){

                                if( list.data.data.length > 0 || page == 1 ){

                                    if( $scope.page.current != page ){

                                        $location.path( '/affiliate' + ( page > 1 ? '/' + page : '' ) );

                                    }

                                    $.extend( $scope.page , { current: page }, list.data );

                                    $scope.list.groups = list.data.affiliate_groups;

                                }else
                                if( !searching ){

                                    Page.error(404);

                                }

                                Page.loading.end();

                                $scope.list.loading = false;

                            }
                        });

                    };

                    //On "Click Delete"
                    $scope.delete 		= function( affiliate ){
                        if( User.hasPermission([ 'affiliate.delete' ]) ){

                            jQuery.confirm({
                                title: 				'Are you sure?',
                                content: 			'Are you sure you want to delete Affiliate ' + affiliate.firstname +' '+ affiliate.lastname,
                                confirmButton: 		'Delete affiliate',
                                confirmButtonClass: 'btn-danger',
                                cancelButton: 		'Cancel',
                                cancelButtonClass: 	'btn bgaaa coloraaa hoverbg555 hovercolorfff',
                                confirm: 			function(){

                                    $http.delete( ':api/affiliate/' + affiliate.id ).then(function( response ){
                                        if( !response.data.result ){

                                            $scope.success 	= '';
                                            $scope.errors 	= response.data.errors;

                                            $window.scrollTo(0,0);

                                        }else{

                                            $scope.errors 	= [];
                                            $scope.success 	= 'Affiliate ' + affiliate.firstname +' '+ affiliate.lastname + ' has been deleted.';

                                            $scope.load( 1 );

                                            $window.scrollTo(0,0);

                                        }
                                    });

                                }
                            });
                        }

                    }

                    //on change fields in search form
                    $scope.search = function( form ){

                        $location.search('');

                        Loader.remove( /:api\/affiliate*/ );

                        $scope.load( 1 , true );
                    }

                    //Load the First Page
                    $scope.load( ( $stateParams.page || 1 ) , false );

                })();




            }else
            if( Page.is( /^\/affiliate\/([0-9]+)\/award$/ ) && User.hasPermission([ 'affiliate' , 'affiliate.award' ])  ){

                /**
                 *
                 *	ROUTE: /affiliate/([0-9]+)?
                 *		- The Affiliates List
                 *
                 * 	Params (URL):
                 * 		- page: 		(INT) Affiliates Listing Pagination
                 *
                 **/
                Loader.get( ':api/affiliate/' + $stateParams.affiliateid + '/award', function( affiliate ){

                    Page.loading.end();

                    $scope.sending 			= false;
                    $scope.timer 			= null;
                    $scope.running 			= 'Updating';
                    $scope.action 			= 'edit';
                    $scope.title 			= 'Affiliate Awards';
                    $scope.button 			= 'Update Awards';
                    $scope.icon 			= 'pencil-square';
                    $scope.hasError 		= Page.hasError;
                    $scope.affiliate 		= affiliate.data.affiliate;
                    $scope.awards 		    = affiliate.data.awards;
                    $scope.errors 			= [];

                    // after change select's
                    $scope.save 			= function( from, to, index, form){
                        if( !$scope.sending ) {

                            $scope.sending = true;
                            if (!to) {
                                $scope.affiliate.awards.splice(index, 1);
                                to = 0;
                            }
                            if (!from) {
                                from = 0;
                            }
                            $http.put(
                                ':api/affiliate/award',
                                {
                                    'id': $scope.affiliate.id,
                                    'from': from,
                                    'to': to,
                                }
                            ).then(function (response) {
                                if (!response.data.result) {

                                    $scope.sending = false;
                                    $scope.success = '';
                                    $scope.errors = response.data.errors;

                                    $window.scrollTo(0, 0);

                                } else {

                                    form.$setPristine();

                                    $scope.sending = false;
                                    $scope.errors = [];
                                    $scope.doctor = {};
                                    $scope.success = 'The Awards has been updated';

                                    $window.scrollTo(0, 0);

                                }
                            });
                        }
                    };

                });




            }else{


                Page.loading.end();
                Page.error(404);


            }

        });
    }]);


})();