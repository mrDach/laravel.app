(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('CustomerController', [ '$window' , '$stateParams' , '$scope' , '$http' , '$location' , 'Config' , 'User' , 'Page' , 'Loader' , function( $window , $stateParams , $scope , $http , $location , Config , User , Page , Loader ){
        User.ready(function(){

            if( Page.is( /^\/affiliate\/customer\/add/ ) &&  User.hasPermission([ 'customer' , 'customer.create' ])  ){


                /**
                 *
                 *	ROUTE: /customer/add
                 *		- add the Customer
                 *
                 * 	Params (URL):
                 * 		n/a
                 *
                 **/

                Page.loading.start();


                $http.get(':api/affiliate/0/1?q=').then(function(affiliate){

                    Page.loading.end();


                    $scope.sending 			= false;
                    $scope.running 			= 'Saving';
                    $scope.action 			= 'insert';
                    $scope.title 			= 'Create new Customer';
                    $scope.button 			= 'Add Customer';
                    $scope.icon 			= 'pencil-square';
                    $scope.hasError 		= Page.hasError;
                    $scope.errors 			= [];
                    $scope.affiliate 		= affiliate.data;

                    //On Form Submit
                    $scope.submit 	= function( form ){
                        if( form.$valid && !$scope.sending ){

                            $scope.sending = true;

                            $http.put( ':api/customer' , $scope.customer ).then(function( response ){

                                $scope.sending 	= false;

                                if( !response.data.result ){

                                    $scope.success 	= '';

                                }else{

                                    form.$setPristine();

                                    $scope.success 	= 'The Customers have been saved';

                                }

                                $scope.errors 	= response.data.errors;


                                $window.scrollTo(0,0);


                            });

                        }
                    };

                    //On Date Select
                    $scope.dateSelect = function( customerForm , type , wrapper ){
                        customerForm[ type ].$dirty = true;
                        jQuery('[uib-dropdown-toggle]:nth(' + ( wrapper - 1 ) + ')').toggleClass('open');
                    };

                    //On change Affiliate
                    $scope.getAffiliate = function(affiliate_name) {
                        //get first 5 Affiliates
                        return $http.get(':api/affiliate/5/1?q='+affiliate_name).then(function(response){
                            return response.data.data;
                        });
                    };

                });

                Page.loading.end();








            }else
            if( Page.is( /^\/affiliate\/customer(\/[0-9]+)?$/ ) && User.hasPermission([ 'customer' ])  ){

                /**
                 *
                 *	ROUTE: /customer/([0-9]+)?
                 *		- The Customer List
                 *
                 * 	Params (URL):
                 * 		- page: 		(INT) Customer Listing Pagination
                 *
                 **/
                (function(){

                    $scope.page.data 		= [];
                    $scope.list 			= {
                        groups:             null,
                        loading: 	        false,
                    };

                    //On Page Load
                    $scope.load 			= function( page ){

                        var queries 		= [];
                        var limit 			= $scope.page.showing;

                        Loader.get( ':api/customer' + ( limit ? '/' + limit : '' ) + ( page ? '/' + page : '' ) + ( queries.length > 0 ? '?' + queries.join('&') : '' ) , function( list ){
                            if( list ){

                                if( list.data.data.length > 0 || page == 1 ){

                                    if( $scope.page.current != page ){

                                        $location.path( '/affiliate/customer' + ( page > 1 ? '/' + page : '' ) );

                                    }

                                    $.extend( $scope.page , { current: page }, list.data );

                                }else{

                                    Page.error(404);

                                }

                                Page.loading.end();

                                $scope.list.loading = false;

                            }
                        });

                    };

                    //On "Click Delete"
                    $scope.delete 		= function( customer ){
                        if( User.hasPermission([ 'customer.delete' ]) ){

                            jQuery.confirm({
                                title: 				'Are you sure?',
                                content: 			'Are you sure you want to delete customer ' + customer.firstname +' '+ customer.lastname,
                                confirmButton: 		'Delete customer',
                                confirmButtonClass: 'btn-danger',
                                cancelButton: 		'Cancel',
                                cancelButtonClass: 	'btn bgaaa coloraaa hoverbg555 hovercolorfff',
                                confirm: 			function(){

                                    $http.delete( ':api/customer/' + customer.id ).then(function( response ){
                                        if( !response.data.result ){

                                            $scope.success 	= '';
                                            $scope.errors 	= response.data.errors;

                                            $window.scrollTo(0,0);

                                        }else{

                                            $scope.errors 	= [];
                                            $scope.success 	= 'customer ' + customer.firstname +' '+ customer.lastname + ' has been deleted.';

                                            $scope.load( 1 );

                                            $window.scrollTo(0,0);

                                        }
                                    });

                                }
                            });
                        }

                    }

                    //Load the First Page
                    $scope.load( ( $stateParams.page || 1 ) );

                })();




            }else{


                Page.loading.end();
                Page.error(404);


            }

        });
    }]);


})();