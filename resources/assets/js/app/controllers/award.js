(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('AwardController', [ '$window' , '$stateParams' , '$scope' , '$http' , '$location' , 'Config' , 'User' , 'Page' , 'Store' , 'Loader' , 'Upload' , function( $window , $stateParams , $scope , $http , $location , Config , User , Page , Store , Loader , Upload){
        User.ready(function(){

            if( Page.is( /^\/affiliate\/award\/edit\/([0-9]+)$/ ) && User.hasPermission([ 'award' , 'award.edit' ]) ){




                /**
                 *
                 *	ROUTE: /award/edit/([0-9]+)
                 *		- Edit the Award
                 *
                 * 	Params (URL):
                 * 		- awardid: 		(INT) The Award ID to Edit
                 *
                 **/

                Page.loading.start();

                Loader.get( ':api/award/' + $stateParams.awardid , function( award ){

                    Loader.get( ':api/group/all'  , function( groups ) {

                        Loader.get( ':api/affiliate/branches'  , function( branches ) {

                            Loader.get( ':api/affiliate/departments'  , function( departments ) {

                                Page.loading.end();

                                $scope.sending 			= false;
                                $scope.timer 			= null;
                                $scope.running 			= 'Updating';
                                $scope.action 			= 'edit';
                                $scope.title 			= 'Edit Award';
                                $scope.button 			= 'Edit Award';
                                $scope.icon 			= 'pencil-square';
                                $scope.hasError 		= Page.hasError;
                                $scope.award 			= award.data;
                                $scope.errors 			= [];
                                $scope.affiliate_groups = groups.data.groups;

                                $scope.branches	= {
                                    data: 					branches.data.data,
                                    settings: 				{
                                        displayProp: 				'name',
                                        //smartButtonMaxItems: 		3,
                                        smartButtonTextConverter: 	function(itemText, originalItem){ return itemText; }
                                    },
                                    events: 				{
                                        onItemSelect: 				function(){ $scope.awardForm.branches.$dirty = $scope.award.branches.length; },
                                        onItemDeselect: 			function(){ $scope.awardForm.branches.$dirty = $scope.award.branches.length; },
                                        onSelectAll: 				function(){ $scope.awardForm.branches.$dirty = $scope.award.branches.length; },
                                        onDeselectAll: 				function(){ $scope.awardForm.branches.$dirty = false; }
                                    }
                                };
                                $scope.departments = {
                                    data: 					departments.data.data,
                                    settings: 				{
                                        displayProp: 				'name',
                                        //smartButtonMaxItems: 		3,
                                        smartButtonTextConverter: 	function(itemText, originalItem){ return itemText; }
                                    },
                                    events: 				{
                                        onItemSelect: 				function(){ $scope.awardForm.departments.$dirty = $scope.award.departments.length; },
                                        onItemDeselect: 			function(){ $scope.awardForm.departments.$dirty = $scope.award.departments.length; },
                                        onSelectAll: 				function(){ $scope.awardForm.departments.$dirty = $scope.award.departments.length; },
                                        onDeselectAll: 				function(){ $scope.awardForm.departments.$dirty = false; }
                                    }
                                };

                                //On Form Submit
                                $scope.submit 	= function( form ){
                                    if( form.$valid && !$scope.sending ){

                                        $scope.sending = true;

                                        $http.post( ':api/award/' + $stateParams.awardid , $scope.award ).then(function( response ){
                                            if( !response.data.result ){

                                                $scope.sending 	= false;
                                                $scope.success 	= '';
                                                $scope.errors 	= response.data.errors;

                                                $window.scrollTo(0,0);

                                            }else{

                                                form.$setPristine();

                                                $scope.sending 	= false;
                                                $scope.errors 	= [];
                                                $scope.success 	= 'The Award has been updated';

                                                $window.scrollTo(0,0);

                                            }

                                            Loader.get( ':api/award/' + $stateParams.awardid , function( award ){
                                                $scope.award = award.data;
                                            });

                                        });

                                    }
                                };

                            });
                        });
                    });
                });








            }else
            if( Page.is( /^\/affiliate\/award\/add$/ ) &&  User.hasPermission([ 'award' , 'award.create' ])  ){


                /**
                 *
                 *	ROUTE: /award/add
                 *		- Add a New award
                 *
                 * 	Params (URL):
                 * 		n/a
                 *
                 **/

                Page.loading.start();

                Loader.get( ':api/group/all'  , function( groups ) {

                    Loader.get( ':api/affiliate/branches'  , function( branches ) {

                        Loader.get( ':api/affiliate/departments'  , function( departments ) {

                            Page.loading.end();

                            $scope.sending = false;
                            $scope.timer = null;
                            $scope.saving = false;
                            $scope.running = 'Creating';
                            $scope.action = 'insert';
                            $scope.title = 'Add Award';
                            $scope.button = 'Add Award';
                            $scope.icon = 'plus-circle';
                            $scope.hasError = Page.hasError;
                            $scope.errors = [];
                            $scope.affiliate_groups  = groups.data.groups;
                            $scope.award.branches  = [];
                            $scope.award.departments  = [];
                            $scope.branches	= {
                                data: 					branches.data.data,
                                settings: 				{
                                    displayProp: 				'name',
                                    //smartButtonMaxItems: 		3,
                                    smartButtonTextConverter: 	function(itemText, originalItem){ return itemText; }
                                },
                                events: 				{
                                    onItemSelect: 				function(){ $scope.awardForm.branches.$dirty = $scope.award.branches.length; },
                                    onItemDeselect: 			function(){ $scope.awardForm.branches.$dirty = $scope.award.branches.length; },
                                    onSelectAll: 				function(){ $scope.awardForm.branches.$dirty = $scope.award.branches.length; },
                                    onDeselectAll: 				function(){ $scope.awardForm.branches.$dirty = false; }
                                }
                            };
                            $scope.departments = {
                                data: 					departments.data.data,
                                settings: 				{
                                    displayProp: 				'name',
                                    //smartButtonMaxItems: 		3,
                                    smartButtonTextConverter: 	function(itemText, originalItem){ return itemText; }
                                },
                                events: 				{
                                    onItemSelect: 				function(){ $scope.awardForm.departments.$dirty = $scope.award.departments.length; },
                                    onItemDeselect: 			function(){ $scope.awardForm.departments.$dirty = $scope.award.departments.length; },
                                    onSelectAll: 				function(){ $scope.awardForm.departments.$dirty = $scope.award.departments.length; },
                                    onDeselectAll: 				function(){ $scope.awardForm.departments.$dirty = false; }
                                }
                            };

                            //On Form Submit
                            $scope.submit = function (form) {
                                if (form.$valid && !$scope.sending) {

                                    $scope.sending = true;

                                    $http.put(':api/award', $scope.award).then(function (response) {
                                        if (!response.data.result) {


                                            $scope.sending = false;
                                            $scope.success = '';
                                            $scope.errors = response.data.errors;

                                            $window.scrollTo(0, 0);

                                        } else {

                                            form.$setPristine();

                                            $scope.sending = false;
                                            $scope.errors = [];
                                            $scope.doctor = {};
                                            $scope.success = 'The Award has been Created';

                                            $window.scrollTo(0, 0);

                                        }
                                    });

                                }
                            };

                            //On Date Select
                            $scope.dateSelect = function( awardForm , type , wrapper ){
                                awardForm[ type ].$dirty = true;
                                jQuery('[uib-dropdown-toggle]:nth(' + ( wrapper - 1 ) + ')').toggleClass('open');
                            }
                        });
                    });
                });


            }else
            if( Page.is( /^\/affiliate\/award(\/[0-9]+)?$/ ) && User.hasPermission([ 'award' ])  ){


                /**
                 *
                 *	ROUTE: /award/([0-9]+)?
                 *		- The award List
                 *
                 * 	Params (URL):
                 * 		- page: 		(INT) Awards Listing Pagination
                 *
                 **/
                (function(){

                    $scope.page.data 		= [];
                    $scope.query 			= '';
                    $scope.list 			= { query: '', loading: false };

                    //On Page Load
                    $scope.load 			= function( page ){

                        Page.loading.start();

                        var limit 		= $scope.page.showing;

                        Loader.get( ':api/award' + ( limit ? '/' + limit : '' ) + ( page ? '/' + page : '' ) , function( awards ){
                            if( awards ){

                                if( awards.data.data.length > 0 || page == 1 ){

                                    if( $scope.page.current != page ){

                                        $location.path( '/affiliate/award' + ( page > 1 ? '/' + page : '' ) );

                                    }

                                    $.extend( $scope.page , { current: page }, awards.data );

                                }

                                Page.loading.end();

                                $scope.list.loading = false;

                            }
                        });

                    };

                    //On "Click Delete"
                    $scope.delete 		= function( award ){
                        if( User.hasPermission([ 'award.delete' ]) ){

                            jQuery.confirm({
                                title: 				'Are you sure?',
                                content: 			'Are you sure you want to delete award ' + award.name,
                                confirmButton: 		'Delete Award',
                                confirmButtonClass: 'btn-danger',
                                cancelButton: 		'Cancel',
                                cancelButtonClass: 	'btn bgaaa coloraaa hoverbg555 hovercolorfff',
                                confirm: 			function(){

                                    $http.delete( ':api/award/' + award.id ).then(function( response ){
                                        if( !response.data.result ){

                                            $scope.success 	= '';
                                            $scope.errors 	= response.data.errors;

                                            $window.scrollTo(0,0);

                                        }else{

                                            $scope.errors 	= [];
                                            $scope.success 	= 'Award ' + award.name + ' has been deleted.';

                                            $scope.load( 1 );

                                            $window.scrollTo(0,0);

                                        }
                                    });

                                }
                            });
                        }

                    }

                    //Load the First Page
                    $scope.load( ( $stateParams.page || 1 ) );

                })();


            }else{


                Page.loading.end();
                Page.error(404);


            }

        });
    }]);


})();