(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('CourseController', [ '$window' , '$stateParams' , '$scope' , '$http' , '$location', '$interval' , 'Config' , 'User' , 'Page' , 'Store' , 'Loader' , 'Upload' , function( $window , $stateParams , $scope , $http , $location , $interval , Config , User , Page , Store , Loader , Upload){
        User.ready(function(){

            if( Page.is( /^\/course\/edit\/([0-9]+)$/ ) && User.hasPermission([ 'course' , 'course.edit' ]) ){

                /**
                 *
                 *	ROUTE: /course/edit/([0-9]+)
                 *		- Edit the Course
                 *
                 * 	Params (URL):
                 * 		- courseid: 		(INT) The Course ID to Edit
                 *
                 **/

                Page.loading.start();

                Loader.get( ':api/course/' + $stateParams.courseid , function( course ){

                    Loader.get( ':api/stores'  , function( stores ) {

                        Loader.get( ':api/roles'  , function( roles ) {

                            Page.loading.end();

                            $scope.sending = false;
                            $scope.timer = null;
                            $scope.saving = false;
                            $scope.running = 'Updating';
                            $scope.action = 'edit';
                            $scope.title = 'Edit Course';
                            $scope.button = 'Edit Course';
                            $scope.icon = 'pencil-square';
                            $scope.hasError = Page.hasError;
                            $scope.errors = [];
                            $scope.files = [];
                            $scope.course = course.data;
                            $scope.stores	= {
                                data: 					stores.data,
                                settings: 				{
                                    displayProp: 				'name',
                                    smartButtonTextConverter: 	function(itemText, originalItem){ return itemText; }
                                },
                                events: 				{
                                    onItemSelect: 				function(){ $scope.courseForm.stores.$dirty = $scope.course.stores.length; },
                                    onItemDeselect: 			function(){ $scope.courseForm.stores.$dirty = $scope.course.stores.length; },
                                    onSelectAll: 				function(){ $scope.courseForm.stores.$dirty = $scope.course.stores.length; },
                                    onDeselectAll: 				function(){ $scope.courseForm.stores.$dirty = false; }
                                }
                            };
                            $scope.roles = {
                                data: 					roles.data,
                                settings: 				{
                                    displayProp: 				'name',
                                    smartButtonTextConverter: 	function(itemText, originalItem){ return itemText; }
                                },
                                events: 				{
                                    onItemSelect: 				function(){ $scope.courseForm.roles.$dirty = $scope.course.roles.length; },
                                    onItemDeselect: 			function(){ $scope.courseForm.roles.$dirty = $scope.course.roles.length; },
                                    onSelectAll: 				function(){ $scope.courseForm.roles.$dirty = $scope.course.roles.length; },
                                    onDeselectAll: 				function(){ $scope.courseForm.roles.$dirty = false; }
                                }
                            };

                            //On Form Submit
                            $scope.submit = function (form, silent) {
                                if (form.$valid && !$scope.sending || silent) {

                                    $scope.sending = true;

                                    $http.post( ':api/course/' + $stateParams.courseid , $scope.course ).then(function( response ){
                                        if (!response.data.result) {


                                            $scope.sending = false;
                                            $scope.success = '';
                                            $scope.errors = response.data.errors;

                                            $window.scrollTo(0, 0);

                                        } else {

                                            form.$setPristine();

                                            $scope.sending = false;
                                            $scope.errors = [];
                                            $scope.course = response.data.course;
                                            if(!silent){
                                                $scope.success = 'The Course has been Updated';
                                            }

                                        }
                                    });

                                }
                            };

                            $scope.delete = function () {

                                $scope.sending = true;

                                $http.delete( ':api/course/' + $stateParams.courseid ).then(function( response ){
                                    if (!response.data.result) {


                                        $scope.sending = false;
                                        $scope.success = '';
                                        $scope.errors = response.data.errors;

                                        $window.scrollTo(0, 0);

                                    } else {

                                        form.$setPristine();

                                        $scope.sending = false;
                                        $scope.errors = [];
                                        $scope.course        = {};
                                        $scope.course.pages  = [];
                                        $scope.course.tests  = [];
                                        $scope.course.stores = [];
                                        $scope.course.roles  = [];

                                    }
                                });

                            };

                            $scope.checkAnswer = function(form,test_id, answer_id) {
                                if (form['test-' + test_id + '-answer-' + answer_id + '-text'] && (form['test-' + test_id + '-answer-' + answer_id + '-text'].$invalid && form['test-' + test_id + '-answer-' + answer_id + '-text'].$dirty)) {
                                    form.error['test-' + test_id + '-answer'] =  true;
                                    form.error['test-' + test_id + '-answer-' + answer_id + '-text'] = true;
                                    return true;
                                } else {
                                    form.error['test-' + test_id + '-answer-' + answer_id + '-text'] = false;
                                    return false;
                                }

                            };

                            //Show already uploaded files
                            $scope.uploaderInit = function( page_index ){
                                $scope.files[page_index] = [];
                                if($scope.course.pages[page_index].attachment){
                                    $scope.course.pages[page_index].attachment.name = $scope.course.pages[page_index].attachment.filename;
                                    $scope.files[page_index].push($scope.course.pages[page_index].attachment);
                                }
                            };

                            //remove file and reset attachment_id
                            $scope.remove = function( file , file_index , page_index){
                                $scope.files[page_index].splice( file_index , 1 );
                                if( file.checked && $scope.files[page_index].length > 0 ){
                                    $scope.files[page_index][0].checked = true;
                                    $scope.course.pages[page_index].attachment_id 	= $scope.files[page_index][0].attachment_id;
                                }else{
                                    $scope.course.pages[page_index].attachment_id 	= null;
                                }
                            };

                            //On add file
                            $scope.upload = function( files , page_index){

                                $scope.sending = true;

                                if( files && files.length ){

                                    for( var i=0; i < files.length; i++ ){
                                        if( !files[i].status ){
                                            (function( file ){

                                                Upload.upload({

                                                    url: 	':api/course/attach',

                                                    data: 	{ 'file': file },

                                                    method: 'POST',

                                                }).then(function(resp){
                                                    if( resp.data.result ){

                                                        file.status 		= 'success';
                                                        $scope.course.pages[page_index].attachment_id = resp.data.attachment_id;
                                                        file.custom_name 	= file.name;

                                                    }else{

                                                        file.status = 'error';

                                                    }

                                                    $scope.sending = false;

                                                },function(resp){

                                                    file.status = 'error';

                                                    $scope.sending = false;

                                                },function(evt){

                                                    file.status 	= 'running';
                                                    file.progress 	= Math.min(100 , parseInt( 100 * evt.loaded / evt.total ) );

                                                });

                                            })( files[i] );
                                        }
                                    }

                                }
                            };
                        });
                    });
                });

            }else
            if( Page.is( /^\/course\/take\/([0-9]+)$/ ) &&  User.hasPermission([ 'course' ])  ){

                /**
                 *
                 *	ROUTE: /course/take/([0-9]+)
                 *		- Take the Course
                 *
                 * 	Params (URL):
                 * 		- courseid: 		(INT) The Course ID to Take
                 *
                 **/

                Page.loading.start();

                Loader.get( ':api/course/take/' + $stateParams.courseid , function( course ){

                    Page.loading.end();

                    $scope.button = 'Edit Course';
                    $scope.errors = [];
                    $scope.course = course.data;
                    $scope.page = {
                        type:'take',    //take/exam/result
                        tab:    0,
                        tests:  [],
                        result: [],
                        timer:  null,   //time for test
                        interval:  0,   //$interval
                    };
                    $scope.answer = {
                        test_id:0,
                        answer_id:0,
                    };

                    //get next Course page
                    $scope.getPage = function (tab_index) {

                        //nosing if start testing
                        if(($scope.page.tests.length == 0) && ($scope.course.pages[tab_index])) {

                            if($scope.page.type == 'result'){
                                $scope.page.type = 'take';
                            }
                            $scope.page.tab = tab_index;

                        }
                    };

                    //get Tests
                    $scope.getExam = function () {

                        $scope.running = 'Getting Tests';

                        $scope.sending = true;

                        Page.loading.start();

                        Loader.get(':api/course/exam/' + $stateParams.courseid, function (tests) {
                            Page.loading.end();
                            //tests.data.result = 0 if error or Tests for this Course already done
                            if (!tests.data.result) {

                                $scope.sending = false;
                                $scope.success = '';

                                $scope.errors = tests.data.errors;

                                //if Test done show results
                                if(tests.data.results){
                                    $.extend($scope.page, {
                                        type: 'result',
                                        tab: 0,
                                        tests:  [],
                                        timer: null,
                                        result: tests.data.results
                                    });
                                }

                                $window.scrollTo(0, 0);


                            } else {

                                $scope.sending = false;

                                //show Tests
                                $.extend($scope.page, {
                                    type: 'exam',
                                    tab: 0,
                                    tests: tests.data.tests
                                });

                                //set current test_id for answer
                                $scope.answer.test_id = $scope.page.tests[$scope.page.tab].id;

                                //start timer for test
                                $scope.timer($scope.page.tests[$scope.page.tab].time);

                                $window.scrollTo(0, 0);

                            }
                        });
                    };

                    //set timer for test
                    $scope.timer = function (time) {
                        var t = time.split(':');
                        $scope.page.timer = 0;

                        if(parseInt(t[0], 10))
                            $scope.page.timer += parseInt(t[0], 10)*60*60*1000;

                        if(parseInt(t[1], 10))
                            $scope.page.timer += parseInt(t[1], 10)*60*1000;

                        if(parseInt(t[2], 10))
                            $scope.page.timer += parseInt(t[2], 10)*1000;

                        $scope.page.interval = $interval(function () {
                            if($scope.page.timer>0){
                                $scope.page.timer -= 1000;
                            }else {
                                $scope.errors = ['Time out!'];
                                //Time out! send empty answer
                                $scope.answer.answer_id = 0;
                                $scope.sendAnswer();
                            }
                        }, 1000);
                    };

                    $scope.sendAnswer = function () {

                        $scope.running = 'Sending Answer';
                        $scope.sending = true;

                        $http.post( ':api/course/exam/' + $stateParams.courseid , $scope.answer ).then(function( response ){
                            if (!response.data.result) {

                                $scope.sending = false;
                                $scope.success = '';
                                $scope.errors = response.data.errors;

                                $window.scrollTo(0, 0);

                            } else {

                                $scope.sending = false;
                                $scope.success = 'The Answer has been Saved!';

                                //stop timer for test
                                $interval.cancel($scope.page.interval);

                                if($scope.page.tab < $scope.page.tests.length - 1){

                                    //go to next test
                                    $scope.page.tab++;
                                    $scope.answer.test_id = $scope.page.tests[$scope.page.tab].id;
                                    $scope.timer($scope.page.tests[$scope.page.tab].time);

                                }else{
                                    //test end get result
                                    $scope.getExam();
                                }

                                $window.scrollTo(0, 0);

                            }
                        });
                    }

                    $scope.view = function( file ){

                        if( file.mime.type == 'video' ){

                            jQuery.dialog({
                                title: 				file.name,
                                content: 			'<video controls preload="auto" width="100%" height="100%"> <source src="' + media.file + '" type="' + media.mime.type + '/' + media.mime.subtype + '" /> </video>'
                            });

                        }else{

                            jQuery.dialog({
                                title: 				file.name,
                                content: 			'<div class="clearfix">' + '<img src="' + file.image + '" />' + '<a href="' + file.image + '" target="_blank" class="btn btn-primary fright">Open in New Window <small>(Full Size)</small></a>' + '</div>'
                            });

                        }

                    }

                });

            }else
            if( Page.is( /^\/course\/add$/ ) &&  User.hasPermission([ 'course' , 'course.create' ])  ){


                /**
                 *
                 *	ROUTE: /course/add
                 *		- Add a New course
                 *
                 * 	Params (URL):
                 * 		n/a
                 *
                 **/

                Page.loading.start();

                Loader.get( ':api/course'  , function( course ) {

                    Loader.get( ':api/stores'  , function( stores ) {

                        Loader.get( ':api/roles'  , function( roles ) {

                            Page.loading.end();

                            $scope.sending       = false;
                            $scope.timer         = null;
                            $scope.saving        = false;
                            $scope.running       = 'Creating';
                            $scope.action        = 'insert';
                            $scope.title         = 'Add Course';
                            $scope.button        = 'Save';
                            $scope.icon          = 'plus-circle';
                            $scope.hasError      = Page.hasError;
                            $scope.errors        = [];
                            $scope.course        = course.data||{};
                            $scope.files         = [];
                            $scope.course.pages  = $scope.course.pages||[];
                            $scope.course.tests  = $scope.course.tests||[];
                            $scope.course.stores = $scope.course.stores||[];
                            $scope.course.roles  = $scope.course.roles||[];
                            $scope.stores	= {
                                data: 					stores.data,
                                settings: 				{
                                    displayProp: 				'name',
                                    smartButtonTextConverter: 	function(itemText, originalItem){ return itemText; }
                                },
                                events: 				{
                                    onItemSelect: 				function(){ $scope.courseForm.stores.$dirty = $scope.course.stores.length; },
                                    onItemDeselect: 			function(){ $scope.courseForm.stores.$dirty = $scope.course.stores.length; },
                                    onSelectAll: 				function(){ $scope.courseForm.stores.$dirty = $scope.course.stores.length; },
                                    onDeselectAll: 				function(){ $scope.courseForm.stores.$dirty = false; }
                                }
                            };
                            $scope.roles = {
                                data: 					roles.data,
                                settings: 				{
                                    displayProp: 				'name',
                                    smartButtonTextConverter: 	function(itemText, originalItem){ return itemText; }
                                },
                                events: 				{
                                    onItemSelect: 				function(){ $scope.courseForm.roles.$dirty = $scope.course.roles.length; },
                                    onItemDeselect: 			function(){ $scope.courseForm.roles.$dirty = $scope.course.roles.length; },
                                    onSelectAll: 				function(){ $scope.courseForm.roles.$dirty = $scope.course.roles.length; },
                                    onDeselectAll: 				function(){ $scope.courseForm.roles.$dirty = false; }
                                }
                            };

                            //On Form Submit
                            $scope.submit = function (form, silent) {
                                if (form.$valid && !$scope.sending || silent) {

                                    $scope.sending = true;

                                    $http.put(':api/course', $scope.course).then(function (response) {
                                        if (!response.data.result) {


                                            $scope.sending = false;
                                            $scope.success = '';
                                            $scope.errors = response.data.errors;

                                            $window.scrollTo(0, 0);

                                        } else {

                                            form.$setPristine();

                                            $scope.sending = false;
                                            $scope.errors = [];
                                            $scope.course = response.data.course;
                                            if($scope.course && $scope.course.pages && $scope.course.pages.length > 0){
                                                for(var i = 0; i < $scope.course.pages.length; i++){
                                                    $scope.uploaderInit(i);
                                                }
                                            }
                                            if(!silent){
                                                $scope.success = 'The Course has been Created';
                                                $location.path( '/course' );
                                            }

                                        }
                                    });

                                }
                            };

                            $scope.delete = function (form) {

                                if($scope.course.id){
                                    $scope.sending = true;
                                    $http.delete( ':api/course/' + $scope.course.id ).then(function( response ){
                                        if (!response.data.result) {


                                            $scope.sending = false;
                                            $scope.success = '';
                                            $scope.errors = response.data.errors;

                                            $window.scrollTo(0, 0);

                                        } else {

                                            form.$setPristine();

                                            $scope.sending       = false;
                                            $scope.errors        = [];
                                            $scope.course        = {};
                                            $scope.course.pages  = [];
                                            $scope.course.tests  = [];
                                            $scope.course.stores = [];
                                            $scope.course.roles  = [];

                                        }
                                    });
                                }else {

                                    $scope.course        = {};
                                    $scope.course.pages  = [];
                                    $scope.course.tests  = [];
                                    $scope.course.stores = [];
                                    $scope.course.roles  = [];
                                }

                            };

                            $scope.uploaderInit = function( page_index ){
                                $scope.files[page_index] = [];
                                if($scope.course.pages[page_index].attachment){
                                    $scope.course.pages[page_index].attachment.name = $scope.course.pages[page_index].attachment.filename;
                                    $scope.files[page_index].push($scope.course.pages[page_index].attachment);
                                }
                            };

                            $scope.remove = function( file , file_index , page_index){
                                $scope.files[page_index].splice( file_index , 1 );
                                if( file.checked && $scope.files[page_index].length > 0 ){
                                    $scope.files[page_index][0].checked = true;
                                    $scope.course.pages[page_index].attachment_id 	= $scope.files[page_index][0].attachment_id;
                                }else{
                                    $scope.course.pages[page_index].attachment_id 	= null;
                                }
                            };

                            $scope.checkAnswer = function(form,test_id, answer_id) {
                                if (form['test-' + test_id + '-answer-' + answer_id + '-text'] && (form['test-' + test_id + '-answer-' + answer_id + '-text'].$invalid && form['test-' + test_id + '-answer-' + answer_id + '-text'].$dirty)) {
                                    form.error['test-' + test_id + '-answer'] =  true;
                                    form.error['test-' + test_id + '-answer-' + answer_id + '-text'] = true;
                                    return true;
                                } else {
                                    form.error['test-' + test_id + '-answer-' + answer_id + '-text'] = false;
                                    return false;
                                }

                            };

                            //On add file
                            $scope.upload = function( files , page_index){

                                $scope.sending = true;

                                if( files && files.length ){

                                    for( var i=0; i < files.length; i++ ){
                                        if( !files[i].status ){
                                            (function( file ){

                                                Upload.upload({

                                                    url: 	':api/course/attach',

                                                    data: 	{ 'file': file },

                                                    method: 'POST',

                                                }).then(function(resp){
                                                    if( resp.data.result ){

                                                        file.status 		= 'success';
                                                        $scope.course.pages[page_index].attachment_id = resp.data.attachment_id;
                                                        file.custom_name 	= file.name;

                                                    }else{

                                                        file.status = 'error';

                                                    }

                                                    $scope.sending = false;

                                                },function(resp){

                                                    file.status = 'error';

                                                    $scope.sending = false;

                                                },function(evt){

                                                    file.status 	= 'running';
                                                    file.progress 	= Math.min(100 , parseInt( 100 * evt.loaded / evt.total ) );

                                                });

                                            })( files[i] );
                                        }
                                    }

                                }
                            };
                        });
                    });
                });


            }else
            if( Page.is( /^\/course(\/[0-9]+)?$/ ) && User.hasPermission([ 'course' ])  ){


                /**
                 *
                 *	ROUTE: /course/([0-9]+)?
                 *		- The course List
                 *
                 * 	Params (URL):
                 * 		- page: 		(INT) Courses Listing Pagination
                 *
                 **/
                (function(){

                    $scope.page.data 		= [];
                    $scope.query 			= '';
                    $scope.list 			= { query: '', loading: false };

                    //On Page Load
                    $scope.load 			= function( page ){

                        Page.loading.start();

                        var limit 		= $scope.page.showing;

                        Loader.get( ':api/course' + ( limit ? '/' + limit : '' ) + ( page ? '/' + page : '' ) , function( courses ){
                            if( courses ){

                                if( courses.data.data.length > 0 || page == 1 ){

                                    if( $scope.page.current != page ){

                                        $location.path( '/course' + ( page > 1 ? '/' + page : '' ) );

                                    }

                                    $.extend( $scope.page , { current: page }, courses.data );

                                }

                                Page.loading.end();

                                $scope.list.loading = false;

                            }
                        });

                    };

                    //On "Click Delete"
                    $scope.delete 		= function( course ){
                        if( User.hasPermission([ 'course.delete' ]) ){

                            jQuery.confirm({
                                title: 				'Are you sure?',
                                content: 			'Are you sure you want to delete course ' + course.name,
                                confirmButton: 		'Delete Course',
                                confirmButtonClass: 'btn-danger',
                                cancelButton: 		'Cancel',
                                cancelButtonClass: 	'btn bgaaa coloraaa hoverbg555 hovercolorfff',
                                confirm: 			function(){

                                    $http.delete( ':api/course/' + course.id ).then(function( response ){
                                        if( !response.data.result ){

                                            $scope.success 	= '';
                                            $scope.errors 	= response.data.errors;

                                            $window.scrollTo(0,0);

                                        }else{

                                            $scope.errors 	= [];
                                            $scope.success 	= 'Course ' + course.name + ' has been deleted.';

                                            $scope.load( 1 );

                                            $window.scrollTo(0,0);

                                        }
                                    });

                                }
                            });
                        }

                    }

                    //Load the First Page
                    $scope.load( ( $stateParams.page || 1 ) );

                })();


            }else{


                Page.loading.end();
                Page.error(404);


            }

        });
    }]);


})();