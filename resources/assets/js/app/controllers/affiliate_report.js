(function(){

	'use strict';

	var app = angular.module('System.Controllers');

	//The App Controller
	app.controller('AffiliateReportController', [ '$window' , '$stateParams' , '$scope' , '$http' , '$location' , 'Config' , 'User' , 'Store' , 'Page' , 'Loader' , 'Upload' , function( $window , $stateParams , $scope , $http , $location , Config , User , Store , Page , Loader , Upload){
		User.ready(function(){


			if( Page.is( /^\/affiliate\/reports(\/[0-9]+)?$/ ) && User.hasPermission([ 'affiliate.reports' ])  ){

				/**
				*
				*	ROUTE: /reports/([0-9]+)?
				*		- The Menu List
				* 	
				* 	Params (URL):
				* 		- page: 		(INT) Reports Listing Pagination
				*
				**/
				(function(){

					$scope.page.data 		= [];
					$scope.hasError 		= Page.hasError;
					$scope.errors 			= [];
					$scope.list 			= {
						query: 		        ( $location.search().q || null ),
						sort: 		        ( $location.search().sort || null ),
						start: 		        ( $location.search().start || null ),
						end: 		        ( $location.search().end || null ),
						loading: 	        false,
					};
					
					//On Page Load
					$scope.load 			= function( page , searching ){

						var queries 		= [];
						var limit 			= $scope.page.showing;

						if( $scope.list.query ) 	queries.push( 'q=' + encodeURIComponent( $scope.list.query ) );
						if( $scope.list.sort ) 		queries.push( 'sort=' + encodeURIComponent( $scope.list.sort ) );
						if( $scope.list.start ) 	queries.push( 'start=' + encodeURIComponent( $scope.list.start ) );
						if( $scope.list.end ) 		queries.push( 'end=' + encodeURIComponent( $scope.list.end ) );

						if( !searching ){

							Page.loading.start();

						}else{

							$scope.list.loading = true;

						}

						Loader.get( ':api/affiliate_reports' + ( limit ? '/' + limit : '' ) + ( page ? '/' + page : '' ) + ( queries.length > 0 ? '?' + queries.join('&') : '' ) , function( reports ){
							if( reports ){

								if( reports.data.data.length > 0 || page == 1 ){

									if( $scope.page.current != page ){

										$location.path( '/affiliate/reports' + ( page > 1 ? '/' + page : '' ) );

									}

									$.extend( $scope.page , { current: page }, reports.data );

								}else{

									Page.error(404);

								}

								Page.loading.end();

								$scope.list.loading = false;

							}
						});

					};

					$scope.dateSelect 		= function( searchForm , type , wrapper ){
						searchForm[ type ].$dirty = true;
						jQuery('[uib-dropdown-toggle]:nth(' + ( wrapper - 1 ) + ')').removeClass('open');
						$scope.search( searchForm );
					}

					//on change fields in search form
					$scope.search = function( form ){

						$location.search('');

						Loader.remove( /:api\/affiliate_reports*/ );

						$scope.load( 1 , true );

					}

					//Load the First Page
					$scope.load( ( $stateParams.page || 1 ) , false );

				})();




			}else{



				Page.loading.end();
				Page.error(404);


			}

		});
	}]);


})();