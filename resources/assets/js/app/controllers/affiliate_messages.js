(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('AffiliateMessageSystemController', [ '$window' , '$stateParams' , '$scope' , '$http' , '$location' , 'Config' , 'User' , 'Page' , 'Loader' , function( $window , $stateParams , $scope , $http , $location , Config , User , Page , Loader ){
        User.ready(function(){

            if( Page.is( /^\/affiliate\/message\/add/ ) &&  User.hasPermission([ 'affiliate.message' , 'affiliate.message.create' ])  ){


                /**
                 *
                 *	ROUTE: /message/add
                 *		- add the Affiliate Message
                 *
                 * 	Params (URL):
                 * 		n/a
                 *
                 **/

                Page.loading.start();


                $http.get(':api/affiliate/0/1?q=').then(function(affiliate){

                    Page.loading.end();


                    $scope.sending 			= false;
                    $scope.running 			= 'Saving';
                    $scope.action 			= 'insert';
                    $scope.title 			= 'Create new Message';
                    $scope.button 			= 'Add Message';
                    $scope.icon 			= 'pencil-square';
                    $scope.hasError 		= Page.hasError;
                    $scope.errors 			= [];

                    $scope.affiliate 		= affiliate.data;

                    //On Form Submit
                    $scope.submit 	= function( form ){
                        if( form.$valid && !$scope.sending ){

                            $scope.sending = true;

                            $http.put( ':api/message' , $scope.message ).then(function( response ){
                                if( !response.data.result ){

                                    $scope.sending 	= false;
                                    $scope.success 	= '';
                                    $scope.errors 	= response.data.errors;

                                    $window.scrollTo(0,0);

                                }else{

                                    form.$setPristine();

                                    $scope.sending 	= false;
                                    $scope.errors 	= [];
                                    $scope.success 	= 'The Message have been saved';

                                    $scope.message.affiliate = null;
                                    $scope.message.text = "";

                                    $window.scrollTo(0,0);
                                }
                            });

                        }
                    };

                    //On change Affiliate
                    $scope.getAffiliate = function(affiliate_name) {
                        //get first 5 Affiliates
                        return $http.get(':api/affiliate/5/1?q='+affiliate_name).then(function(response){
                            return response.data.data;
                        });
                    };

                });

                Page.loading.end();








            }else
            if( Page.is( /^\/affiliate\/message(\/[0-9]+)?$/ ) && User.hasPermission([ 'affiliate.message' ])  ){

                /**
                 *
                 *	ROUTE: /message/([0-9]+)?
                 *		- The Affiliates Message List
                 *
                 * 	Params (URL):
                 * 		- page: 		(INT) Affiliates Message Listing Pagination
                 *
                 **/
                (function() {

                    $scope.page.data 		= [];
                    $scope.hasError 		= Page.hasError;
                    $scope.errors 			= [];

                    $scope.list 			= {
                        start: 		        ( $location.search().start || null ),
                        end: 		        ( $location.search().end || null ),
                        loading: 	        false,
                    };

                    //On Page Load
                    $scope.load 			= function( page , searching ){

                        var queries 		= [];
                        var limit 			= $scope.page.showing;

                        if( $scope.list.start ) 	queries.push( 'start=' + encodeURIComponent( $scope.list.start ) );
                        if( $scope.list.end ) 		queries.push( 'end=' + encodeURIComponent( $scope.list.end ) );

                        if( !searching ){

                            Page.loading.start();

                        }else{

                            $scope.list.loading = true;

                        }

                        Loader.get( ':api/message' + ( limit ? '/' + limit : '' ) + ( page ? '/' + page : '' ) + ( queries.length > 0 ? '?' + queries.join('&') : '' ) , function( reports ){
                            if( reports ){

                                if( reports.data.data.length > 0 || page == 1 ){

                                    if( $scope.page.current != page ){

                                        $location.path( '/affiliate/message' + ( page > 1 ? '/' + page : '' ) );

                                    }

                                    $.extend( $scope.page , { current: page }, reports.data );

                                }else{

                                    Page.error(404);

                                }

                                Page.loading.end();

                                $scope.list.loading = false;

                            }
                        });

                    };

                    $scope.dateSelect 		= function( searchForm , type , wrapper ){
                        searchForm[ type ].$dirty = true;
                        jQuery('[uib-dropdown-toggle]:nth(' + ( wrapper - 1 ) + ')').removeClass('open');
                        $scope.search( searchForm );
                    }

                    //on change fields in search form
                    $scope.search = function( form ){

                        $location.search('');

                        Loader.remove( /:api\/message*/ );

                        $scope.load( 1 , true );

                    }

                    //Load the First Page
                    $scope.load( ( $stateParams.page || 1 ) , false );

                }());




            }else{


                Page.loading.end();
                Page.error(404);


            }

        });
    }]);


})();