
	
    @extends('layouts.email')
    @section('type', 'New Customer')
    @section('subject', '' )
    @section('content')

        <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; border:1px solid #DDD;">
            <thead>
                <tr>
                    <th style="border-collapse:collapse; border:1px solid #DDD; border-bottom-width:2px; padding:8px; font-family:'Helvetica';"></th>
                    <th style="border-collapse:collapse; border:1px solid #DDD; border-bottom-width:2px; padding:8px; font-family:'Helvetica';"></th>
                </tr>
            </thead>
            <tbody>
                <tr style="background-color:#F9F9F9;">
                    <td style="border-collapse:collapse; border:1px solid #DDD; padding:8px; font-family:'Helvetica';">                                                
                        <strong style="font-family:'Helvetica';">Firstname:</strong>
                    </td>
                    <td style="border-collapse:collapse; border:1px solid #DDD; padding:8px; font-family:'Helvetica';">
                        {{ $data['firstname'] }}
                    </td>
                </tr>
                <tr style="background-color:#F9F9F9;">
                    <td style="border-collapse:collapse; border:1px solid #DDD; padding:8px; font-family:'Helvetica';">
                        <strong style="font-family:'Helvetica';">Lastname:</strong>
                    </td>
                    <td style="border-collapse:collapse; border:1px solid #DDD; padding:8px; font-family:'Helvetica';">
                        {{ $data['lastname'] }}
                    </td>
                </tr>
                <tr style="background-color:#F9F9F9;">
                    <td style="border-collapse:collapse; border:1px solid #DDD; padding:8px; font-family:'Helvetica';">
                        <strong style="font-family:'Helvetica';">Date:</strong>
                    </td>
                    <td style="border-collapse:collapse; border:1px solid #DDD; padding:8px; font-family:'Helvetica';">
                        {{ $data['date'] }}
                    </td>
                </tr>
                <tr style="background-color:#F9F9F9;">
                    <td style="border-collapse:collapse; border:1px solid #DDD; padding:8px; font-family:'Helvetica';">
                        <strong style="font-family:'Helvetica';">Spent:</strong>
                    </td>
                    <td style="border-collapse:collapse; border:1px solid #DDD; padding:8px; font-family:'Helvetica';">
                        {{ $data['spent'] }}
                    </td>
                </tr>
            </tbody>
        </table>

	@stop
