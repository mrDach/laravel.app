
        <!-- components.manage-course -->
        <form name="courseForm" method="post" ng-submit="submit( courseForm )" novalidate>


            <!-- components.manage-course success -->
            <div class="bs-callout bs-callout-info" ng-show="success.length && courseForm.$pristine">
                <h4 class="font2">Success!</h4>
                <p>@{{ success }}</p>
            </div>


            <!-- components.manage-course error -->
            <div class="bs-callout bs-callout-danger" ng-show="hasError( courseForm ) || errors.length">
                <h4 class="font2">Form Errors</h4>
                <div>
                    <ul>

                        <!-- General Errors -->
                        <li ng-repeat="error in errors">
                            @{{ error }}
                        </li>

                        <!-- Customers Error -->
                        <li ng-show="courseForm.error[ 'passing_score' ] = courseForm.passing_score.$invalid && courseForm.passing_score.$dirty" ng-messages="courseForm.passing_score.$error">
                            <span ng-message="required">Passing Score is required</span>
                        </li>

                        <!-- Name Error -->
                        <li ng-show="courseForm.error[ 'name' ] = courseForm.name.$invalid && courseForm.name.$dirty" ng-messages="courseForm.name.$error">
                            <span ng-message="required">Name is required</span>
                        </li>

                        <!-- Description Error -->
                        <li ng-show="courseForm.error[ 'description' ] = courseForm.description.$invalid && courseForm.description.$dirty" ng-messages="courseForm.description.$error">
                            <span ng-message="required">Description is required</span>
                        </li>
                        
                        <!-- Page Error -->
                        <li ng-repeat="(page_index , page) in course.pages" ng-show="courseForm.error[ 'page-'+page.id+'-subject' ] || courseForm.error[ 'page-'+page.id+'-acticle' ]">
                            Page @{{ page_index + 1 }} Errors:
                            <ul>

                                <!-- subject -->
                                <li ng-show="courseForm.error[ 'page-'+page.id+'-subject' ] = ( courseForm[ 'page-'+page.id+'-subject' ].$invalid && courseForm[ 'page-'+page.id+'-subject' ].$dirty )" ng-messages="courseForm[ 'page-'+page.id+'-subject' ].$error">
                                    <span ng-message="required">Subject is required</span>
                                </li>

                                <!-- Time -->
                                <li ng-show="courseForm.error[ 'page-'+page.id+'-acticle' ] = ( courseForm[ 'page-'+page.id+'-acticle' ].$invalid && courseForm[ 'page-'+page.id+'-acticle' ].$dirty )" ng-messages="courseForm[ 'page-'+page.id+'-acticle' ].$error">
                                    <span ng-message="required">Acticle are required</span>
                                </li>

                            </ul>
                        </li>

                        <!-- Test Error -->
                        <li ng-repeat="(test_index , test) in course.tests" ng-show="courseForm.error[ 'test-'+test.id+'-question' ] || courseForm.error[ 'test-'+test.id+'-time' ] || courseForm.error[ 'test-'+test.id+'-answer' ]">
                            Test @{{ test_index + 1 }} Errors:
                            <ul>

                                <!-- Question -->
                                <li ng-show="courseForm.error[ 'test-'+test.id+'-question' ] = ( courseForm[ 'test-'+test.id+'-question' ].$invalid && courseForm[ 'test-'+test.id+'-question' ].$dirty )" ng-messages="courseForm[ 'test-'+test.id+'-question' ].$error">
                                    <span ng-message="required">Question is required</span>
                                </li>

                                <!-- Time -->
                                <li ng-show="courseForm.error[ 'test-'+test.id+'-time' ] = ( courseForm[ 'test-'+test.id+'-time' ].$invalid && courseForm[ 'test-'+test.id+'-time' ].$dirty )" ng-messages="courseForm[ 'test-'+test.id+'-time' ].$error">
                                    <span ng-message="required">Time are required</span>
                                    <span ng-message="time-invalid">Time invalid need hh:mm:ss</span>
                                    <span ng-message="time-lower-min">Time lower minimum (1 minute)</span>
                                </li>

                                <!-- Answer Error -->
                                <li ng-repeat="(answer_index , answer) in test.answers" ng-show="courseForm.error[ 'test-'+test.id+'-answer-'+answer.id+'-text' ]">
                                    Answer @{{ answer_index + 1 }} Errors:
                                    <ul>

                                        <!-- Text -->
                                        <li ng-show="checkAnswer(courseForm,test.id,answer.id)" ng-messages="courseForm[ 'test-'+test.id+'-answer-'+answer.id+'-text' ].$error">
                                            <span ng-message="required">Text is required</span>
                                        </li>

                                    </ul>
                                </li>

                            </ul>
                        </li>

                    </ul>
                </div>
            </div>


            <!-- components.manage-course -->
            <main id="course-management" class="border panel panel-default">
                <header class="font2 panel-heading">

                    <i class="fa fa-@{{ icon }}"></i>
                    @{{ title }}

                    <div class="form-group fright">

                        <!-- Submitting Wait Notice -->
                        <small class="colorinfo text-right fleft submitnotice" ng-show="sending">
                            <i class="fa fa-circle-o-notch fa-spin colorinfo"></i>
                            @{{ running }} course - Please wait.
                        </small>

                        <!-- Saving Wait Notice -->
                        <small class="colorinfo text-right fleft submitnotice" ng-show="saving">
                            <i class="fa fa-circle-o-notch fa-spin colorinfo"></i>
                            Saving - Please wait.
                        </small>

                        <!-- Form Error Notices -->
                        <small class="colorred text-right fleft submitnotice" ng-show="hasError( courseForm )">
                            <span ng-show="hasError( courseForm )">You must fix any form errors before proceeding * See Errors at Top *</span>
                        </small>

                        <!-- Submit Button -->
                        <button type="submit" name="submit" class="btn-sm fright" ng-click="course.published = 1" ng-class="hasError( courseForm ) || errors.length ? 'btn-danger' : 'btn-primary'" ng-disabled="courseForm.$invalid || sending || saving">
                            <i class="fa fa-@{{ icon }}"></i>
                            @{{ button }}
                        </button>

                        <span class="vdivider bgddd fright"></span>

                        <button class="btn-sm btn-default fright" ng-click="delete(courseForm)" ng-disabled="sending || saving">
                            <i class="fa fa-times-circle"></i>
                            Clear All
                        </button>

                        <!-- /End .form-group -->
                    </div>

                </header>
                <div class="panel-body">

                    <div class="course-panel">

                        <!-- Tabs -->
                        <div class="tab-content">

                            <!-- Form Data -->
                            <div class="form-data" ng-show="!loading">

                                <!-- Segment Store -->
                                <div class="form-group" ng-if="stores" ng-class="{ 'has-error' : courseForm.stores.$invalid && courseForm.stores.$dirty , 'has-success' : !courseForm.stores.$invalid && courseForm.stores.$dirty }">
                                    <label class="hide" for="stores">Stores:</label>
                                    <multiselect
                                            name="stores"
                                            ng-model="course.stores"
                                            ng-value="course.stores"
                                            options="stores.data"
                                            selected-model="course.stores"
                                            extra-settings="stores.settings"
                                            events="stores.events"
                                            translation-texts="{ buttonDefaultText: 'Stores' }"
                                            ng-dropdown-multiselect="stores"
                                    ></multiselect>
                                    <i class="fa fa-lock color888 fleft"></i>
                                    <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!courseForm.stores.$invalid && courseForm.stores.$dirty"></span>
                                </div>

                                <!-- Segment Role -->
                                <div class="form-group" ng-if="roles" ng-class="{ 'has-error' : courseForm.roles.$invalid && courseForm.roles.$dirty , 'has-success' : !courseForm.roles.$invalid && courseForm.roles.$dirty }">
                                    <label class="hide" for="roles">Roles:</label>
                                    <multiselect
                                            name="roles"
                                            ng-model="course.roles"
                                            ng-value="course.roles"
                                            options="roles.data"
                                            selected-model="course.roles"
                                            extra-settings="roles.settings"
                                            events="roles.events"
                                            translation-texts="{ buttonDefaultText: 'Roles' }"
                                            ng-dropdown-multiselect="roles"
                                    ></multiselect>
                                    <i class="fa fa-lock color888 fleft"></i>
                                    <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!courseForm.roles.$invalid && courseForm.roles.$dirty"></span>
                                </div>

                                <!-- Course Name -->
                                <div class="form-group clearfix" ng-class="{ 'has-error' : courseForm.name.$invalid && courseForm.name.$dirty , 'has-success' : !courseForm.name.$invalid && courseForm.name.$dirty }">
                                    <label class="hide" for="name">Name:</label>
                                    <input type="text" id="name" name="name" placeholder="Name"  ng-model="course.name" ng-value="course.name" required is-empty />
                                    <i class="fa fa-male color888 fleft"></i>
                                    <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="courseForm.name.$invalid && courseForm.name.$dirty"></span>
                                    <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!courseForm.name.$invalid && courseForm.name.$dirty"></span>
                                </div>

                                <!-- Course Description -->
                                <div class="form-group clearfix" ng-class="{ 'has-error' : courseForm.description.$invalid && courseForm.description.$dirty , 'has-success' : !courseForm.description.$invalid && courseForm.description.$dirty }">
                                    <label class="hide" for="description">Description:</label>
                                    <input type="text" id="description" name="description" placeholder="Description"  ng-model="course.description" required is-empty />
                                    <i class="fa fa-paragraph color888 fleft"></i>
                                    <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="courseForm.description.$invalid && courseForm.description.$dirty"></span>
                                    <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!courseForm.description.$invalid && courseForm.description.$dirty"></span>
                                </div>

                                <!-- Course Passing Score -->
                                <div class="form-group clearfix" ng-class="{ 'has-error' : courseForm.passing_score.$invalid && courseForm.passing_score.$dirty , 'has-success' : !courseForm.passing_score.$invalid && courseForm.passing_score.$dirty }">
                                    <label class="hide" for="passing_score">Passing Score:</label>
                                    <input type="text" id="passing_score" name="passing_score" placeholder="Passing Score"  ng-model="course.passing_score" step="0.01" percentage required />
                                    <i class="fa fa-percent color888 fleft"></i>
                                    <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="courseForm.passing_score.$invalid && courseForm.passing_score.$dirty"></span>
                                    <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!courseForm.passing_score.$invalid && courseForm.passing_score.$dirty"></span>
                                </div>

                                <!-- Course Pages -->
                                <table class="table table-bordered table-striped list">
                                    <thead>
                                    <tr>
                                        <th colspan="4" valign="middle">
                                            <i class="fa fa-file-text"></i>
                                            Pages
                                            <button type="button" ng-click="course.pages.push({subject:'',article:''});submit( courseForm, true );" class="btn-sm btn-primary fright">
                                                <i class="fa fa-plus-circle"></i>
                                                Add Page
                                            </button>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="(page_index, page) in course.pages">
                                        <td>

                                            <!-- Page -->

                                            <!-- File Drop Zone -->
                                            <p>Attach a Files</p>
                                            <div class="form-group">
                                                <div
                                                        ngf-drop="upload($files , page_index)"
                                                        ngf-select="upload($files , page_index)"
                                                        ngf-drag-over-class="'dragover'"
                                                        ngf-keep="'distinct'"
                                                        ng-model="files[page_index]"
                                                        ng-init="uploaderInit(page_index)"
                                                        class="drop-area text-center"
                                                        ngf-pattern="'image/*,video/*'"
                                                        ngf-accept="'image/*,video/*'"
                                                        ngf-multiple="false">

                                                    <i class="fa fa-paperclip color888"></i>
                                                    Drop Attachment / Click Here

                                                </div>
                                            </div>

                                            <!-- Page File List -->
                                            <ul class="files clearfix">
                                                <li class="fleft clearfix" ng-if="files[page_index].length > 0">

                                                    <progress max="100" value="@{{ files[page_index][files[page_index].length-1].progress }}" ng-class="files[page_index][files[page_index].length-1].status ? files[page_index][files[page_index].length-1].status : null"></progress>

                                                    <div class="clearfix">
                                                        <small class="fleft font2">
                                                            <input type="text" name="name" ng-model="files[page_index][files[page_index].length-1].custom_name" value="@{{ files[page_index][files[page_index].length-1].name }}" class="fleft" ng-init="files[page_index][files[page_index].length-1].custom_name = files[page_index][files[page_index].length-1].name" />
                                                        </small>
                                                        <i class="fa fa-times fright" ng-click="remove( files[page_index][files[page_index].length-1] , files[page_index].length-1 , page_index)"></i>
                                                    </div>

                                                </li>
                                            </ul>

                                            <p>Create your News Article</p>

                                            <!-- Page Subject -->
                                            <div class="form-group clearfix" ng-class="{ 'has-error' : courseForm[ 'page-'+page.id+'-subject' ].$invalid && courseForm[ 'page-'+page.id+'-subject' ].$dirty , 'has-success' : !courseForm[ 'page-'+page.id+'-subject' ].$invalid && courseForm[ 'page-'+page.id+'-subject' ].$dirty }">
                                                <label class="hide" for="page-@{{ page.id }}-subject">Subject:</label>
                                                <input type="text" id="page-@{{ page.id }}-subject" name="page-@{{ page.id }}-subject" placeholder="Subject"  ng-model="page.subject" required is-empty />
                                                <i class="fa fa-male color888 fleft"></i>
                                                <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="courseForm[ 'page-'+page.id+'-subject' ].$invalid && courseForm[ 'page-'+page.id+'-subject' ].$dirty"></span>
                                                <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!courseForm[ 'page-'+page.id+'-subject' ].$invalid && courseForm[ 'page-'+page.id+'-subject' ].$dirty"></span>
                                            </div>
                                            
                                            <!-- Page Article -->
                                            <div class="form-group clearfix" ng-class="{ 'has-error' : courseForm[ 'page-'+page.id+'-article' ].$invalid && courseForm[ 'page-'+page.id+'-article' ].$dirty , 'has-success' : !courseForm[ 'page-'+page.id+'-article' ].$invalid && courseForm[ 'page-'+page.id+'-article' ].$dirty }">
                                                <label class="hide" for="page-@{{ page.id }}-article">Article:</label>
                                                <text-angular id="page-@{{ page.id }}-article" name="page-@{{ page.id }}-article" placeholder="Article"  ng-model="page.article" required is-empty></text-angular>
                                                <i class="fa fa-comments color888 fleft"></i>
                                                <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="courseForm[ 'page-'+page.id+'-article' ].$invalid && courseForm[ 'page-'+page.id+'-article' ].$dirty"></span>
                                                <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!courseForm[ 'page-'+page.id+'-article' ].$invalid && courseForm[ 'page-'+page.id+'-article' ].$dirty"></span>
                                            </div>

                                            <!-- end Page -->

                                        </td>
                                        <td width="50" align="center">
                                            <!-- Delete Page -->
                                            <button type="button" ng-click="course.pages[page_index].deleted = 1; submit( courseForm, true );" class="btn-sm btn-danger font2" >Delete</button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <!-- Course Test Page -->
                                <table class="table table-bordered table-striped list">
                                    <thead>
                                    <tr>
                                        <th colspan="4" valign="middle">
                                            <i class="fa fa-file-text"></i>
                                            Exams
                                            <button type="button" ng-click="course.tests.push({answers:{}}); submit( courseForm, true );" class="btn-sm btn-primary fright">
                                                <i class="fa fa-plus-circle"></i>
                                                Add Question
                                            </button>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="(test_index, test) in course.tests">
                                        <td>

                                            <!-- Test -->
                                            
                                            <!-- Question -->
                                            <div class="form-group clearfix" ng-class="{ 'has-error' : courseForm[ 'test-'+test.id+'-question' ].$invalid && courseForm[ 'test-'+test.id+'-question' ].$dirty , 'has-success' : !courseForm[ 'test-'+test.id+'-question' ].$invalid && courseForm[ 'test-'+test.id+'-question' ].$dirty }">
                                                <label class="hide" for="test-@{{ test.id }}-question">Question:</label>
                                                <textarea type="text" id="test-@{{ test.id }}-question" name="test-@{{ test.id }}-question" placeholder="Question" ng-model="test.question" required /></textarea>
                                                <i class="fa fa-paragraph color888 fleft"></i>
                                                <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="courseForm[ 'test-'+test.id+'-question' ].$invalid && courseForm[ 'test-'+test.id+'-question' ].$dirty"></span>
                                                <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!courseForm[ 'test-'+test.id+'-question' ].$invalid && courseForm[ 'test-'+test.id+'-question' ].$dirty"></span>
                                            </div>
                                            <!-- Time -->
                                            <div class="form-group clearfix" ng-class="{ 'has-error' : courseForm[ 'test-'+test.id+'-time' ].$invalid && courseForm[ 'test-'+test.id+'-time' ].$dirty , 'has-success' : !courseForm[ 'test-'+test.id+'-time' ].$invalid && courseForm[ 'test-'+test.id+'-time' ].$dirty }">
                                                <label class="hide" for="test-@{{ test.id }}-time">Time:</label>
                                                <!---->
                                                <input type="text" id="test-@{{ test.id }}-time" name="test-@{{ test.id }}-time" placeholder="Time" model-view-value="true" ng-model="test.time" ui-mask="99:99:99" ui-mask-placeholder="__:__:__" time time-min="'00:00:59'" required />
                                                <i class="fa fa-clock-o color888 fleft"></i>
                                                <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="courseForm[ 'test-'+test.id+'-time' ].$invalid && courseForm[ 'test-'+test.id+'-time' ].$dirty"></span>
                                                <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!courseForm[ 'test-'+test.id+'-time' ].$invalid && courseForm[ 'test-'+test.id+'-time' ].$dirty"></span>
                                            </div>

                                            <!-- Test Page Answer -->
                                            <table class="table table-bordered table-striped list">
                                                <thead>
                                                <tr>
                                                    <th colspan="4" valign="middle">
                                                        <i class="fa fa-font"></i>
                                                        Answers
                                                        <button type="button" ng-click="test.answers.push({}); submit( courseForm, true );" class="btn-sm btn-primary fright">
                                                            <i class="fa fa-plus-circle"></i>
                                                            Add Answer
                                                        </button>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="(answer_index, answer) in test.answers">
                                                    <td>

                                                        <!-- Answer -->

                                                        <!-- Text -->
                                                        <div class="form-group clearfix" ng-class="{ 'has-error' : courseForm[ 'test-'+test.id+'-answer-'+answer.id+'-text' ].$invalid && courseForm[ 'test-'+test.id+'-answer-'+answer.id+'-text' ].$dirty , 'has-success' : !courseForm[ 'test-'+test.id+'-answer-'+answer.id+'-text' ].$invalid && courseForm[ 'test-'+test.id+'-answer-'+answer.id+'-text' ].$dirty }">
                                                            <label class="hide" for="test-@{{ test.id }}-answer-@{{ answer.id }}-text">Answer:</label>
                                                            <textarea type="text" id="test-@{{ test.id }}-answer-@{{ answer.id }}-text" name="test-@{{ test.id }}-answer-@{{ answer.id }}-text" placeholder="Answer"  ng-model="answer.text" ng-change="courseForm.error[ 'test-'+test.id+'-answer' ] = false;" required is-empty /></textarea>
                                                            <i class="fa fa-paragraph color888 fleft"></i>
                                                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="courseForm[ 'test-'+test.id+'-answer-'+answer.id+'-text' ].$invalid && courseForm[ 'test-'+test.id+'-answer-'+answer.id+'-text' ].$dirty"></span>
                                                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!courseForm[ 'test-'+test.id+'-answer-'+answer.id+'-text' ].$invalid && courseForm[ 'test-'+test.id+'-answer-'+answer.id+'-text' ].$dirty"></span>
                                                        </div>

                                                        <!-- end Answer -->
                                                    </td>
                                                    <td width="120" align="center actions">
                                                        <!-- Delete Question -->
                                                        <button type="button" ng-click="submit( courseForm, true );"  ng-model="test.answer_id" uib-btn-radio="answer.id" class="btn-sm font2" ng-class="{ 'btn-default' : test.answer_id !== answer.id, 'btn-primary' : test.answer_id == answer.id}" ng-disabled="!answer.text">Correct</button>
                                                        <!-- Delete Question -->
                                                        <button type="button" ng-click="course.tests[test_index].answers[answer_index].deleted = 1; submit( courseForm, true );" class="btn-sm btn-danger font2">Delete</button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                            <!-- end Test -->
                                        </td>
                                        <td width="50" align="center">
                                            <!-- Delete Test -->
                                            <button type="button" ng-click="course.tests[test_index].deleted = 1; submit( courseForm, true );" class="btn-sm btn-danger font2">Delete</button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <!-- /End .tab-pane -->
                            </div>

                            <!-- /End .form-data -->
                        </div>

                        <!-- Show Loading -->
                        <div ng-show="loading" class="text-center font2 color000">
                            <i class="fa fa-circle-o-notch fa-spin color000"></i>
                            <span>Loading</span>
                        </div>

                        <!-- /End .logout-panel -->
                    </div>

                    <!-- /End .panel-body -->
                </div>

            </main>

        </form>

