
        <!-- components.course-page -->
        <div>


            <!-- components.course-page success -->
            <div class="bs-callout bs-callout-info" ng-show="success.length">
                <h4 class="font2">Success!</h4>
                <p>@{{ success }}</p>
            </div>


            <!-- components.course-page error -->
            <div class="bs-callout bs-callout-danger" ng-show="hasError( courseForm ) || errors.length">
                <h4 class="font2">Form Errors</h4>
                <div>
                    <ul>r

                        <!-- General Errors -->
                        <li ng-repeat="error in errors">
                            @{{ error }}
                        </li>

                        <!-- Customers Error -->
                        <li ng-show="courseForm.error[ 'passing_score' ] = courseForm.passing_score.$invalid && courseForm.passing_score.$dirty" ng-messages="courseForm.passing_score.$error">
                            <span ng-message="required">Customers is required</span>
                        </li>

                        <!-- Name Error -->
                        <li ng-show="courseForm.error[ 'name' ] = courseForm.name.$invalid && courseForm.name.$dirty" ng-messages="courseForm.name.$error">
                            <span ng-message="required">Name is required</span>
                        </li>

                        <!-- Description Error -->
                        <li ng-show="courseForm.error[ 'description' ] = courseForm.description.$invalid && courseForm.description.$dirty" ng-messages="courseForm.description.$error">
                            <span ng-message="required">Description is required</span>
                        </li>

                    </ul>
                </div>
            </div>
            
            <!-- components.course-page -->
            <main id="course-page" class="border panel panel-default">
                <header class="font2 panel-heading">

                </header>
                <div class="panel-body">

                    <div class="course-panel">

                        <div>
                            <h1>
                                @{{ course.name }}
                                <span class="fright" ng-class="page.timer <= 5000 ? 'colorred' : ''" ng-show="page.timer >= 0">@{{ page.timer | date:'HH:mm:ss':'+0000' }}</span>
                                <span class="fright" ng-show="!page.timer && page.type == 'take'" ng-if="course.pages && course.pages.length > 0">
                                    <a class="btn-sm bgbbb colorfff hovercolorfff font2" ng-disabled="page.tab == 0" ng-click="getPage(page.tab - 1)">Prev Page</a>
                                    <a class="btn-sm btn-primary colorfff hovercolorfff font2"
                                       ng-click="getPage(page.tab + 1)"
                                       ng-if="page.tab !== course.pages.length-1"
                                    >Next Page</a>
                                    <a class="btn-sm btn-primary colorfff hovercolorfff font2"
                                       ng-click="getExam()"
                                       ng-if="page.tab == course.pages.length-1 && course.tests && course.tests.length > 0"
                                    >Take Exam</a>
                                </span>
                            </h1>

                            <!-- Breadcrumbs -->
                            <ol class="breadcrumb" ng-hide="loading" ng-cloak>
                                <li ng-repeat="(tab_index, tab) in course.pages">
                                    <a ng-click="getPage(tab_index)" ng-disabled="page.tests.length > 0">@{{ tab.subject }}</a>
                                </li>
                                <li ng-if="course.tests && course.tests.length > 0">
                                    <a ng-click="getExam()">Exam</a>
                                </li>
                            </ol>

                        </div>
                        <hr />
                        <!-- Tabs -->
                        <div class="tab-content">

                            <!-- Take Tab -->
                            <div class="tab-pane take" ng-class="page.tab == tab_index ? 'active' : ''" ng-if="page.tab == tab_index && page.type == 'take'" ng-repeat="(tab_index, tab) in course.pages">

                                <a class="media" ng-click="view(tab.attachment)">
                                    <img ng-src="@{{ tab.attachment.thumbnail }}" />
                                    <span ng-if="tab.attachment.mime.type == 'video'" class="play">
                                        <i class="fa fa-play-circle color000"></i>
                                    </span>
                                </a>
                                <ng-bind-html ng-bind-html="tab.article"></ng-bind-html>
                                <!-- /End .tab-pane -->

                            </div>

                            <!-- Take Tab -->
                            <div class="tab-pane take active" ng-class="page.tab == tab_index ? 'active' : ''" ng-if="(!course.pages || course.pages.length == 0) && page.type == 'take'">

                                <div class="text-center" ng-show="hasPermission('course.edit')">You haven't added pages to this course! <a ng-click="open('/course/edit/'+course.id)">Do it</a> if wont continue.</div>
                                <div class="text-center" ng-show="!hasPermission('course.edit')">Any pages didn't added to this course!</div>
                                <!-- /End .tab-pane -->
                            </div>

                            <!-- Exam Tab -->
                            <div class="tab-pane exam active" ng-if="page.tab == test_index && page.type == 'exam'" ng-repeat="(test_index, test) in page.tests">

                                <h1>@{{ test.question }}</h1>
                                <hr />
                                <div class="grid" id="masonry">
                                    <div class="grid-item" ng-repeat="(answer_index, answ) in test.answers">
                                        <label>
                                            <input type="radio" ng-model="answer.answer_id" ng-value="answ.id" ng-disabled="sending"/>
                                            @{{ answ.text }}
                                        </label>
                                    </div>
                                </div>
                                <hr />
                                <!-- Submit Button -->
                                <a class="btn-sm btn-primary fright" ng-click="sendAnswer()" ng-disabled="sending || !answer.answer_id">
                                    Send Answer
                                </a>

                                <!-- /End .tab-pane -->
                            </div>

                            <!-- Result Tab -->
                            <div class="tab-pane result active" ng-if="page.type == 'result'">

                                <h1 class="text-center" ng-class="page.result.type">@{{ page.result.type | uppercase }}: @{{ page.result.correct }}/@{{ page.result.questions }}</h1>
                                <p class="text-center">
                                    <a class="btn-sm btn-primary colorfff hovercolorfff font2" ng-click="open( 'course' )">View Courses</a>
                                </p>

                                <!-- /End .tab-pane -->
                            </div>

                            <!-- /End .form-data -->
                        </div>

                        <!-- Show Loading -->
                        <div ng-show="loading" class="text-center font2 color000">
                            <i class="fa fa-circle-o-notch fa-spin color000"></i>
                            <span>Loading</span>
                        </div>

                        <!-- /End .logout-panel -->
                    </div>

                    <!-- /End .panel-body -->
                </div>

            </main>

        </div>