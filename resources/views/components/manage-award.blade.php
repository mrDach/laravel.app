
<!-- components.manage-award -->
<form name="awardForm" method="post" ng-submit="submit( awardForm )" novalidate>


    <!-- components.manage-award success -->
    <div class="bs-callout bs-callout-info" ng-show="success.length && awardForm.$pristine">
        <h4 class="font2">Success!</h4>
        <p>@{{ success }}</p>
    </div>


    <!-- components.manage-award error -->
    <div class="bs-callout bs-callout-danger" ng-show="hasError( awardForm ) || errors.length">
        <h4 class="font2">Form Errors</h4>
        <div>
            <ul>

                <!-- General Errors -->
                <li ng-repeat="error in errors">
                    @{{ error }}
                </li>

                <!-- Start Date Error -->
                <li ng-show="awardForm.error[ 'start' ] = awardForm.start.$invalid && awardForm.start.$dirty" ng-messages="awardForm.start.$error">
                    <span ng-message="required">Start Date is required</span>
                    <span ng-message="date-max">Start Time cannot start after the End Time</span>
                    <span ng-message="date-invalid">Start Time Requires a Valid Date</span>
                </li>

                <!-- End Date Error -->
                <li ng-show="awardForm.error[ 'end' ] = awardForm.end.$invalid && awardForm.end.$dirty" ng-messages="awardForm.end.$error">
                    <span ng-message="required">End Date is required</span>
                    <span ng-message="date-min">End Time cannot start before the Start Time</span>
                    <span ng-message="date-invalid">End Time Requires a Valid Date</span>
                </li>

                <!-- Customers Error -->
                <li ng-show="awardForm.error[ 'customers' ] = awardForm.customers.$invalid && awardForm.customers.$dirty" ng-messages="awardForm.customers.$error">
                    <span ng-message="required">Customers is required</span>
                </li>

                <!-- Name Error -->
                <li ng-show="awardForm.error[ 'name' ] = awardForm.name.$invalid && awardForm.name.$dirty" ng-messages="awardForm.name.$error">
                    <span ng-message="required">Name is required</span>
                </li>

                <!-- Description Error -->
                <li ng-show="awardForm.error[ 'description' ] = awardForm.description.$invalid && awardForm.description.$dirty" ng-messages="awardForm.description.$error">
                    <span ng-message="required">Description is required</span>
                </li>

            </ul>
        </div>
    </div>


    <!-- components.manage-award -->
    <main id="award-management" class="border panel panel-default">
        <header class="font2 panel-heading">

            <i class="fa fa-@{{ icon }}"></i>
            @{{ title }}

            <div class="form-group fright">

                <!-- Submitting Wait Notice -->
                <small class="colorinfo text-right fleft submitnotice" ng-show="sending">
                    <i class="fa fa-circle-o-notch fa-spin colorinfo"></i>
                    @{{ running }} award - Please wait.
                </small>

                <!-- Saving Wait Notice -->
                <small class="colorinfo text-right fleft submitnotice" ng-show="saving">
                    <i class="fa fa-circle-o-notch fa-spin colorinfo"></i>
                    Saving - Please wait.
                </small>

                <!-- Form Error Notices -->
                <small class="colorred text-right fleft submitnotice" ng-show="hasError( awardForm )">
                    <span ng-show="hasError( awardForm )">You must fix any form errors before proceeding * See Errors at Top *</span>
                </small>

                <!-- Submit Button -->
                <button type="submit" name="submit" class="btn-sm fright" ng-class="hasError( awardForm ) || errors.length ? 'btn-danger' : 'btn-primary'" ng-disabled="awardForm.$invalid || sending || saving">
                    <i class="fa fa-@{{ icon }}"></i>
                    @{{ button }}
                </button>

                <!-- /End .form-group -->
            </div>

        </header>
        <div class="panel-body">

            <div class="award-panel">

                <!-- Tabs -->
                <div class="tab-content">

                    <!-- Form Data -->
                    <div class="form-data" ng-show="!loading">

                        <!-- Segment Affiliate -->
                        <div class="form-group clearfix">
                            <label class="hide" for="location">Segment Affiliate(s):</label>
                            <div class="select">
                                <select type="text" id="location" name="location" ng-model="award.location" ng-change="loadLocations( awardForm )">
                                    <option value="">Segment Affiliate(s)</option>
                                    <option value="both">Both</option>
                                    <option value="branch">Branch</option>
                                    <option value="department">Department</option>
                                </select>
                            </div>
                            <i class="fa fa-building color888 fleft"></i>
                        </div>

                        <!-- Segment Branch -->
                        <div class="form-group" ng-if="(award.location==='both' || award.location==='branch') && branches" ng-class="{ 'has-error' : awardForm.branches.$invalid && awardForm.branches.$dirty , 'has-success' : !awardForm.branches.$invalid && awardForm.branches.$dirty }">
                            <label class="hide" for="branches">Branches:</label>
                            <multiselect
                                    name="branches"
                                    ng-model="award.branches"
                                    ng-value="award.branches"
                                    options="branches.data"
                                    selected-model="award.branches"
                                    extra-settings="branches.settings"
                                    events="branches.events"
                                    translation-texts="{ buttonDefaultText: 'Branches' }"
                                    ng-dropdown-multiselect="branches"
                            ></multiselect>
                            <i class="fa fa-lock color888 fleft"></i>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!awardForm.branches.$invalid && awardForm.branches.$dirty"></span>
                        </div>

                        <!-- Segment Department -->
                        <div class="form-group" ng-if="(award.location==='both' || award.location==='department') && departments" ng-class="{ 'has-error' : awardForm.departments.$invalid && awardForm.departments.$dirty , 'has-success' : !awardForm.departments.$invalid && awardForm.departments.$dirty }">
                            <label class="hide" for="departments">Departments:</label>
                            <multiselect
                                    name="departments"
                                    ng-model="award.departments"
                                    ng-value="award.departments"
                                    options="departments.data"
                                    selected-model="award.departments"
                                    extra-settings="departments.settings"
                                    events="departments.events"
                                    translation-texts="{ buttonDefaultText: 'Departments' }"
                                    ng-dropdown-multiselect="departments"
                            ></multiselect>
                            <i class="fa fa-lock color888 fleft"></i>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!awardForm.departments.$invalid && awardForm.departments.$dirty"></span>
                        </div>

                        <!-- Start Date -->
                        <div class="form-group clearfix" uib-dropdown uib-dropdown-toggle is-open="award.calendars[0].open" ng-class="{ 'has-error' : awardForm.start.$invalid && awardForm.start.$dirty , 'has-success' : !awardForm.start.$invalid && awardForm.start.$dirty }">
                            <label class="hide" for="start">Start Date:</label>
                            <input type="datetime" id="start" name="start" ng-model="award.start" placeholder="Start Date" date-compare date-max="award.end" autocomplete="off" ng-change="dateSelect( awardForm , 'start' , 1 )" required />
                            <i class="fa fa-clock-o color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="awardForm.start.$invalid && awardForm.start.$dirty"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!awardForm.start.$invalid && awardForm.start.$dirty"></span>
                            <span class="input-group-btn">
                                <button type="button" class="btn" ng-class="( award.calendars[0].open ? 'btn-primary' : 'btn-default' )"><i class="fa fa-calendar"></i></button>
                            </span>
                            <div uib-dropdown-menu>
                                <datetimepicker data-ng-model="award.start" data-datetimepicker-config="{ modelType:'YYYY-MM-DD', minView:'day' }" data-on-set-time="dateSelect( awardForm , 'start' , 1 )"></datetimepicker>
                            </div>
                        </div>

                        <!-- End Date -->
                        <div class="form-group clearfix" uib-dropdown uib-dropdown-toggle is-open="award.calendars[1].open" ng-class="{ 'has-error' : awardForm.end.$invalid && awardForm.end.$dirty , 'has-success' : !awardForm.end.$invalid && awardForm.end.$dirty }">
                            <label class="hide" for="end">End Date:</label>
                            <input type="datetime" id="end" name="end" ng-model="award.end" placeholder="End Date" date-compare date-min="award.start" autocomplete="off" ng-change="dateSelect( awardForm , 'end' , 2 )" required />
                            <i class="fa fa-clock-o color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="awardForm.end.$invalid && awardForm.end.$dirty"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!awardForm.end.$invalid && awardForm.end.$dirty"></span>
                            <span class="input-group-btn">
                                <button type="button" class="btn" ng-class="( award.calendars[1].open ? 'btn-primary' : 'btn-default' )"><i class="fa fa-calendar"></i></button>
                            </span>
                            <div uib-dropdown-menu>
                                <datetimepicker data-ng-model="award.end" data-datetimepicker-config="{ modelType:'YYYY-MM-DD', minView:'day' }" data-on-set-time="dateSelect( awardForm , 'end' , 2 )"></datetimepicker>
                            </div>
                        </div>

                        <!-- Customers Required -->
                        <div class="form-group clearfix" ng-class="{ 'has-error' : awardForm.customers.$invalid && awardForm.customers.$dirty , 'has-success' : !awardForm.customers.$invalid && awardForm.customers.$dirty }">
                            <label class="hide" for="customers">Customers Required</label>
                            <input type="number" id="customers" name="customers" placeholder="Customers Required" ng-model="award.customers" min="0" step="1" number required is-empty />
                            <i class="fa fa-male color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="awardForm.customers.$invalid && awardForm.customers.$dirty"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!awardForm.customers.$invalid && awardForm.customers.$dirty"></span>
                        </div>

                        <!-- Award Name -->
                        <div class="form-group clearfix" ng-class="{ 'has-error' : awardForm.name.$invalid && awardForm.name.$dirty , 'has-success' : !awardForm.name.$invalid && awardForm.name.$dirty }">
                            <label class="hide" for="name">Name:</label>
                            <input type="text" id="name" name="name" placeholder="Name" ng-model="award.name" ng-value="award.name" required is-empty />
                            <i class="fa fa-male color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="awardForm.name.$invalid && awardForm.name.$dirty"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!awardForm.name.$invalid && awardForm.name.$dirty"></span>
                        </div>

                        <!-- Description -->
                        <div class="form-group clearfix" ng-class="{ 'has-error' : awardForm.description.$invalid && awardForm.description.$dirty , 'has-success' : !awardForm.description.$invalid && awardForm.description.$dirty }">
                            <label class="hide" for="description">Description</label>
                            <text-angular id="description" name="description" placeholder="Description" ng-model="award.description" required is-empty></text-angular>
                            <i class="fa fa-comments color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="awardForm.description.$invalid && awardForm.description.$dirty"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!awardForm.description.$invalid && awardForm.description.$dirty"></span>
                        </div>

                        <!-- /End .tab-pane -->
                    </div>

                    <!-- /End .form-data -->
                </div>

                <!-- Show Loading -->
                <div ng-show="loading" class="text-center font2 color000">
                    <i class="fa fa-circle-o-notch fa-spin color000"></i>
                    <span>Loading</span>
                </div>

                <!-- /End .logout-panel -->
            </div>

            <!-- /End .panel-body -->
        </div>

    </main>

</form>

