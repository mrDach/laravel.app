
        <!-- components.manage-group form -->
        <form name="groupForm" method="post" ng-submit="submit( groupForm )" novalidate>

            <!-- components.manage-group success -->
            <div class="bs-callout bs-callout-info" ng-show="success.length && groupForm.$pristine">
                <h4 class="font2">Success!</h4>
                <p>@{{ success }}</p>
            </div>

            <!-- components.manage-group error -->
            <div class="bs-callout bs-callout-danger" ng-show="hasError( groupForm ) || errors.length">
                <h4 class="font2">Form Errors</h4>
                <div>
                    <ul>

                        <!-- General Errors -->
                        <li ng-repeat="error in errors">
                            @{{ error }}
                        </li>

                        <!-- Group Name Error -->
                        <li ng-show="groupForm.error.name = ( groupForm.name.$dirty && groupForm.name.$invalid )" ng-messages="groupForm.name.$error">
                            <span ng-message="required">Name is required</span>
                            <span ng-message="uniqueRole">The Group Name already Exists</span>
                        </li>

                        <!-- Group Description Error -->
                        <li ng-show="groupForm.error.description = ( groupForm.description.$dirty && groupForm.description.$invalid )" ng-messages="groupForm.description.$error">
                            <span ng-message="required">Description is required</span>
                        </li>

                    </ul>
                </div>
            </div>

            <!-- components.manage-group success -->
            <main id="group-management" class="border panel panel-default">
                <header class="font2 panel-heading">

                    <i class="fa fa-@{{ icon }}"></i>
                    @{{ title }}

                </header>
                <div class="panel-body">

                    <!-- Group Name -->
                    <div class="form-group clearfix" ng-class="{ 'has-error' : groupForm.name.$invalid && groupForm.name.$dirty , 'has-success' : !groupForm.name.$invalid && groupForm.name.$dirty }">
                        <label class="hide" for="name">Name:</label>
                        <input type="text" id="name" name="name" placeholder="Name" ng-model="group.name" ng-value="group.name" required is-empty />
                        <i class="fa fa-male color888 fleft"></i>
                        <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="groupForm.name.$invalid && groupForm.name.$dirty"></span>
                        <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!groupForm.name.$invalid && groupForm.name.$dirty"></span>
                    </div>

                    <!-- Group Description -->
                    <div class="form-group clearfix" ng-class="{ 'has-error' : groupForm.description.$invalid && groupForm.description.$dirty , 'has-success' : !groupForm.description.$invalid && groupForm.description.$dirty }">
                        <label class="hide" for="description">Description:</label>
                        <textarea type="text" id="description" name="description" placeholder="Description" ng-model="group.description" ng-value="group.description" required is-empty /></textarea>
                        <i class="fa fa-paragraph color888 fleft"></i>
                        <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="groupForm.description.$invalid && groupForm.description.$dirty"></span>
                        <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!groupForm.description.$invalid && groupForm.description.$dirty"></span>
                    </div>

                    <div class="form-group">

                        <!-- Back Button -->
                        <div class="fleft">
                            <a ng-click="back('/affiliate/group(/[0-9]+)?' , '/affiliate/group')" class="btn bgeee coloraaa hoverbg555 hovercolorfff">
                                <i class="fa fa-arrow-circle-o-left"></i>
                                Back to List
                            </a>
                        </div>


                        <div class="fright">

                            <!-- Form Error Notice -->
                            <small class="colorred submitnotice" ng-show="hasError( groupForm )">
                                You must fix any form errors before proceeding * See Errors at Top *
                            </small>

                            <!-- Submitting Wait Notice -->
                            <small class="colorinfo submitnotice" ng-show="sending">
                                <i class="fa fa-circle-o-notch fa-spin colorinfo"></i>
                                @{{ running }} group - Please wait.
                            </small>

                            <!-- Submit Button -->
                            <button name="submit" class="btn fright" ng-class="hasError( groupForm ) || errors.length ? 'btn-danger' : 'btn-primary'" ng-disabled="groupForm.$invalid || sending">
                                <i class="fa fa-@{{ icon }}"></i>
                                @{{ button }}
                            </button>

                            <!-- /End .fright -->
                        </div>

                        <!-- /End .form-group -->
                    </div>

                    <!-- /End .panel-body -->
                </div>

            </main>

        </form>

