        
        <!-- components.manage-affiliate -->
        <form name="affiliateForm" method="post" ng-submit="submit( affiliateForm )" novalidate>

            <!-- components.manage-affiliate success -->
            <div class="bs-callout bs-callout-info" ng-show="success.length && affiliateForm.$pristine">
                <h4 class="font2">Success!</h4>
                <p>@{{ success }}</p>
            </div>
            <!-- components.manage-affiliate error -->
            <div class="bs-callout bs-callout-danger" ng-show="hasError( affiliateForm ) || errors.length">
                <h4 class="font2">Form Errors</h4>
                <div>
                    <ul>

                        <!-- General Errors -->
                        <li ng-repeat="error in errors">
                            @{{ error }}
                        </li>

                        <!-- Group Name Error -->
                        <li ng-show="affiliateForm.error.group = ( affiliateForm.group.$dirty && affiliateForm.group.$invalid )" ng-messages="affiliateForm.group.$error">
                            <span ng-message="required">Group is required</span>
                        </li>

                        <!-- Group Files Error -->
                        <li ng-show="affiliateForm.error.files = ( affiliateForm.files.$dirty && affiliateForm.files.$invalid )" ng-messages="affiliateForm.files.$error">
                            <span ng-message="required">Files is required</span>
                        </li>

                    </ul>
                </div>
            </div>
        
            <!-- components.manage-affiliate -->
            <main id="affiliate-management" class="border panel panel-default">
                <header class="font2 panel-heading">
        
                    <i class="fa fa-@{{ icon }}"></i>
                    @{{ title }}
        
                    <div class="form-group fright">
        
                        <!-- Form Error Notices -->
                        <small class="colorred text-right fleft submitnotice" ng-show="Upload.isUploadInProgress()">
                            <span ng-show="Upload.isUploadInProgress()">You must wait for the uploads before proceeding</span>
                        </small>
        
                        <!-- Submitting Wait Notice -->
                        <small class="colorinfo text-right fleft submitnotice" ng-show="sending">
                            <i class="fa fa-circle-o-notch fa-spin colorinfo"></i>
                            @{{ running }} - Please wait.
                        </small>
        
                        <!-- Submit Button -->
                        <button type="submit" name="submit" class="btn-sm fright btn-primary" ng-disabled="affiliateForm.$invalid || Upload.isUploadInProgress() || sending">
                            <i class="fa fa-@{{ icon }}"></i>
                            @{{ button }}
                        </button>
        
                        <!-- /End .form-group -->
                    </div>
        
                </header>
                <div class="panel-body">
        
                    <div class="affiliate-panel">
                        <div ng-show="affiliate_groups.length !== 0">
                            <div class="form-group clearfix">
                                <label class="hide" for="store_id">Group:</label>
                                <div class="select">
                                    <select type="text" id="group_id" name="group_id" ng-model="affiliate.group" required>
                                        <option value="">Group</option>
                                        <option ng-repeat="item in affiliate_groups" value="@{{ item.id }}">@{{ item.name }}</option>
                                    </select>
                                </div>
                                <i class="fa fa-building color888 fleft"></i>
                            </div>

                            <!-- File Drop Zone -->
                            <div class="form-group">
                                <div ngf-drop="upload($files)" ngf-select="upload($files)" ngf-drag-over-class="'dragover'" ngf-keep="'distinct'" ng-model="affiliate.files" class="drop-area text-center" ngf-multiple="false" required>

                                    <i class="fa fa-paperclip color888"></i>
                                    Drop Attachment / Click Here

                                </div>
                            </div>

                            <!-- File List -->
                            <ul class="files clearfix">
                                <li ng-repeat="( index , file ) in affiliate.files" class="fleft clearfix">

                                    <progress max="100" value="@{{ file.progress }}" ng-class="file.status ? file.status : null"></progress>

                                    <div class="clearfix">
                                        <small class="fleft font2">
                                            <input type="text" name="name" ng-model="file.custom_name" value="@{{ file.name }}" class="fleft" ng-init="file.custom_name = file.name" />
                                        </small>
                                        <i class="fa fa-times fright" ng-click="remove( file , index )"></i>
                                    </div>

                                </li>
                            </ul>

                        </div>
                        <div ng-show="affiliate_groups.length == 0">
                            <p class="text-center" ng-show="hasPermission('affiliate.groups.create')">You haven't added any groups! <a ng-click="open('/affiliate/group/add')">Do it</a> if wont continue.</p>
                            <p class="text-center" ng-show="!hasPermission('affiliate.groups.create')">It is impossible to continue the group not found.</p>
                        </div>
        
                        <!-- /End .document-panel -->
                    </div>
        
                    <!-- /End .panel-body -->
                </div>
        
            </main>
        
        </form>
        
