        
        <!-- components.manage-affiliate-award -->
        <form name="affiliateAwardForm" method="post" ng-submit="submit( affiliateAwardForm )" novalidate>
        
        
            <!-- components.manage-affiliate-award success -->
            <div class="bs-callout bs-callout-info" ng-show="success.length && affiliateAwardForm.$pristine">
                <h4 class="font2">Success!</h4>
                <p>@{{ success }}</p>
            </div>
        
        
            <!-- components.manage-affiliate-award error -->
            <div class="bs-callout bs-callout-danger" ng-show="hasError( affiliateAwardForm ) || errors.length">
                <h4 class="font2">Form Errors</h4>
                <div>
                    <ul>
        
                        <!-- General Errors -->
                        <li ng-repeat="error in errors">
                            @{{ error }}
                        </li>
        
                        <!-- Recap Error -->
                        <li ng-show="affiliateAwardForm.error[ 'recap' ] = affiliateAwardForm.recap.$invalid && affiliateAwardForm.recap.$dirty" ng-messages="affiliateAwardForm.recap.$error">
                            <span ng-message="required">Recap is required</span>
                        </li>
        
                    </ul>
                </div>
            </div>
        
        
            <!-- components.manage-affiliate-award -->
            <main id="affiliate-management" class="border panel panel-default">
                <header class="font2 panel-heading">
        
                    <i class="fa fa-@{{ icon }}"></i>
                    @{{ title }}
        
                    <div class="form-group fright">
        
                        <!-- Submitting Wait Notice -->
                        <small class="colorinfo text-right fleft submitnotice" ng-show="sending">
                            <i class="fa fa-circle-o-notch fa-spin colorinfo"></i>
                            @{{ running }} affiliate - Please wait.
                        </small>
        
                        <!-- Saving Wait Notice -->
                        <small class="colorinfo text-right fleft submitnotice" ng-show="saving">
                            <i class="fa fa-circle-o-notch fa-spin colorinfo"></i>
                            Saving - Please wait.
                        </small>
        
                        <!-- Form Error Notices -->
                        <small class="colorred text-right fleft submitnotice" ng-show="hasError( affiliateAwardForm )">
                            <span ng-show="hasError( affiliateAwardForm )">You must fix any form errors before proceeding * See Errors at Top *</span>
                        </small>

                        <!-- /End .form-group -->
                    </div>
        
                </header>
                <div class="panel-body">
        
                    <div class="affiliate-panel">
        
                        <!-- Tabs -->
                        <div class="tab-content">
        
                            <!-- Form Data -->
                            <div class="form-data" ng-show="!loading">

                                <!-- Affiliate Name -->
                                <div class="form-group clearfix" ng-class="{ 'has-error' : affiliateAwardForm.name.$invalid && affiliateAwardForm.name.$dirty , 'has-success' : !affiliateAwardForm.name.$invalid && affiliateAwardForm.name.$dirty }">
                                    <label class="hide" for="name">Name:</label>
                                    <input type="text" id="name" name="name" placeholder="Name" ng-value="affiliate.firstname + ' ' + affiliate.lastname" disabled/>
                                    <i class="fa fa-male color888 fleft"></i>
                                    <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="affiliateAwardForm.name.$invalid && affiliateAwardForm.name.$dirty"></span>
                                    <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!affiliateAwardForm.name.$invalid && affiliateAwardForm.name.$dirty"></span>
                                </div>

                                <!-- Awards -->
                                <table class="table table-bordered table-striped list">
                                    <thead>
                                    <tr>
                                        <th colspan="4" valign="middle">
                                            <i class="fa fa-users"></i>
                                            Awards
                                            <button type="button" ng-click="affiliate.awards.push({})" class="btn-sm btn-primary fright">
                                                <i class="fa fa-plus-circle"></i>
                                                Add Awards
                                            </button>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="(index, data) in affiliate.awards">
                                        <td>
        
                                            <!-- Awards -->
                                            <div class="form-group clearfix" ng-class="{ 'has-error' : affiliateAwardForm.award[ index ].id.$invalid && affiliateAwardForm.award[ index ].id.$dirty , 'has-success' : !affiliateAwardForm.award[ index ].id.$invalid && affiliateAwardForm.award[ index ].id.$dirty }">
                                                <label class="hide" for="award-@{{ index }}-id">Award</label>
                                                <div class="select">
                                                    <select id="award-@{{ index }}-id" name="award[@{{ index }}][id]" ng-model="affiliate.awards[ index ].id" ng-change="save( @{{affiliate.awards[ index ].id || 0}}, affiliate.awards[ index ].id, index, affiliateAwardForm)" ng-options="award.id as award.name for award in awards | options:affiliate.awards:index" required>
                                                        <option value="">Award</option>
                                                    </select>
                                                </div>
                                                <i class="fa fa-users color888 fleft"></i>
                                                <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="affiliateAwardForm.award[ index ].id.$invalid && affiliateAwardForm.award[ index ].id.$dirty"></span>
                                                <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!affiliateAwardForm.award[ index ].id.$invalid && affiliateAwardForm.award[ index ].id.$dirty"></span>
                                            </div>
        
                                        </td>
                                        <td width="50" align="center">
                                            <!-- Delete Product -->
                                            <button type="button" ng-click="save( affiliate.awards[ index ].id || 0, 0, index, affiliateAwardForm);" class="btn-sm btn-danger font2" ng-disabled="!affiliate.awards[ index ].id || sending">Delete</button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
        
                                <!-- /End .tab-pane -->
                            </div>
        
                            <!-- /End .form-data -->
                        </div>
        
                        <!-- Show Loading -->
                        <div ng-show="loading" class="text-center font2 color000">
                            <i class="fa fa-circle-o-notch fa-spin color000"></i>
                            <span>Loading</span>
                        </div>
        
                        <!-- /End .affiliate-panel -->
                    </div>
        
                    <!-- /End .panel-body -->
                </div>
        
            </main>
        
        </form>
        
