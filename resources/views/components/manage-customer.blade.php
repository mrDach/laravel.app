
        <!-- components.manage-customer form -->
        <form name="customerForm" method="post" ng-submit="submit( customerForm )" novalidate>

            <!-- components.manage-customer success -->
            <div class="bs-callout bs-callout-info" ng-show="success.length && customerForm.$pristine">
                <h4 class="font2">Success!</h4>
                <p>@{{ success }}</p>
            </div>

            <!-- components.manage-customer error -->
            <div class="bs-callout bs-callout-danger" ng-show="hasError( customerForm ) || errors.length">
                <h4 class="font2">Form Errors</h4>
                <div>
                    <ul>

                        <!-- General Errors -->
                        <li ng-repeat="error in errors">
                            @{{ error }}
                        </li>

                        <!-- customer Name Error -->
                        <li ng-show="customerForm.error.name = ( customerForm.name.$dirty && customerForm.name.$invalid )" ng-messages="customerForm.name.$error">
                            <span ng-message="required">Name is required</span>
                            <span ng-message="uniqueRole">The customer Name already Exists</span>
                        </li>

                        <!-- customer Description Error -->
                        <li ng-show="customerForm.error.description = ( customerForm.description.$dirty && customerForm.description.$invalid )" ng-messages="customerForm.description.$error">
                            <span ng-message="required">Description is required</span>
                        </li>

                    </ul>
                </div>
            </div>

            <!-- components.manage-customer success -->
            <main id="customer-management" class="border panel panel-default">
                <header class="font2 panel-heading">

                    <i class="fa fa-@{{ icon }}"></i>
                    @{{ title }}

                </header>
                <div class="panel-body">

                    <div class="form-customer" ng-show="affiliate.total > 0">
                        <!-- customer Firstname -->
                        <div class="form-group clearfix" ng-class="{ 'has-error' : customerForm.firstname.$invalid && customerForm.firstname.$dirty , 'has-success' : !customerForm.firstname.$invalid && customerForm.firstname.$dirty }">
                            <label class="hide" for="firstname">Firstname:</label>
                            <input type="text" id="firstname" name="firstname" placeholder="Firstname" ng-model="customer.firstname" ng-value="customer.firstname" required is-empty />
                            <i class="fa fa-male color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="customerForm.firstname.$invalid && customerForm.firstname.$dirty"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!customerForm.firstname.$invalid && customerForm.firstname.$dirty"></span>
                        </div>

                        <!-- customer Lastname -->
                        <div class="form-group clearfix" ng-class="{ 'has-error' : customerForm.lastname.$invalid && customerForm.lastname.$dirty , 'has-success' : !customerForm.lastname.$invalid && customerForm.lastname.$dirty }">
                            <label class="hide" for="lastname">Lastname:</label>
                            <input type="text" id="lastname" name="lastname" placeholder="Lastname" ng-model="customer.lastname" ng-value="customer.lastname" required is-empty />
                            <i class="fa fa-male color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="customerForm.lastname.$invalid && customerForm.lastname.$dirty"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!customerForm.lastname.$invalid && customerForm.lastname.$dirty"></span>
                        </div>

                        <!-- customer Affiliate -->
                        <div class="form-group clearfix" ng-class="{ 'has-error' : customerForm.affiliate.$invalid && customerForm.affiliate.$dirty && !customer.affiliate.id && noResults, 'has-success' : !customerForm.affiliate.$invalid && customerForm.affiliate.$dirty && customer.affiliate.id && !noResults && !loadingAffiliate}">
                            <label class="hide" for="affiliate">Affiliate:</label>
                            <input type="text" id="affiliate" name="affiliate" placeholder="Affiliate" ng-model="customer.affiliate" uib-typeahead="affiliate as affiliate.firstname+' '+affiliate.lastname for affiliate in getAffiliate($viewValue)" typeahead-loading="loadingAffiliate" typeahead-no-results="noResults" required is-empty affiliate autocomplete="off"/>
                            <i class="fa fa-male color888 fleft"></i>
                            <div class="loading-results" ng-show="loadingAffiliate">
                                <i class="glyphicon glyphicon-refresh"></i> Loading Results
                            </div>
                            <div class="no-results" ng-show="noResults">
                                <i class="glyphicon glyphicon-remove"></i> No Results Found
                            </div>
                            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback"></span>
                        </div>

                        <!-- customer Date -->
                        <div class="form-group clearfix" uib-dropdown uib-dropdown-toggle ng-class="{ 'has-error' : customerForm.date.$invalid && customerForm.date.$dirty , 'has-success' : !customerForm.date.$invalid && customerForm.date.$dirty }">
                            <label class="hide" for="start">Date:</label>
                            <input type="datetime" id="date" name="date" ng-model="customer.date" placeholder="Date" autocomplete="off" required />
                            <i class="fa fa-clock-o color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="customerForm.date.$invalid && customerForm.date.$dirty"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!customerForm.date.$invalid && customerForm.date.$dirty"></span>
                            <span class="input-group-btn">
                                <button type="button" class="btn" ng-class="( logout.calendars[0].open ? 'btn-primary' : 'btn-default' )"><i class="fa fa-calendar"></i></button>
                            </span>
                            <div uib-dropdown-menu>
                                <datetimepicker data-ng-model="customer.date" data-datetimepicker-config="{ modelType:'YYYY-MM-DD', minView:'day' }" data-on-set-time="dateSelect( customerForm , 'date' , 1 )"></datetimepicker>
                            </div>
                        </div>

                        <!-- customer Total Spent -->
                        <div class="form-group clearfix" ng-class="{ 'has-error' : customerForm.spent.$invalid && customerForm.spent.$dirty , 'has-success' : !customerForm.spent.$invalid && customerForm.spent.$dirty }">
                            <label class="hide" for="sales">Total Spent:</label>
                            <input type="text" id="sales" name="sales" placeholder="Total Spent" ng-model="customer.spent" currency required />
                            <i class="fa fa-usd color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="customerForm.spent.$invalid && customerForm.spent.$dirty"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!customerForm.spent.$invalid && customerForm.spent.$dirty"></span>
                        </div>

                        <!-- Back Button -->
                        <div class="fleft">
                            <a ng-click="back('/affiliate/customer(/[0-9]+)?' , '/affiliate/customer')" class="btn bgeee coloraaa hoverbg555 hovercolorfff">
                                <i class="fa fa-arrow-circle-o-left"></i>
                                Back to List
                            </a>
                        </div>


                        <div class="fright">

                            <!-- Form Error Notice -->
                            <small class="colorred submitnotice" ng-show="hasError( customerForm )">
                                You must fix any form errors before proceeding * See Errors at Top *
                            </small>

                            <!-- Submitting Wait Notice -->
                            <small class="colorinfo submitnotice" ng-show="sending">
                                <i class="fa fa-circle-o-notch fa-spin colorinfo"></i>
                                @{{ running }} customer - Please wait.
                            </small>

                            <!-- Submit Button -->
                            <button name="submit" class="btn fright" ng-class="hasError( customerForm ) || errors.length ? 'btn-danger' : 'btn-primary'" ng-disabled="customerForm.$invalid || sending">
                                <i class="fa fa-@{{ icon }}"></i>
                                @{{ button }}
                            </button>

                            <!-- /End .fright -->
                        </div>

                        <!-- /End .form-customer -->
                    </div>
                    <div ng-show="affiliate.total == 0">
                        <p class="text-center" ng-show="hasPermission('affiliate.create')">You haven't added any affiliate! <a ng-click="open('/affiliate/add')">Do it</a> if wont continue.</p>
                        <p class="text-center" ng-show="!hasPermission('affiliate.create')">It is impossible to continue the affiliate not found.</p>
                    </div>

                    <!-- /End .panel-body -->
                </div>

            </main>

        </form>

