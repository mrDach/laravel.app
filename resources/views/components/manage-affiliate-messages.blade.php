
        <!-- components.manage-message form -->
        <form name="messageForm" method="post" ng-submit="submit( messageForm )" novalidate>

            <!-- components.manage-message success -->
            <div class="bs-callout bs-callout-info" ng-show="success.length && messageForm.$pristine">
                <h4 class="font2">Success!</h4>
                <p>@{{ success }}</p>
            </div>

            <!-- components.manage-message error -->
            <div class="bs-callout bs-callout-danger" ng-show="hasError( messageForm ) || errors.length">
                <h4 class="font2">Form Errors</h4>
                <div>
                    <ul>

                        <!-- General Errors -->
                        <li ng-repeat="error in errors">
                            @{{ error }}
                        </li>

                        <!-- Group Name Error -->
                        <li ng-show="messageForm.error.affiliate = ( messageForm.affiliate.$dirty && messageForm.affiliate.$invalid )" ng-messages="messageForm.affiliate.$error">
                            <span ng-message="required">Affiliate is required</span>
                        </li>

                        <!-- Group Description Error -->
                        <li ng-show="messageForm.error.text = ( messageForm.text.$dirty && messageForm.text.$invalid )" ng-messages="messageForm.text.$error">
                            <span ng-message="required">Text is required</span>
                        </li>

                    </ul>
                </div>
            </div>

            <!-- components.manage-message success -->
            <main id="message-management" class="border panel panel-default">
                <header class="font2 panel-heading">

                    <i class="fa fa-@{{ icon }}"></i>
                    @{{ title }}

                </header>
                <div class="panel-body">

                    <div class="form-message" ng-show="affiliate.total > 0">
                        <!-- Affiliate Name -->
                        <div class="form-group clearfix" ng-class="{ 'has-error' : messageForm.affiliate.$invalid && messageForm.affiliate.$dirty && !message.affiliate.id && noResults, 'has-success' : !messageForm.affiliate.$invalid && messageForm.affiliate.$dirty && message.affiliate.id && !noResults && !loadingAffiliate}">
                            <label class="hide" for="affiliate">Affiliate:</label>
                            <input type="text" id="affiliate" name="affiliate" placeholder="Affiliate" ng-model="message.affiliate" uib-typeahead="affiliate as affiliate.firstname+' '+affiliate.lastname for affiliate in getAffiliate($viewValue)" typeahead-loading="loadingAffiliate" typeahead-no-results="noResults" required affiliate autocomplete="off"/>
                            <i class="fa fa-male color888 fleft"></i>
                            <div class="loading-results" ng-show="loadingAffiliate">
                                <i class="glyphicon glyphicon-refresh"></i> Loading Results
                            </div>
                            <div class="no-results" ng-show="noResults">
                                <i class="glyphicon glyphicon-remove"></i> No Results Found
                            </div>
                            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback"></span>
                        </div>

                        <!-- Message Text -->
                        <div class="form-group clearfix" ng-class="{ 'has-error' : messageForm.text.$invalid && messageForm.text.$dirty , 'has-success' : !messageForm.text.$invalid && messageForm.text.$dirty }">
                            <label class="hide" for="text">Text</label>
                            <text-angular id="text" name="text" placeholder="Text" ng-model="message.text" required></text-angular>
                            <i class="fa fa-comments color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="messageForm.text.$invalid && messageForm.text.$dirty"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback" ng-show="!messageForm.text.$invalid && messageForm.text.$dirty"></span>
                        </div>

                        <!-- Back Button -->
                        <div class="fleft">
                            <a ng-click="back('/affiliate/message(/[0-9]+)?' , '/affiliate/message')" class="btn bgeee coloraaa hoverbg555 hovercolorfff">
                                <i class="fa fa-arrow-circle-o-left"></i>
                                Back to List
                            </a>
                        </div>

                        <div class="fright">

                            <!-- Form Error Notice -->
                            <small class="colorred submitnotice" ng-show="hasError( messageForm )">
                                You must fix any form errors before proceeding * See Errors at Top *
                            </small>

                            <!-- Submitting Wait Notice -->
                            <small class="colorinfo submitnotice" ng-show="sending">
                                <i class="fa fa-circle-o-notch fa-spin colorinfo"></i>
                                @{{ running }} message - Please wait.
                            </small>

                            <!-- Submit Button -->
                            <button name="submit" class="btn fright" ng-class="hasError( messageForm ) || errors.length ? 'btn-danger' : 'btn-primary'" ng-disabled="messageForm.$invalid || sending">
                                <i class="fa fa-@{{ icon }}"></i>
                                @{{ button }}
                            </button>

                            <!-- /End .fright -->
                        </div>

                        <!-- /End .form-message -->
                    </div>
                    <div ng-show="affiliate.total == 0">
                        <p class="text-center" ng-show="hasPermission('affiliate.create')">You haven't added any affiliate! <a ng-click="open('/affiliate/add')">Do it</a> if wont continue.</p>
                        <p class="text-center" ng-show="!hasPermission('affiliate.create')">It is impossible to continue the affiliate not found.</p>
                    </div>
                    <!-- /End .panel-body -->
                </div>

            </main>

        </form>

