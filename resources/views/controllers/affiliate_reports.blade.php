

        <!-- controllers.report success -->
        <div class="bs-callout bs-callout-info" ng-show="success.length"> 
            <h4 class="font2">Success!</h4> 
            <p>@{{ success }}</p>
        </div>    


        <!-- controllers.report delete -->
        <div class="bs-callout bs-callout-danger" ng-show="errors.length"> 
            <h4 class="font2">Form Errors</h4> 
            <div>
                <ul>
                    <li ng-repeat="error in errors">
                        @{{ error }}
                    </li>
                </ul>
            </div>
        </div>

        <!-- components.manage-logout error -->
        <div class="bs-callout bs-callout-danger" ng-show="hasError( searchForm ) || errors.length">
            <h4 class="font2">Form Errors</h4>
            <div>
                <ul>

                    <!-- Start Date Error -->
                    <li ng-show="searchForm.error[ 'start' ] = searchForm.start.$invalid && searchForm.start.$dirty" ng-messages="searchForm.start.$error">
                        <span ng-message="required">Start Date is required</span>
                        <span ng-message="date-max">Start Time cannot start after the End Time</span>
                        <span ng-message="date-invalid">Start Time Requires a Valid Date</span>
                    </li>

                    <!-- End Date Error -->
                    <li ng-show="searchForm.error[ 'end' ] = searchForm.end.$invalid && searchForm.end.$dirty" ng-messages="searchForm.end.$error">
                        <span ng-message="required">End Date is required</span>
                        <span ng-message="date-min">End Time cannot start before the Start Time</span>
                        <span ng-message="date-invalid">End Time Requires a Valid Date</span>
                    </li>

                </ul>
            </div>
        </div>

        <!-- controllers.report -->
        <section id="report" class="border panel panel-default">
            <header class="font2 panel-heading clearfix">
                
                <i class="fa fa-area-chart"></i>
                Reports

                <div class="fright">

                    <pagination-limit class="fleft"></pagination-limit>

                    <span class="vdivider bgddd"></span>

                </div>
            </header>
            <div class="body clearfix panel-body">
                <!-- Search Form -->
                <form class="search clearfix" name="searchForm" ng-submit="search( searchForm )" novalidate>
                    <section class="col-md-4 column">
                        <div>
                            <label for="search" class="color000 font2 hide">Search:</label>
                            <input type="text" name="query" ng-model="list.query" placeholder="Search" ng-keyup="search( searchForm )" />
                            <i class="fa fa-sitemap color888 fleft"></i>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </section>
                    <section class="col-md-3 column">
                        <!-- Start Date -->
                        <div class="form-group clearfix" uib-dropdown uib-dropdown-toggle is-open="list.calendars[0].open" ng-class="{ 'has-error' : searchForm.start.$invalid && searchForm.start.$dirty , 'has-success' : !searchForm.start.$invalid && searchForm.start.$dirty }">
                            <label class="hide" for="start">Start Date:</label>
                            <input type="datetime" id="start" name="start" ng-model="list.start" placeholder="Start Date" date-compare date-max="list.end" autocomplete="off" ng-change="dateSelect( searchForm , 'start' , 1 )" />
                            <i class="fa fa-clock-o color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="searchForm.start.$invalid && searchForm.start.$dirty"></span>
                            <span class="input-group-btn">
                                <button type="button" class="btn" ng-class="( list.calendars[0].open ? 'btn-primary' : 'btn-default' )"><i class="fa fa-calendar"></i></button>
                            </span>
                            <div uib-dropdown-menu>
                                <datetimepicker data-ng-model="list.start" data-datetimepicker-config="{ modelType:'YYYY-MM-DD', minView:'day'  }" data-on-set-time="dateSelect( searchForm , 'start' , 1 )"></datetimepicker>
                            </div>
                        </div>
                    </section>
                    <section class="col-md-3 column">
                        <!-- End Date -->
                        <div class="form-group clearfix" uib-dropdown uib-dropdown-toggle is-open="list.calendars[1].open" ng-class="{ 'has-error' : searchForm.end.$invalid && searchForm.end.$dirty , 'has-success' : !searchForm.end.$invalid && searchForm.end.$dirty }">
                            <label class="hide" for="end">End Date:</label>
                            <input type="datetime" id="end" name="end" ng-model="list.end" placeholder="End Date" date-compare date-min="list.start" autocomplete="off" ng-change="dateSelect( searchForm , 'end' , 2 )" />
                            <i class="fa fa-clock-o color888 fleft"></i>
                            <span class="glyphicon glyphicon-remove form-control-feedback" ng-show="searchForm.end.$invalid && searchForm.end.$dirty"></span>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-class="( list.calendars[1].open ? 'btn-primary' : 'btn-default' )"><i class="fa fa-calendar"></i></button>
                            </span>
                            <div uib-dropdown-menu>
                                <datetimepicker data-ng-model="list.end" data-datetimepicker-config="{ modelType:'YYYY-MM-DD', minView:'day'  }" data-on-set-time="dateSelect( searchForm , 'end' , 2 )"></datetimepicker>
                            </div>
                        </div>
                    </section>
                    <section class="col-md-2 column">
                        <div>
                            <div class="select" select-wrapper>
                                <select ng-model="list.sort" ng-change="search( searchForm )">
                                    <option value="">Sort by</option>
                                    <option value="firstname">Name</option>
                                    <option value="count_customers">Customers</option>
                                    <option value="sum_spent">Spent</option>
                                </select>
                            </div>
                            <i class="fa fa-sort-amount-desc color888 fleft"></i>
                        </div>
                    </section>
                </form>
                 <table class="table table-bordered table-striped list">
                    <thead>
                        <tr>
                            <th class="text-left font2 color000">Affiliate Name</th>
                            <th class="text-left font2 color000">Total Customers</th>
                            <th class="text-left font2 color000">Total Spent</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <!-- List -->
                        <tr ng-show="!list.loading" dir-paginate="report in page.data | itemsPerPage:page.showing" total-items="page.total" current-page="page.current">
                            <td class="color111">@{{ report.firstname }} @{{ report.lastname }}</td>
                            <td class="color111">@{{ report.count_customers }}</td>
                            <td class="color111">@{{ report.sum_spent || 0 | number : 2 }}</td>
                        </tr>

                        <!-- No Results -->
                        <tr ng-show="page.data.length == 0 && !list.loading">
                            <td class="text-center" colspan="100%">No Reports Yet!</td>
                        </tr>

                        <!-- Loading -->
                        <tr ng-show="list.loading">
                            <td colspan="100%" class="bgfff">
                                <div class="text-center font2 color000">
                                    <i class="fa fa-circle-o-notch fa-spin color000" style="font-size:20px;margin-right:5px;"></i>
                                    <span style="font-size:15px;">Loading</span>
                                </div>
                            </td>
                        </tr>

                    </tbody>
                </table>



                <dir-pagination-controls on-page-change="load(newPageNumber)"></dir-pagination-controls>
            </div>
        </section>

