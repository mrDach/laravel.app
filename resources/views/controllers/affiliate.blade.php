

        <!-- controllers.affiliate success -->
        <div class="bs-callout bs-callout-info" ng-show="success.length">
            <h4 class="font2">Success!</h4>
            <p>@{{ success }}</p>
        </div>


        <!-- controllers.affiliate delete -->
        <div class="bs-callout bs-callout-danger" ng-show="errors.length">
            <h4 class="font2">Form Errors</h4>
            <div>
                <ul>
                    <li ng-repeat="error in errors">
                        @{{ error }}
                    </li>
                </ul>
            </div>
        </div>

        <!-- controllers.affiliate -->
        <section id="list" class="border panel panel-default">
            <header class="font2 panel-heading clearfix">

                <i class="fa fa-user"></i>
                Affiliates List

                <div class="fright">

                    <pagination-limit class="fleft"></pagination-limit>

                    <span class="vdivider bgddd fleft"></span>

                    <a class="fright colorfff font2 btn-sm btn-primary" ng-click="open('/affiliate/add')" ng-show="hasPermission('affiliate.create')">
                        Manage Affiliates
                        <i class="fa fa-arrow-circle-o-right"></i>
                    </a>


                </div>
            </header>
            <div class="body clearfix panel-body">
                <!-- Search Form -->
                <form class="search clearfix" name="searchForm" ng-submit="search( searchForm )" novalidate>
                    <section class="col-md-8 column">
                        <div>
                            <label for="search" class="color000 font2 hide">Search:</label>
                            <input type="text" name="query" ng-model="list.query" placeholder="Search" ng-keyup="search( searchForm )" />
                            <i class="fa fa-sitemap color888 fleft"></i>
                            <button name="submit" class="fa fa-search"></button>
                        </div>
                    </section>
                    <section class="col-md-4 column">
                        <div>
                            <div class="select" select-wrapper>
                                <select ng-options="item.id as item.name for item in list.groups" ng-model="list.group" ng-change="search( searchForm )">
                                    <option value="">Group</option>
                                </select>
                            </div>
                            <i class="fa fa-sort-amount-desc color888 fleft"></i>
                        </div>
                    </section>
                </form>
                <table class="table table-bordered table-striped list">

                    <thead>
                    <tr>
                        <th class="text-center font2 color000">#</th>
                        <th class="text-left font2 color000">Name</th>
                        <th class="text-left font2 color000">Branch</th>
                        <th class="text-left font2 color000">Department</th>
                        <th class="text-left font2 color000">Status</th>
                        <th class="text-left font2 color000">Group</th>
                        <th class="text-center actions font2 color000">Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    <!-- List -->
                    <tr dir-paginate="affiliate in page.data | itemsPerPage:page.showing" total-items="page.total" current-page="page.current">
                        <td class="text-center">@{{ affiliate.id }}</td>
                        <td>@{{ affiliate.firstname }} @{{ affiliate.lastname }}</td>
                        <td>@{{ affiliate.branch.name }}</td>
                        <td>@{{ affiliate.department.name }}</td>
                        <td>@{{ affiliate.user_status }}</td>
                        <td>@{{ affiliate.group.name }}</td>
                        <td class="text-center actions">
                            <a class="btn-sm btn-primary" ng-click="open('/affiliate/' + affiliate.id + '/award')" ng-show="hasPermission('affiliate.award')">Awards</a>
                            <a class="btn-sm btn-danger" ng-click="delete( affiliate )" ng-show="hasPermission('affiliate.delete')">Delete</a>
                        </td>
                    </tr>

                    <!-- No Results -->
                    <tr ng-show="page.data.length == 0">
                        <td class="text-center" colspan="100%">You haven't added any affiliates!</td>
                    </tr>

                    </tbody>
                </table>
                <dir-pagination-controls on-page-change="load(newPageNumber)"></dir-pagination-controls>
            </div>
        </section>

