        
        
        <!-- controllers.award success -->
        <div class="bs-callout bs-callout-info" ng-show="success.length">
            <h4 class="font2">Success!</h4>
            <p>@{{ success }}</p>
        </div>
        
        
        <!-- controllers.award delete -->
        <div class="bs-callout bs-callout-danger" ng-show="errors.length">
            <h4 class="font2">Form Errors</h4>
            <div>
                <ul>
                    <li ng-repeat="error in errors">
                        @{{ error }}
                    </li>
                </ul>
            </div>
        </div>
        
        <!-- controllers.award -->
        <section id="award" class="border panel panel-default">
            <header class="font2 panel-heading clearfix">
        
                <i class="fa fa-trophy"></i>
                Awards List
        
                <div class="fright">
        
                    <pagination-limit class="fleft"></pagination-limit>
        
                    <span class="vdivider bgddd fleft"></span>

                    <a class="fright colorfff font2 btn-sm btn-primary" ng-click="open('/affiliate/award/add')" ng-show="hasPermission('award.create')">
                        Add Award
                        <i class="fa fa-arrow-circle-o-right"></i>
                    </a>
        
                </div>
            </header>
            <div class="body clearfix panel-body">
                <table class="table table-bordered table-striped list">
        
                    <thead>
                    <tr>
                        <th class="text-center font2 color000">#</th>
                        <th class="text-left font2 color000">Name</th>
                        <th class="text-left font2 color000">Customers Required</th>
                        <th class="text-left font2 color000">Start</th>
                        <th class="text-left font2 color000">End</th>
                        <th class="text-left font2 color000">Status</th>
                        <th class="text-center actions font2 color000">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
        
                    <!-- List -->
                    <tr dir-paginate="award in page.data | itemsPerPage:page.showing" total-items="page.total" current-page="page.current">
                        <td class="text-center">@{{ award.id }}</th>
                        <td>@{{ award.name }}</th>
                        <td>@{{ award.customers }}</td>
                        <td>@{{ award.start }}</td>
                        <td>@{{ award.end }}</td>
                        <td>@{{ award.status }}</td>
                        <td class="text-center actions">
                            <a class="btn-sm btn-primary" ng-click="open('/affiliate/award/edit/' + award.id )" ng-show="hasPermission('award.edit')">Edit</a>
                            <a class="btn-sm btn-danger" ng-click="delete( award )" ng-show="hasPermission('award.delete')">Delete</a>
                        </td>
                    </tr>
        
                    <!-- No Results -->
                    <tr ng-show="page.data.length == 0">
                        <td class="text-center" colspan="100%">You haven't added any awards!</td>
                    </tr>
        
                    </tbody>
                </table>
                <dir-pagination-controls on-page-change="load(newPageNumber)"></dir-pagination-controls>
            </div>
        </section>
        
