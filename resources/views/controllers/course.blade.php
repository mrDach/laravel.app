

        <!-- controllers.course success -->
        <div class="bs-callout bs-callout-info" ng-show="success.length">
            <h4 class="font2">Success!</h4>
            <p>@{{ success }}</p>
        </div>


        <!-- controllers.course delete -->
        <div class="bs-callout bs-callout-danger" ng-show="errors.length">
            <h4 class="font2">Form Errors</h4>
            <div>
                <ul>
                    <li ng-repeat="error in errors">
                        @{{ error }}
                    </li>
                </ul>
            </div>
        </div>

        <!-- controllers.course -->
        <section id="course" class="border panel panel-default">
            <header class="font2 panel-heading clearfix">

                <i class="fa fa-university"></i>
                Courses List

                <div class="fright">

                    <pagination-limit class="fleft"></pagination-limit>

                    <span class="vdivider bgddd fleft"></span>

                    <a class="fright colorfff font2 btn-sm btn-primary" ng-click="open('/course/add')" ng-show="hasPermission('course.create')">
                        Add Course
                        <i class="fa fa-arrow-circle-o-right"></i>
                    </a>

                </div>
            </header>
            <div class="body clearfix panel-body">
                <table class="table table-bordered table-striped list">

                    <thead>
                    <tr>
                        <th class="text-center font2 color000">#</th>
                        <th class="text-left font2 color000">Name</th>
                        <th class="text-left font2 color000">Test Date</th>
                        <th class="text-left font2 color000">Score</th>
                        <th class="text-left font2 color000">Status</th>
                        <th class="text-center actions font2 color000">Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    <!-- List -->
                    <tr dir-paginate="course in page.data | itemsPerPage:page.showing" total-items="page.total" current-page="page.current">
                        <td class="text-center">@{{ course.id }}</td>
                        <td>
                            <p>@{{ course.name }}</p>
                            <p class="description">@{{ course.description }}</p>
                        </td>
                        <td>@{{ course.status.data }}</td>
                        <td>@{{ (100/course.status.questions)*course.status.correct || 0 | number : 2 }}%</td>
                        <td><span class="label label-@{{ course.status.type }}">@{{ course.status.type | uppercase }}</span></td>
                        <td class="text-center actions">
                            <a class="btn-sm btn-default" ng-click="open('/course/take/' + course.id )" ng-show="hasPermission('course')">Take</a>
                            <a class="btn-sm btn-primary" ng-click="open('/course/edit/' + course.id )" ng-show="hasPermission('course.edit')">Edit</a>
                            <a class="btn-sm btn-danger" ng-click="delete( course )" ng-show="hasPermission('course.delete')">Delete</a>
                        </td>
                    </tr>

                    <!-- No Results -->
                    <tr ng-show="page.data.length == 0">
                        <td class="text-center" colspan="100%">You haven't added any courses!</td>
                    </tr>

                    </tbody>
                </table>
                <dir-pagination-controls on-page-change="load(newPageNumber)"></dir-pagination-controls>
            </div>
        </section>

