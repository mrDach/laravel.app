<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Search as AffiliateSearch;
use App\Contracts\Search as AffiliateSearchContract;


class Affiliate extends Model implements AffiliateSearchContract {

    use AffiliateSearch;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'affiliates';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'firstname' , 'lastname', 'branch_id', 'department_id', 'payroll_status', 'affiliate_status_id', 'affiliate_group_id', 'email'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

















    /**
     *
     *   status
     *       - Loads the Belongs to Many Relationship AffiliateStatus
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate status
     *
     **/
    public function status(){

        return $this->belongsTo('App\Models\AffiliateStatus','affiliate_status_id');

    }

















    /**
     *
     *   group
     *       - Loads the Belongs to Many Relationship AffiliateGroup
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate group
     *
     **/
    public function group(){

        return $this->belongsTo('App\Models\AffiliateGroup','affiliate_group_id');

    }















    /**
     *
     *   customers
     *       - Loads the Belongs to Many Relationship Customers
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The customer
     *
     **/
    public function customers(){

        return $this->hasMany('App\Models\Customer');

    }















    /**
     *
     *   awards
     *       - Loads the Belongs to Many Relationship awards
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The award
     *
     **/
    public function awards(){

        return $this->belongsToMany('App\Models\Award', 'affiliate_awards', 'affiliate_id', 'award_id')->withTimestamps();

    }















    /**
     *
     *   departments
     *       - Loads the Belongs to Many Relationship departments
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The award
     *
     **/
    public function department(){

        return $this->belongsTo('App\Models\Department', 'department_id', 'id');

    }















    /**
     *
     *   branches
     *       - Loads the Belongs to Many Relationship branches
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The award
     *
     **/
    public function branch(){

        return $this->belongsTo('App\Models\Branch', 'branch_id', 'id');

    }

}