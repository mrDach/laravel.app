<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Answer;


class Result extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'results';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id' , 'user_id' , 'course_id' , 'score', 'data', 'created_at', 'updated_at'];
















    /**
     *
     *   course
     *       - Loads the Belongs to Relationship course
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The course
     *
     **/
    public function course(){

        return $this->belongsTo('App\Models\Course');

    }
















    /**
     *
     *   user
     *       - Loads the Belongs to Many Relationship user
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The user
     *
     **/
    public function user(){

        return $this->belongsTo('App\Models\User');

    }
}