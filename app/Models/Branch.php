<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Branch extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branches';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

















    /**
     *
     *   affiliate
     *       - Loads the Belongs to Many Relationship affiliate
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate group
     *
     **/
    public function affiliate(){

        return $this->hasMany('App\Models\Affiliate','branch_id');

    }
















    /**
     *
     *   awards
     *       - Loads the Belongs to Many Relationship awards
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate group
     *
     **/
    public function awards(){

        return $this->belongsToMany('App\Models\Award', 'award_branches', 'branch_id', 'award_id');

    }

}