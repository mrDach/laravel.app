<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Search as LogoutSearch;
use App\Contracts\Search as LogoutSearchContract;

class Logout extends Model implements LogoutSearchContract {

	use LogoutSearch;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'logouts';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'store_id' , 'start' , 'end' , 'recap' , 'lymtd' , 'mtd' , 'sales' , 'returns' , 'traffic' , 'conversions' , 'insoles' , 'notes' , 'saved' , 'creator_id'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    














    /**
    *
    *   notifications
    *       - Loads the Belongs to Many Relationship Attachments
    *
    *   URL Params:
    *       n/a
    *
    *
    *   Returns (Object):
    *       1. The Notification to Create
    *
    **/
    public function notifications(){

        return $this->morphMany('App\Models\Notification', 'link');

    }


    














    /**
    *
    *   store
    *       - Loads the Store Relationship
    *
    *   URL Params:
    *       n/a
    *
    *
    *   Returns (Object):
    *       1. The Store to Load
    *
    **/
    public function store(){

        return $this->hasOne('App\Models\Store', 'id' , 'store_id');

    }


    














    /**
    *
    *   staff
    *       - Loads the Store Relationship
    *
    *   URL Params:
    *       n/a
    *
    *
    *   Returns (Object):
    *       1. The Store to Load
    *
    **/
    public function staff(){

        return $this->belongsToMany('App\Models\User', 'logout_staff')->withPivot('hours');

    }

















    /**
    *
    *   creator
    *       - Loads the User Relationship
    *
    *   URL Params:
    *       n/a
    *
    *
    *   Returns (Object):
    *       1. The User to Load
    *
    **/
    public function creator(){

        return $this->hasOne('App\Models\User', 'id' , 'creator_id');

    }



}
