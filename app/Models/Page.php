<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Page extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pages';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id' , 'course_id' , 'attachment_id' , 'subject' , 'article', 'created_at', 'updated_at'];
















    /**
     *
     *   course
     *       - Loads the Belongs to Relationship course
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The course
     *
     **/
    public function course(){

        return $this->belongsTo('App\Models\Course');

    }
















    /**
     *
     *   attachment
     *       - Loads the Belongs to Relationship Attachment
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The course
     *
     **/
    public function attachment(){

        return $this->belongsTo('App\Models\Attachment', 'attachment_id' , 'id' );

    }

}