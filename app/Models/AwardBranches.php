<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AwardBranches extends Model
{




    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'award_branches';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['award_id','branch_id'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}
