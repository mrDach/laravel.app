<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{




    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname','lastname', 'affiliate_id', 'date', 'spent'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

















    /**
     *
     *   affiliate
     *       - Loads the Belongs to Many Relationship affiliate
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate
     *
     **/
    public function affiliate(){

        return $this->belongsTo('App\Models\Affiliate','affiliate_id');

    }
}
