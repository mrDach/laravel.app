<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Answer;


class Test extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tests';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id' , 'course_id' , 'answer_id' , 'time', 'question', 'created_at', 'updated_at'];
















    /**
     *
     *   course
     *       - Loads the Belongs to Relationship course
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The course
     *
     **/
    public function course(){

        return $this->belongsTo('App\Models\Course');

    }
















    /**
     *
     *   answers
     *       - Loads the Belongs to Many Relationship answers
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The answer
     *
     **/
    public function answers(){

        return $this->hasMany('App\Models\Answer');

    }

















    /**
     *
     *   answers
     *      - save, create or delete associated Tests
     *
     *   URL Params:
     *      - answers:    (ARRAY) Array answers
     *
     *
     *   Returns (Object):
     *
     *
     **/
    public function setAnswersAttribute($answers)
    {
        foreach ($answers as $answer){
            if(isset($answer['id'])) {
                if(isset($answer['deleted']) && $answer['deleted']){
                    Answer::where('id','=',$answer['id'])->delete();
                }else{
                    Answer::where('id','=',$answer['id'])->update($answer);
                }
            }else{
                $answer['test_id'] = $this->id;
                Answer::create($answer);
            }
        }
        if(empty($answers)){
            Answer::where('test_id','=',$this->id)->delete();
        }
    }

}