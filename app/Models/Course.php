<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Auth;


class Course extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'courses';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id' , 'name', 'description', 'creator_id', 'published', 'passing_score', 'created_at', 'updated_at'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['status'];
















    /**
     *
     *   stores
     *       - Loads the Belongs to Many Relationship stores
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The store
     *
     **/
    public function stores(){

        return $this->belongsToMany('App\Models\Store', 'course_stores', 'course_id', 'store_id');

    }

















    /**
     *
     *   stores
     *      reset associated stores
     *
     *   URL Params:
     *       - stores:    (ARRAY) Array stores
     *
     *
     *   Returns (Object):
     *
     **/
    public function setStoresAttribute($stores)
    {
        $collect_stores = collect($stores);

        //delete old stores
        $diff = $this->stores->diffKeys($collect_stores);

        $store_ids = array();
        foreach ($diff->toArray() as $store){
            $store_ids[] = $store['id'];
        }
        if(!empty($store_ids))
            $this->stores()->detach($store_ids);

        //add new stores
        if(!empty($stores)) {

            $diff = $collect_stores->diffKeys($this->stores);

            $store_ids = array();
            foreach ($diff->toArray() as $store) {
                $store_ids[] = $store['id'];
            }

            if (!empty($store_ids))
                $this->stores()->attach($store_ids);
        }
    }
















    /**
     *
     *   roles
     *       - Loads the Belongs to Many Relationship roles
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The role
     *
     **/
    public function roles(){

        return $this->belongsToMany('App\Models\Role', 'course_roles', 'course_id', 'role_id');

    }

















    /**
     *
     *   roles
     *      reset associated roles
     *
     *   URL Params:
     *      - roles:    (ARRAY) Array roles
     *
     *
     *   Returns (Object):
     *
     **/
    public function setRolesAttribute($roles)
    {
        $collect_roles = collect($roles);

        //delete old roles
        $diff = $this->roles->diffKeys($collect_roles);

        $role_ids = array();
        foreach ($diff->toArray() as $role){
            $role_ids[] = $role['id'];
        }
        if(!empty($role_ids))
            $this->roles()->detach($role_ids);

        //add new roles
        if(!empty($roles)) {

            $diff = $collect_roles->diffKeys($this->roles);

            $role_ids = array();
            foreach ($diff->toArray() as $role) {
                $role_ids[] = $role['id'];
            }

            if (!empty($role_ids))
                $this->roles()->attach($role_ids);
        }
    }
















    /**
     *
     *   pages
     *       - Loads the Belongs to Many Relationship pages
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The page
     *
     **/
    public function pages(){

        return $this->hasMany('App\Models\Page');

    }

















    /**
     *
     *   pages
     *      - save, create or delete associated Pages
     *
     *   URL Params:
     *      - pages:    (ARRAY) Array pages
     *
     *
     *   Returns (Object):
     *
     **/
    public function setPagesAttribute($pages)
    {
        foreach ($pages as $page){
            unset($page['attachment']);
            if(isset($page['id'])) {
                if(isset($page['deleted']) && $page['deleted']){
                    Page::where('id','=',$page['id'])->delete();
                }else{
                    Page::where('id','=',$page['id'])->update($page);
                }
            }else{
                $page['course_id'] = $this->id;
                Page::create($page);
            }
        }
        if(empty($pages)){
            Page::where('course_id','=',$this->id)->delete();
        }
    }
















    /**
     *
     *   tests
     *       - Loads the Belongs to Many Relationship tests
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The test
     *
     **/
    public function tests(){

        return $this->hasMany('App\Models\Test');

    }

















    /**
     *
     *   tests
     *      - save, create or delete associated Tests
     *
     *   URL Params:
     *      - tests:    (ARRAY) Array tests
     *
     *
     *   Returns (Object):
     *
     *
     **/
    public function setTestsAttribute($tests)
    {
        foreach ($tests as $test){
            if(isset($test['id'])) {
                if(isset($test['deleted']) && $test['deleted']){
                    Test::where('id','=',$test['id'])->delete();
                }else{
                    $objTest = Test::find($test['id']);
                    if(isset($test['answers'])){
                        $objTest->answers = $test['answers'];
                    }
                    unset($test['answers']);
                    $objTest->update($test);
                }
            }else{
                $test['course_id'] = $this->id;
                Test::create($test);
            }
        }
        if(empty($tests)){
            Test::where('course_id','=',$this->id)->delete();
        }
    }
















    /**
     *
     *   result
     *       - Loads the Belongs to Many Relationship Result
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The result
     *
     **/
    public function result(){

        return $this->hasMany('App\Models\Result');

    }

















    /**
     *
     *   status
     *      get status Course for user
     *
     *   URL Params:
     *      - user_id:      (INT) User id
     *      - full_info:    (BOOL) get all info or only string status
     *
     *
     *   Returns (ARRAY/STRING):
     *
     *
     **/
    public function status($user_id = null, $full_info = false){

        if($user_id === null)
            $user_id = Auth::user()->id;

        $return = [
            'type' => 'pending',
        ];

        $result = $this->result()->where('user_id','=',$user_id)->first();

        if(!$result){

            if(!$full_info)
                return 'pending';

            return $return;
        }

        $result = $result->toArray();
        $questions = $this->tests()->count();

        $percent = (100 / $questions) * $result['score'];

        $satisfactory_score = (((float)$this->passing_score - 10) > 0) ? (float)$this->passing_score - 10 : 0;

        $return = [
            'correct' => (int)$result['score'],
            'questions' => $questions,
            'data' => $result['updated_at'],
        ];

        if($percent>=(float)$this->passing_score){

            if(!$full_info)
                return 'passing';
            $return['type'] = 'passed';

        }elseif ($percent<=$satisfactory_score){

            if(!$full_info)
                return 'failed';
            $return['type'] = 'failed';

        }else{

            if(!$full_info)
                return 'satisfactory';
            $return['type'] = 'satisfactory';

        }

        if(!$full_info)
            return 'pending';

        return $return;

    }

















    /**
     *
     *   getStatusAttribute
     *      get status attribute
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *
     *
     **/
    public function getStatusAttribute(){

        return $this->status(null,true);

    }

















    /**
     *
     *   creator
     *       - Loads the User Relationship
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The User to Load
     *
     **/
    public function creator(){

        return $this->hasOne('App\Models\User', 'id' , 'creator_id');

    }

}