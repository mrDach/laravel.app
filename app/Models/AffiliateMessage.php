<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliateMessage extends Model
{




    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'affiliate_messages';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['affiliate_id', 'text', 'created_at'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
















    /**
     *
     *   affiliate
     *       - Loads the Belongs to Many Relationship AffiliateGroup
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate
     *
     **/
    public function affiliate(){

        return $this->belongsTo('App\Models\Affiliate');

    }


}
