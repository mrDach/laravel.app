<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Award extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'awards';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['affiliate_group_id' , 'location', 'start', 'end', 'customers', 'name', 'description', 'created_at'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['status'];

















    /**
     *
     *   status
     *      - Active:    (Is the current date within the Start / End Range)
     *      - Ended:     (Is the current date After the End Date)
     *      - Upcoming:  (Is the current date BEFORE the start date)
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate group
     *
     **/
    public function getStatusAttribute()
    {
        $today = date("Y-m-d");
        if($today<$this->start)
            return 'Upcoming';
        if($today>$this->end)
            return 'Ended';
        return 'Active';
    }
















    /**
     *
     *   group
     *       - Loads the Belongs to Many Relationship AffiliateGroup
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate group
     *
     **/
    public function affiliates(){

        return $this->belongsToMany('App\Models\Affiliate', 'affiliate_awards', 'award_id', 'affiliate_id')->withTimestamps();

    }
















    /**
     *
     *   branches
     *       - Loads the Belongs to Many Relationship branches
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate group
     *
     **/
    public function branches(){

        return $this->belongsToMany('App\Models\Branch', 'award_branches', 'award_id', 'branch_id');

    }
















    /**
     *
     *   departments
     *       - Loads the Belongs to Many Relationship branches
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate group
     *
     **/
    public function departments(){

        return $this->belongsToMany('App\Models\Department', 'award_departments', 'award_id', 'department_id');

    }
















    /**
     *
     *   branches
     *       - Loads the Belongs to Many Relationship branches
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate group
     *
     **/
    public function affiliates_from_branches(){

        return $this->hasManyThrough('App\Models\Affiliate', 'App\Models\AwardBranches', 'branch_id', 'branch_id');

    }
















    /**
     *
     *   branches
     *       - Loads the Belongs to Many Relationship AffiliateGroup
     *
     *   URL Params:
     *       n/a
     *
     *
     *   Returns (Object):
     *       1. The affiliate group
     *
     **/
    public function affiliates_from_departments(){

        return $this->hasManyThrough('App\Models\Affiliate', 'App\Models\AwardDepartments', 'department_id', 'department_id');

    }

}