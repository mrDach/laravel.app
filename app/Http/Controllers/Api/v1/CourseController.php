<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\Models\Course;
use App\Models\Attachment;
use App\Models\Test;
use App\Models\Result;

use Input;
use Validator;
use Auth;
use DB;

class CourseController extends Controller {


    /**
     *
     *   getPagedCourses
     *       - Loads All of the Courses
     *
     *   URL Params:
     *       - limit:     (INT) The Page Limit (Default: 15)
     *       - page:      (INT) Pages to Load (Default: 1)
     *
     *
     *   Returns (JSON):
     *       - total      (INT) count Courses in result
     *       - data       (JSON) The Courses List
     *
     **/
    public function getPagedCourses( $limit = 15 , $page = 1 ){

        $query  = Course::where(function($query){

            //Segment by Stores
            $query->where(function($query){

                $stores = Auth::user()->stores()->get()->lists('id');

                $query->whereDoesntHave('stores');

                if( count( $stores ) > 0 ){

                    $query->orWhereHas('stores',function($query) use ($stores) {

                        $query->whereIn('store_id' , $stores );

                    });

                }

            });

            //Segment by Roles
            $query->where(function($query){
            
                $roles = Auth::user()->roles()->get()->lists('id');

                $query->whereDoesntHave('roles');

                if( count( $roles ) > 0 ){

                    $query->orWhereHas('roles',function($query) use ($roles) {

                        $query->whereIn('role_id' , $roles );

                    });

                }

            });

            //Segment by Roles
            $query->where( [
                'published'  => 1,
            ] );

        })->take( $limit )->skip( ( ( $page - 1 ) * $limit ) );

        $total  = Course::count();
        $data   = $query->get(['id','name','description']);

        //Return the Data
        return [
            'total' => $total ,
            'data'  => $data ,
        ];

    }














    /**
     *
     *   editCourses
     *       - edit Courses exist Courses or create new
     *
     *   Params ($_POST):
     *       - stores:           (ARRAY) stores
     *       - roles:            (ARRAY) roles
     *       - pages:            (ARRAY) pages
     *       - tests:            (ARRAY) tests
     *       - name:             (String) The Course's Name
     *       - description:      (String) The Course's Description
     *       - passing_score:    (String) count Customers Required
     *
     *   Returns (JSON):
     *       1. (Bool) Returns True / False
     *
     **/
    public function editCourses(){

        $data           = Input::all();

        $rules = [];

        if(isset($data['published']) && $data['published'] == 1){

            $rules = [
                'id'                     => '',
                'published'              => '',
                'stores'                 => '',
                'roles'                  => '',
                'name'                   => 'required',
                'description'            => 'required',
                'passing_score'          => 'required',
                'pages'                  => '',
                'tests'                  => '',
            ];

            //make validation rules for relations objects
            if (isset($data['stores'])) {
                for ($i = 0; $i < count($data['stores']); $i++) {

                    $rules["stores.{$i}.id"] = 'required|exists:stores,id';

                }
            }

            if (isset($data['roles'])) {
                for ($i = 0; $i < count($data['roles']); $i++) {

                    $rules["roles.{$i}.id"] = 'required|exists:roles,id';

                }
            }

            if (isset($data['pages'])) {
                for ($i = 0; $i < count($data['pages']); $i++) {

                    $rules["pages.{$i}.id"] = "exists:pages,id";
                    $rules["pages.{$i}.attachment_id"] = "exists:attachments,id";
                    if (!isset($data["pages"][$i]['deleted'])) {
                        $rules["pages.{$i}.subject"] = "required_with:pages.{$i}.id";
                        $rules["pages.{$i}.article"] = "required_with:pages.{$i}.id";
                    }

                    //delete unused filed
                    unset($data["pages"][$i]["attachment"]);
                    if (isset($data["pages"][$i]["files"]))
                        unset($data["pages"][$i]["files"]);

                }
            }

            if (isset($data["tests"])) {
                for ($i = 0; $i < count($data["tests"]); $i++) {

                    $rules["tests.{$i}.id"] = "exists:tests,id";

                    if (!isset($data["tests"][$i]['deleted'])) {
                        $rules["tests.{$i}.question"] = "required_with:tests.{$i}.id";
                        $rules["tests.{$i}.time"] = "required_with:tests.{$i}.id";
                    }

                    if (isset($data["tests"]['answer_id']) && $data["tests"]['answer_id'])
                        $rules["tests.{$i}.answer_id"] = "required_with:tests.{$i}.id|exists:answers,id";

                    if (isset($data["tests"]["answer"])) {
                        for ($j = 0; $j < count($data["tests"]["answer"]); $j++) {

                            $rules["tests.{$i}.answer.{$j}.id"] = "exists:answers,id";
                            $rules["tests.{$i}.answer.{$j}.text"] = "required_with:tests.{$i}.answer.{$j}.id";

                        }
                    }
                }
            }
        }

        $validator      = Validator::make( $data, $rules);

        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            DB::beginTransaction();

            try {

                //if we get id this edit. try get Course
                if(isset($data['id'])){
                    $course = Course::find($data['id']);
                }else{
                    $data['creator_id'] = Auth::user()->id;
                    $course = Course::create($data);
                }

                if(isset($data['stores']))
                    $course->stores = $data['stores'];
                if(isset($data['roles']))
                    $course->roles = $data['roles'];
                if(isset($data['pages']))
                    $course->pages = $data['pages'];
                if(isset($data['tests']))
                    $course->tests = $data['tests'];

                $course->update($data);

                DB::commit();
                //get updated course and send it
                return [ 'result' => 1, 'course' => $this->getCourses($course->id) ];

            } catch(Exception $e ){

                DB::rollback();
                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    }














    /**
     *
     *   attach
     *       - Attaches a File
     *
     *   Request Params:
     *       - file:       (FILE) The File to Attach
     *
     *
     *   Returns (JSON):
     *       result:            0/1 operation is successful
     *       attachment_id:     id new Attachment
     *
     **/
    public function attach(){

        if( Input::file('file') ){

            $Attachment = Attachment::store( Input::file('file') , 'public' );

            return [ 'result' => 1 , 'attachment_id' => $Attachment->id ];

        }

        return [ 'result' => 0 , 'error' => 'Invalid File Uploaded' , 'code' => 'invalid-file' ];

    }















    /**
     *
     *   getCourses
     *       - Loads All of the Course
     *
     *   URL Params:
     *       - award_id:       (INT) The Course ID to Lookup
     *
     *
     *   Returns (JSON):
     *       1. The Course Data
     *
     **/
    public function getCourses( $course_id = 1 ){

        return Course::with('stores','roles','pages.attachment','tests.answers')->find( $course_id );

    }















    /**
     *
     *   getCoursesForEdit
     *       - Loads All of the not published Course for continue editing
     *
     *   URL Params:
     *
     *
     *   Returns (JSON):
     *       1. The Course Data
     *
     **/
    public function getCoursesForEdit(){

        return Course::with('stores','roles','pages.attachment','tests.answers')
            ->where( [
                'creator_id' => Auth::user()->id,
                'published'  => 0,
            ] )
            ->first();

    }















    /**
     *
     *   getPages
     *       - Loads All Pages of the Course
     *
     *   URL Params:
     *       - course_id:       (INT) The Course ID to Lookup
     *
     *
     *   Returns (JSON):
     *       1. The Pages Data
     *
     **/
    public function getPages( $course_id = 1 ){

        $course = Course::with('stores','roles','pages.attachment','tests.answers')->find( $course_id )->toArray();

        foreach( $course['pages'] as $key => $page ){

            if($page['attachment']){

                if( $page['attachment']['mime']['type'] == 'image' && in_array( $page['attachment']['mime']['subtype'] , Attachment::$images ) ){

                    $course['pages'][ $key ]['attachment']['image']       = Attachment::URL( $page['attachment']['filename'] );
                    $course['pages'][ $key ]['attachment']['thumbnail']   = Attachment::URL( Attachment::resize( $page['attachment']['filename'] , 228 , 152 , 'public' ) );
                    $course['pages'][ $key ]['attachment']['file']        = Attachment::URL( $page['attachment']['filename'] );

                }else{

                    $course['pages'][ $key ]['attachment']['image']       = Attachment::URL( Attachment::thumbnail( $page['attachment']['filename'] , 'public' ) );
                    $course['pages'][ $key ]['attachment']['thumbnail']   = Attachment::URL( Attachment::resize( Attachment::thumbnail( $page['attachment']['filename'] , 'public' ) , 228 , 152 , 'public' ) );
                    $course['pages'][ $key ]['attachment']['file']        = Attachment::URL( $page['attachment']['filename'] );

                }

            }

        }

        return $course;

    }















    /**
     *
     *   getTests
     *       - Loads All Tests of the Course
     *
     *   URL Params:
     *       - course_id:       (INT) The Course ID to Lookup
     *
     *
     *   Returns (JSON):
     *       1. The Tests Data
     *
     **/
    public function getTests( $course_id = 1 ){

        $data              = Input::all();
        $data['course_id'] = $course_id;

        $validator         = Validator::make( $data, [
            'course_id'    => 'required|exists:courses,id',
        ]);

        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            DB::beginTransaction();

            try {

                //check result Tests for this Course
                $result = Result::where([
                    ['user_id','=', Auth::user()->id],
                    ['course_id','=', $data['course_id']],
                ])->first();

                //if exist result this tests already done. Send results
                if(isset($result->id)){

                    $results = Course::find($data['course_id'])->status(Auth::user()->id, true);

                    return [ 'result' => 0 , 'results' => $results];
                }

                //if not exist create result
                //when we get the answers we write here score
                Result::create([
                    'course_id' => $data['course_id'],
                    'user_id'   => Auth::user()->id,
                ]);

                DB::commit();

                //send tests without correct answer_id)
                $tests = Test::with('answers')->where([
                    ['course_id','=', $data['course_id']],
                ])->get()->makeHidden(['answer_id']);

                //Return Success
                return [ 'result' => 1 , 'tests' => $tests];

            } catch(Exception $e ){

                DB::rollback();
                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    }















    /**
     *
     *   setAnswer
     *       - save the user response to the test
     *
     *   URL Params:
     *       - test_id:         (INT) The Test ID
     *       - answer_id:       (INT) The Answer ID
     *       - course_id:       (INT) The Course ID
     *
     *
     *   Returns (JSON):
     *       1. (Bool) Returns True / False
     *
     **/
    public function setAnswer( $course_id = 1 ){

        $data              = Input::all();
        $data['course_id'] = $course_id;

        $validator         = Validator::make( $data, [
            'test_id'      => 'required|exists:tests,id',
            'answer_id'    => 'required',
            'course_id'    => 'required|exists:courses,id',
        ]);

        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            DB::beginTransaction();

            try {

                $result = Result::where([
                    ['user_id','=', Auth::user()->id],
                    ['course_id','=', $data['course_id']],
                ])->first();

                //if not exist result this tests didn't started. Send error
                if(!isset($result->id)){
                    return [ 'result' => 0 , 'errors' => [ 'You didn\'t starts this test' ] ];
                }

                $test = Test::where([
                    ['id',          '=', $data['test_id']],
                    ['course_id',   '=', $data['course_id']],
                    ['answer_id',   '=', $data['answer_id']],
                ])->first();

                //check time and correct answer
                if ((isset($test->id)) && ($test->answer_id == $data['answer_id'])) {

                    if(Carbon::now()->timestamp < Carbon::createFromFormat("Y-m-d H:i:s",$result->updated_at)->timestamp+Carbon::createFromFormat("H:i:s",$test->time)->timestamp) {

                        $result->score++;
                        $result->save();

                    }
                }

                DB::commit();
                //TODO:  add feedback
                //Return Success
                return [ 'result' => 1 ];

            } catch(Exception $e ){

                DB::rollback();
                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    }















    /**
     *
     *   deleteCourses
     *       - Delete an Courses
     *
     *   Params (URL):
     *       - award_id:                   (INT) The Course ID
     *
     *   Returns (JSON):
     *       1. (Bool) Returns True / False
     *
     **/
    public function deleteCourses( $course_id ){


        if( $course = Course::find( $course_id ) ){

            //remove the associated relations
            $course->stores()->detach();
            $course->roles()->detach();
            $course->pages()->delete();
            $course->tests()->delete();
            $course->result()->delete();

            //Delete the Course
            $course->delete();

            //Return Success
            return [ 'result' => 1 ];

        }

        //Return Failed
        return [ 'result' => 0 , 'errors' => [ 'That Course Doesn\'t exist' ] ];

    }



}