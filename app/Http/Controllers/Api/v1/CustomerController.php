<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

use App\Models\Customer;
use App\Models\Affiliate;

use Input;
use Validator;
use Auth;
use Mail;

class CustomerController extends Controller {


    /**
     *
     *   getPagedCustomers
     *       - Loads All of the Customers
     *
     *   URL Params:
     *       - limit:     (INT) The Page Limit (Default: 15)
     *       - page:      (INT) Pages to Load (Default: 1)
     *
     *
     *   Returns (JSON):
     *       - total      (INT) count Customers in result
     *       - data       (JSON) The Customers List
     *
     **/
    public function getPagedCustomers( $limit = 15 , $page = 1 ){

        $query  = Customer::with('affiliate')->take( $limit )->skip( ( ( $page - 1 ) * $limit ) );

        $total  = Customer::count();
        $data   = $query->get();

        //Return the Data
        return [
            'total' => $total ,
            'data'  => $data
        ];

    }














    /**
     *
     *   createCustomers
     *       - create an Customers
     *
     *   Params ($_PUT):
     *       - name:             (String) The Customer's Name
     *       - description:      (String) The Customer's Description
     *
     *   Returns (JSON):
     *       1. (Bool) Returns True / False
     *
     **/
    public function createCustomers(){


        $data           = Input::all();

        if(!empty($data['spent'])){
            $data['spent'] = preg_replace( '/[^0-9.]/', '' , $data['spent'] );
        }

        if(!empty($data['affiliate'])){
            $data['affiliate_id'] = $data['affiliate']['id'];
            unset($data['affiliate']);
        }

        $validator      = Validator::make( $data, [
            'firstname'         => 'required',
            'lastname'          => 'required',
            'affiliate_id'       => 'required|exists:affiliates,id',
            'date'              => 'required',
            'spent'             => 'required|min:0',
        ]);

        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            try {

                //Create the new Customer
                Customer::create($data);

                $user = Affiliate::find( $data['affiliate_id'] );

                //Send the Email
                Mail::send([ 'html' => 'emails.customer'] ,  [ 'data' => $data ]  , function($message) use ($user){

                    $user->fullname = ( !empty( $user->firstname ) && !empty( $user->lastname ) ? $user->firstname . ' ' . $user->lastname : null );

                    $message->to( $user->email , $user->fullname );

                    //Set Subject
                    $message->subject( env('COMPANY') . ' New Customers: ' . date( 'M d, Y h:ia' ) );

                });

                //TODO:  add feedback
                //Return Success
                return [ 'result' => 1 ];

            } catch(Exception $e ){

                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    }















    /**
     *
     *   deleteCustomers
     *       - Delete an Customers
     *
     *   Params (URL):
     *       - customer_id:                   (INT) The Customer ID
     *
     *   Returns (JSON):
     *       1. (Bool) Returns True / False
     *
     **/
    public function deleteCustomers( $customer_id ){


        if( $group = Customer::find( $customer_id ) ){

            //Delete the Customer
            $group->delete();

            //Return Success
            return [ 'result' => 1 ];

        }

        //Return Failed
        return [ 'result' => 0 , 'errors' => [ 'That Customer Doesn\'t exist' ] ];

    }



}