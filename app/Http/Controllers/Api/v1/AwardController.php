<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

use App\Models\Award;
use App\Models\Affiliate;

use Input;
use Validator;
use Auth;
use Mail;
use DB;

class AwardController extends Controller {


    /**
     *
     *   getPagedAwards
     *       - Loads All of the Awards
     *
     *   URL Params:
     *       - limit:     (INT) The Page Limit (Default: 15)
     *       - page:      (INT) Pages to Load (Default: 1)
     *
     *
     *   Returns (JSON):
     *       - total      (INT) count Awards in result
     *       - data       (JSON) The Awards List
     *
     **/
    public function getPagedAwards( $limit = 15 , $page = 1 ){

        $query  = Award::take( $limit )->skip( ( ( $page - 1 ) * $limit ) );

        $total  = Award::count();
        $data   = $query->get();

        //Return the Data
        return [
            'total' => $total ,
            'data'  => $data
        ];

    }














    /**
     *
     *   createAwards
     *       - create an Awards
     *
     *   Params ($_PUT):
     *       - group_id:         (INT) ID Affiliate Group
     *       - location:         (String) Segment Affiliate null/branch/department/both
     *       - branches:         (ARRAY) id's branches
     *       - departments:      (ARRAY) id's departments
     *       - start:            (Date) The Start Date
     *       - end:              (Date) The End Date
     *       - customers:        (INT) count Customers Required
     *       - name:             (String) The Award's Name
     *       - description:      (String) The Award's Description
     *
     *   Returns (JSON):
     *       1. (Bool) Returns True / False
     *
     **/
    public function createAwards(){

        $data           = Input::all();

        $validator      = Validator::make( $data, [
            'location'               => '',
            'branches'               => '',
            'departments'            => '',
            'start'                  => 'required|'.( Input::get('end') ? 'before:' . Input::get('end') : '' ),
            'end'                    => 'required|'.( Input::get('start') ? 'after:' . Input::get('start') : '' ),
            'customers'              => 'required|min:0',
            'name'                   => 'required|unique:awards,name',
            'description'            => 'required'
        ]);

        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            DB::beginTransaction();

            try {

                //Create the new Award
                $award = Award::create($data);

                $branches = array();
                foreach ($data['branches'] as $branch){
                    $branches[] = $branch['id'];
                }

                $award->branches()->attach($branches);

                $departments = array();
                foreach ($data['departments'] as $department){
                    $departments[] = $department['id'];
                }

                $award->departments()->attach($departments);

                $affiliates  = Affiliate::whereIn('department_id', $departments)
                    ->whereIn('branch_id', $branches)
                    ->get();

                foreach ($affiliates as $affiliate){

                    //Send the Email
                    Mail::send([ 'html' => 'emails.award'] ,  [ 'data' => $data ]  , function($message) use ($affiliate){

                        $affiliate->fullname = ( !empty( $affiliate->firstname ) && !empty( $affiliate->lastname ) ? $affiliate->firstname . ' ' . $affiliate->lastname : null );

                        $message->to( $affiliate->email , $affiliate->fullname );

                        //Set Subject
                        $message->subject( env('COMPANY') . ' New Award: ' . date( 'M d, Y h:ia' ) );

                    });

                }

                DB::commit();
                //Return Success
                return [ 'result' => 1 ];

            } catch(Exception $e ){

                DB::rollback();
                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    }














    /**
     *
     *   editAwards
     *       - edit Awards
     *
     *   Params ($_POST):
     *       - id:               (INT) ID Award
     *       - group_id:         (INT) ID Affiliate Group
     *       - location:         (String) Segment Affiliate null/branch/department/both
     *       - branches:         (ARRAY) id's branches
     *       - departments:      (ARRAY) id's departments
     *       - start:            (Date) The Start Date
     *       - end:              (Date) The End Date
     *       - customers:        (INT) count Customers Required
     *       - name:             (String) The Award's Name
     *       - description:      (String) The Award's Description
     *
     *   Returns (JSON):
     *       1. (Bool) Returns True / False
     *
     **/
    public function editAwards(){

        $data           = Input::all();

        $validator      = Validator::make( $data, [
            'id'                     => 'required|exists:awards,id',
            'location'               => '',
            'branches'               => ( ((Input::get('location')==='both')||(Input::get('location')==='branches')) ? 'required' : '' ),
            'departments'            => ( ((Input::get('location')==='both')||(Input::get('location')==='department')) ? 'required' : '' ),
            'start'                  => 'required|'.( Input::get('end') ? 'before:' . Input::get('end') : '' ),
            'end'                    => 'required|'.( Input::get('start') ? 'after:' . Input::get('start') : '' ),
            'customers'              => 'required|min:0',
            'name'                   => 'required',
            'description'            => 'required'
        ]);

        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            DB::beginTransaction();

            try {

                $award = Award::with('branches','departments')->find($data['id']);

                $collect_branches = collect($data['branches']);

                //delete old branches
                $diff = $award->branches->diffKeys($collect_branches);

                $branches = array();
                foreach ($diff->toArray() as $branch){
                    $branches[] = $branch['id'];
                }

                if(!empty($branches) || ($data['location']==='') || ($data['location']==='department'))
                    $award->branches()->detach($branches);

                //add new branches
                if(($data['location']==='both')||($data['location']==='branch')) {

                    $diff = $collect_branches->diffKeys($award->branches);

                    $branches = array();
                    foreach ($diff->toArray() as $branch) {
                        $branches[] = $branch['id'];
                    }

                    if (!empty($branches))
                        $award->branches()->attach($branches);

                }
                $collect_departments = collect($data['departments']);

                //delete old departments
                $diff = $award->departments->diffKeys($collect_departments);

                $departments = array();
                foreach ($diff->toArray() as $department){
                    $departments[] = $department['id'];
                }

                if(!empty($departments) || ($data['location']==='') || ($data['location']==='branches'))
                    $award->departments()->detach($departments);

                //add new departments
                if(($data['location']==='both')||($data['location']==='department')) {

                    $diff = $collect_departments->diffKeys($award->departments);

                    $departments = array();
                    foreach ($diff->toArray() as $department) {
                        $departments[] = $department['id'];
                    }

                    if (!empty($departments))
                        $award->departments()->attach($departments);
                }

                unset($data['id']);
                unset($data['branches']);
                unset($data['departments']);
                $award->update($data);

                DB::commit();
                //TODO:  add feedback
                //Return Success
                return [ 'result' => 1 ];

            } catch(Exception $e ){

                DB::rollback();
                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    }















    /**
     *
     *   getAwards
     *       - Loads All of the Award
     *
     *   URL Params:
     *       - award_id:       (INT) The Award ID to Lookup
     *
     *
     *   Returns (JSON):
     *       1. The Award Data
     *
     **/
    public function getAwards( $award_id = 1 ){

        return Award::with('branches','departments')->find( $award_id ) ;

    }















    /**
     *
     *   deleteAwards
     *       - Delete an Awards
     *
     *   Params (URL):
     *       - award_id:                   (INT) The Award ID
     *
     *   Returns (JSON):
     *       1. (Bool) Returns True / False
     *
     **/
    public function deleteAwards( $award_id ){


        if( $award = Award::find( $award_id ) ){

            //Delete the Award
            $award->delete();

            //Return Success
            return [ 'result' => 1 ];

        }

        //Return Failed
        return [ 'result' => 0 , 'errors' => [ 'That Award Doesn\'t exist' ] ];

    }



}