<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

use App\Models\Affiliate;
use App\Models\AffiliateGroup;
use App\Models\Attachment;
use App\Models\Branch;
use App\Models\Department;
use App\Models\Award;
use App\Models\Customer;

use Input;
use Validator;
use Auth;
use DB;
use Mail;

use PHPExcel_IOFactory;

class AffiliateController extends Controller {


    /**
     *
     *   getPagedAffiliates
     *       - Loads All of the Affiliates
     *
     *   URL Params:
     *       - limit:     (INT) The Page Limit (Default: 15)
     *       - page:      (INT) Pages to Load (Default: 1)
     *
     *
     *   Returns (JSON):
     *       1. The Affiliates List
     *
     **/
    public function getPagedAffiliates( $limit = 15 , $page = 1 ){

        $query  = Affiliate::with('status','group','department','branch');

        if( $search = Input::get('q') ){

            $keys = explode(" ", $search);

            $query->where(function($query) use ($keys){

                foreach ($keys as $key){

                    $query->where(function($query) use ($key) {

                        $query->orwhere('firstname', 'like', '%' . Str::slug($key) . '%');

                        $query->orwhere('lastname', 'like', '%' . Str::slug($key) . '%');

                    });
                }

            });

            $query->orWhere('id', $search);

        }

        if( $group = Input::get('group') ){

            $query->where( 'affiliate_group_id' , $group );

        }

        $total  = $query->count();
        $data   = $query->take( $limit )->skip( ( ( $page - 1 ) * $limit ) )->orderBy( 'affiliate_status_id' , 'ASC' )->get();
        $affiliate_groups  = AffiliateGroup::get();

        //Loop through the Groups
        foreach( $data as $index => $item ){

            //Set the Status ID
            switch( $item['affiliate_status_id'] ){

                //Active
                case 1:
                    $data[ $index ]['user_status'] = 'Active';
                    break;

                //Removed
                case 2:
                    $data[ $index ]['user_status'] = 'Removed';
                    break;

                //Deleted
                case 3:
                    $data[ $index ]['user_status'] = 'Deleted';
                    break;

            }

        }

        //Return the Data
        return [
            'total'             => $total ,
            'affiliate_groups'  => $affiliate_groups ,
            'data'              => $data
        ];

    }














    /**
     *
     *   attach
     *       - Attaches a File
     *
     *   Request Params:
     *       - file:       (FILE) The File to Attach
     *
     *
     *   Returns (JSON):
     *       result:            0/1 operation is successful
     *       attachment_id:     id new Attachment
     *
     **/
    public function attach(){

        if( Input::file('file') ){

            $Attachment = Attachment::store( Input::file('file') , 'public' );

            return [ 'result' => 1 , 'attachment_id' => $Attachment->id ];

        }

        return [ 'result' => 0 , 'error' => 'Invalid File Uploaded' , 'code' => 'invalid-file' ];

    }














    /**
     *
     *   createAffiliates
     *       - create Affiliates from xls file
     *
     *   Request Params (POST):
     *       - group:       affiliate Group
     *       - file:        file for parse
     *
     *   Returns (JSON):
     *       result:      0/1 operation is successful
     *
     **/
    public function createAffiliates(){

        $request = Input::all();
        $validator      = Validator::make( $request, [
            'group'                      => 'required',
            'files'                      => 'required'
        ]);

        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            try {
                $file  = Attachment::grab( [$request['files'][0]['attachment_id']] , 'public' )->first()->file;

                //create from loaded file
                $objPHPExcel = PHPExcel_IOFactory::load($file);

                $objPHPExcel->setActiveSheetIndex(0);

                $aSheet = $objPHPExcel->getActiveSheet();

                //set 'delete' status all Affiliates
                Affiliate::where('affiliate_status_id', 1)
                        ->update(['affiliate_status_id' => 2]);

                //DB column name => array( XLS column liter, column name )
                $columns = array(
                    'name'          => array(
                        'liter'     => 'A',
                        'name'      => 'Affiliate Name',
                    ),
                    'id'            => array(
                        'liter'     => 'B',
                        'name'      => 'Affiliate Id',
                    ),
                    'branch'        => array(
                        'liter'     => 'C',
                        'name'      => 'Branch',
                    ),
                    'department'    => array(
                        'liter'     => 'D',
                        'name'      => 'Department',
                    ),
                    'payroll_status'=> array(
                        'liter'     => 'E',
                        'name'      => 'Payroll Status',
                    ),
                );

                //walk all rows
                foreach($aSheet->getRowIterator(2) as $row){

                    $row_number = $row->getRowIndex();

                    //validation start
                    //walk cells
                    foreach($columns as $key => $cell){

                        $data[$key] = $objPHPExcel->getActiveSheet()->getCell($cell['liter'].$row_number)->getValue();

                        if(!isset($data[$key]) && ($data[$key] !== '')){

                            $warnings[] = array(
                                'cell'      => $cell['liter'].$row_number,
                                'name'   => 'empty '.$cell['name']
                            );

                        }

                    }

                    if(!isset($data['id']) && ($data['id'] == "")){
                        continue;
                    }

                    $name = explode(", ", $data['name']);
                    if(!isset($name[1])){
                        $name[1] = '';
                    }
                    //validation end

                    $branch = Branch::select('id')->where('name', '=', $data['branch'])->first();
                    $department = Department::select('id')->where('name', '=', $data['department'])->first();

                    if(isset($branch->id)){
                        $branch_id = $branch->id;
                    }else{
                        $branch = Branch::create(array(
                            'name' => $data['branch'],
                        ));
                        $branch_id = $branch->id;
                    }

                    if(isset($department->id)){
                        $department_id = $department->id;
                    }else{
                        $department = Department::create(array(
                            'name' => $data['department'],
                        ));
                        $department_id = $department->id;
                    }

                    if($affiliate = Affiliate::find( $data['id'] )){
                        //reset 'exists' status exists Affiliates
                        $affiliate->update(['affiliate_status_id' => 1]);
                    }else{
                        Affiliate::create(array(
                            'id'                    => $data['id'],
                            'firstname'             => $name[0],
                            'lastname'              => $name[1],
                            'branch_id'             => $branch_id,
                            'department_id'         => $department_id,
                            'affiliate_status_id'   => 1,
                            'affiliate_group_id'    => $request['group'],
                            'payroll_status'        => $data['payroll_status'],
                            'email'                 => $data['id'].'_'.$name[0].'@test.test',
                        ));
                    }

                }

            } catch(Exception $e ){

                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

        return [ 'result' => 1 ];

    }















    /**
     *
     *   deleteAffiliates
     *       - Delete an Affiliates
     *
     *   Params (URL):
     *       - affiliate_id:                   (INT) The Affiliates ID
     *
     *   Returns (JSON):
     *       - result:      0/1 operation is successful
     *
     **/
    public function deleteAffiliates( $affiliate_id ){

        //Make sure the Affiliate Exists
        if( $affiliate = Affiliate::find( $affiliate_id ) ){

            //Set Status as Deleted
            $affiliate->affiliate_status_id = 3;

            //Save the Affiliate
            $affiliate->save();

            //Return Success
            return [ 'result' => 1 ];

        }

        //Return Failed
        return [ 'result' => 0 , 'errors' => [ 'That Affiliate Doesn\'t exist' ] ];

    }















    /**
     *
     *   getAllBranches
     *       - Loads All of the Branches
     *
     *   URL Params:
     *
     *
     *   Returns (JSON):
     *       1. The Branches List
     *
     **/
    public function getAllBranches(  ){

        $query  = Branch::select('id','name');

        $data   = $query->get();

        //Return the Data
        return [
            'data'  => $data
        ];

    }















    /**
     *
     *   getAllDepartments
     *       - Loads All of the Department
     *
     *   URL Params:
     *
     *
     *   Returns (JSON):
     *       1. The Departments List
     *
     **/
    public function getAllDepartments(  ){

        $query  = Department::select('id','name');

        $data   = $query->get();

        //Return the Data
        return [
            'data'  => $data
        ];

    }















    /**
     *
     *   addAwardAffiliate
     *       - add Award to Affiliate
     *
     *   Params ($_PUT):
     *       - id:          (INT) Affiliate ID
     *       - awards:      (ARRAY) Awards to be added
     *
     *
     *   Returns (JSON):
     *       - result:      0/1 operation is successful
     *
     **/
    public function addAwardAffiliate(  ){

        $data           = Input::all();

        $rules = [
            'id'                     => 'required|exists:affiliates,id',
            'from'                   => 'required',
            'to'                     => 'required',
        ];

        if(isset($data['from']) && $data['from']!==0){
            $rules['from'] = 'required|exists:awards,id';
        }

        if(isset($data['to']) && $data['to']!==0){
            $rules['to'] = 'required|exists:awards,id';
        }

        $validator      = Validator::make( $data, $rules);

        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            DB::beginTransaction();

            try {

                //add the new Award to Affiliate
                $affiliate = Affiliate::where( 'id', '=', $data['id'] )->first();
                if($data['from']){
                    $affiliate->awards()->detach($data['from']);
                }
                if($data['to']){

                    $awards = DB::select('
                        SELECT 
                            `awards`.`id`,
                            `awards`.`name`,
                            `awards`.`customers`
                        FROM `awards`,
                             `affiliates`                                          
                            LEFT JOIN  `customers` ON  `affiliates`.`id` = `customers`.`affiliate_id`                                              
                        WHERE  `affiliate_status_id` = 1
                            AND `affiliates`.`id` = :affiliate_id
                            AND `awards`.`id` = :awards_id
                            AND `awards`.`start` <= `customers`.`date`
                            AND `awards`.`end` >= `customers`.`date`
                            AND (
                                CASE WHEN `awards`.`location` = "both" THEN
                                        `affiliates`.`branch_id` IN (
                                            SELECT `award_branches`.`branch_id`
                                            FROM `award_branches`
                                            WHERE `award_branches`.`award_id` = `awards`.`id`
                                        )
                                        AND `affiliates`.`department_id` IN (
                                            SELECT `award_departments`.`department_id`
                                            FROM `award_departments`
                                            WHERE `award_departments`.`award_id` = `awards`.`id`
                                        )
                                    WHEN `awards`.`location` = "branch" THEN
                                        `affiliates`.`branch_id` IN (
                                            SELECT `award_branches`.`branch_id`
                                            FROM `award_branches`
                                            WHERE `award_branches`.`award_id` = `awards`.`id`
                                        )
                                    WHEN `awards`.`location` = "department" THEN
                                        `affiliates`.`department_id` IN (
                                            SELECT `award_departments`.`department_id`
                                            FROM `award_departments`
                                            WHERE `award_departments`.`award_id` = `awards`.`id`
                                        )
                                    ELSE true
                                END
                            )
                        GROUP BY  `awards`.`id`
                        HAVING `awards`.`customers` <= COUNT( `customers`.`id` )
                       ',[
                           'affiliate_id'=>$data['id'],
                           'awards_id'=>$data['to'],
                        ]
                    );

                    //check can we add award to affiliate
                    if(empty($awards))
                        return [ 'result' => 0 , 'errors' => [ 'Invalid award' ] ];

                    $affiliate->awards()->attach($data['to']);
                    $award = Award::find( $data['to'] )->toArray();


                    //Send the Email
                    Mail::send([ 'html' => 'emails.award'] ,  [ 'data' => $award ]  , function($message) use ($affiliate){

                        $affiliate->fullname = ( !empty( $affiliate->firstname ) && !empty( $affiliate->lastname ) ? $affiliate->firstname . ' ' . $affiliate->lastname : null );

                        $message->to( $affiliate->email , $affiliate->fullname );

                        //Set Subject
                        $message->subject( env('COMPANY') . ' New Award: ' . date( 'M d, Y h:ia' ) );

                    });
                }

                DB::commit();
                //Return Success
                return [ 'result' => 1 ];

            } catch(Exception $e ){

                DB::rollback();
                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    }















    /**
     *
     *   getAwardAffiliate
     *       - Loads Affiliate with exist awards and all available awards for Affiliate
     *
     *   URL Params:
     *       - affiliate_id:     (INT) The Affiliate ID
     *
     *
     *   Returns (JSON):
     *       - affiliate:        Affiliate with exist awards
     *       - awards:          All available awards for Affiliate
     *
     **/
    public function getAwardAffiliate( $affiliate_id ){

        $affiliate  = Affiliate::with(array('awards'=>function($query){
            $query->select('awards.id','awards.name');
        }))->where( 'id', '=', $affiliate_id )->first();

        $awards = DB::select('
            SELECT 
                `awards`.`id`,
                `awards`.`name`,
                `awards`.`customers`
            FROM `awards`,
                 `affiliates`                                          
                LEFT JOIN  `customers` ON  `affiliates`.`id` = `customers`.`affiliate_id`                                              
            WHERE  `affiliate_status_id` = 1
                AND `affiliates`.`id` = :affiliate_id
                AND `awards`.`start` <= `customers`.`date`
                AND `awards`.`end` >= `customers`.`date`
                AND (
                    CASE WHEN `awards`.`location` = "both" THEN
                            `affiliates`.`branch_id` IN (
                                SELECT `award_branches`.`branch_id`
                                FROM `award_branches`
                                WHERE `award_branches`.`award_id` = `awards`.`id`
                            )
                            AND `affiliates`.`department_id` IN (
                                SELECT `award_departments`.`department_id`
                                FROM `award_departments`
                                WHERE `award_departments`.`award_id` = `awards`.`id`
                            )
                        WHEN `awards`.`location` = "branch" THEN
                            `affiliates`.`branch_id` IN (
                                SELECT `award_branches`.`branch_id`
                                FROM `award_branches`
                                WHERE `award_branches`.`award_id` = `awards`.`id`
                            )
                        WHEN `awards`.`location` = "department" THEN
                            `affiliates`.`department_id` IN (
                                SELECT `award_departments`.`department_id`
                                FROM `award_departments`
                                WHERE `award_departments`.`award_id` = `awards`.`id`
                            )
                        ELSE true
                    END
                )
            GROUP BY  `awards`.`id`
            HAVING `awards`.`customers` <= COUNT( `customers`.`id` )
           ',['affiliate_id'=>$affiliate_id]);

        for($i=0; $i < count($awards);$i++){
            if(empty($award_ids)){
                $awards[$i]->id =  (int)$awards[$i]->id;
            }else{
                $awards[$i] = (int)$awards[$i]->id;
            }
        }

        return [
            'affiliate'  => $affiliate,
            'awards'    => $awards,
        ];

    }

}