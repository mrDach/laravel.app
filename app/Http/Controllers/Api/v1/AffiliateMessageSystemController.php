<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;

use App\Models\AffiliateMessage;
use App\Models\Affiliate;

use Input;
use Validator;
use Auth;
use DB;
use Mail;

class AffiliateMessageSystemController extends Controller {


    /**
     *
     *   getPagedMessages
     *       - Loads All of the AffiliateMessage
     *
     *   URL Params:
     *       - limit:     (INT) The Page Limit (Default: 15)
     *       - page:      (INT) Pages to Load (Default: 1)
     *
     *
     *   Returns (JSON):
     *       - total      (INT) count Affiliate Messages in result
     *       - data       (JSON) The Affiliate Messages List
     *
     **/
    public function getPagedMessages( $limit = 15 , $page = 1 ){

        $query  = AffiliateMessage::with(['affiliate' => function ($query){
            $query->with('branch','department');
        }]);

        $query->where(function( $query ){
            if( Input::get('start') )       $query->where( 'created_at' , '>=' , Input::get('start') . '00:00:00' );
            if( Input::get('end') )         $query->where( 'created_at' , '<=' , Input::get('end') . '23:59:59' );
        });

        $total = $query->count();

        $data   = $query->take( $limit )->skip( ( ( $page - 1 ) * $limit ) )->get();

        //Return the Data
        return [
            'total' => $total ,
            'data'  => $data
        ];

    }















    /**
     *
     *   createMessages
     *       - create an Affiliate Messages
     *
     *   Params ($_PUT):
     *       - name:             (String) The Message's Name
     *       - description:      (String) The Message's Description
     *
     *   Returns (JSON):
     *       1. (Bool) Returns True / False
     *
     **/
    public function createMessages(){


        $data           = Input::all();
        $validator      = Validator::make( $data, [
            'affiliate.id'      => 'required|exists:affiliates,id',
            'text'              => 'required'
        ]);

        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            DB::beginTransaction();

            try {

                $data['affiliate_id'] = $data['affiliate']['id'];
                unset($data['affiliate']);

                //Create the new Message
                AffiliateMessage::create($data);

                $affiliate = Affiliate::find( $data['affiliate_id'] );

                //Send the Email
                Mail::send([ 'html' => 'emails.message'] ,  [ 'data' => $data ]  , function($message) use ($affiliate){

                    $affiliate->fullname = ( !empty( $affiliate->firstname ) && !empty( $affiliate->lastname ) ? $affiliate->firstname . ' ' . $affiliate->lastname : null );

                    $message->to( $affiliate->email , $affiliate->fullname );

                    //Set Subject
                    $message->subject( env('COMPANY') . ' New Message: ' . date( 'M d, Y h:ia' ) );

                });

                //TODO:  add feedback
                //Return Success
                DB::commit();
                //Return Success
                return [ 'result' => 1 ];

            } catch(Exception $e ){

                DB::rollback();
                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    }



}