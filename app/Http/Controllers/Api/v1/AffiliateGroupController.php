<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

use App\Models\Affiliate;
use App\Models\AffiliateGroup;

use Input;
use Validator;
use Auth;

class AffiliateGroupController extends Controller {


    /**
     *
     *   getPagedGroups
     *       - Loads All of the AffiliateGroup
     *
     *   URL Params:
     *       - limit:     (INT) The Page Limit (Default: 15)
     *       - page:      (INT) Pages to Load (Default: 1)
     *
     *
     *   Returns (JSON):
     *       - total      (INT) count Affiliate Groups in result
     *       - data       (JSON) The Affiliate Groups List
     *
     **/
    public function getPagedGroups( $limit = 15 , $page = 1 ){

        $query  = AffiliateGroup::take( $limit )->skip( ( ( $page - 1 ) * $limit ) );

        $total  = AffiliateGroup::count();
        $data   = $query->get();

        //Return the Data
        return [
            'total' => $total ,
            'data'  => $data
        ];

    }















    /**
     *
     *   getAllGroups
     *       - Loads All of the Affiliates
     *
     *   URL Params:
     *       n/a
     *
     *   Returns (JSON):
     *       - groups   (JSON) The all Affiliate Groups List
     *
     **/
    public function getAllGroups(){

        $affiliate_groups = AffiliateGroup::get()->toArray();

        //Return the Data
        return [
            'groups'    => $affiliate_groups,
        ];

    }















    /**
     *
     *   createGroups
     *       - create an Affiliate Groups
     *
     *   Params ($_PUT):
     *       - name:             (String) The Group's Name
     *       - description:      (String) The Group's Description
     *
     *   Returns (JSON):
     *       1. (Bool) Returns True / False
     *
     **/
    public function createGroups(){


        $data           = Input::all();
        $validator      = Validator::make( $data, [
            'name'                      => 'required|unique:affiliate_groups,name',
            'description'               => 'required'
        ]);

        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            try {

                //Create the new Group
                AffiliateGroup::create($data);

                //TODO:  add feedback
                //Return Success
                return [ 'result' => 1 ];

            } catch(Exception $e ){

                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    }















    /**
     *
     *   deleteGroups
     *       - Delete an Affiliate Groups
     *
     *   Params (URL):
     *       - affiliate_id:                   (INT) The Groups ID
     *
     *   Returns (JSON):
     *       1. (Bool) Returns True / False
     *
     **/
    public function deleteGroups( $group_id ){


        if( $group = AffiliateGroup::find( $group_id ) ){

            //Delete the Store
            $group->delete();

            //Return Success
            return [ 'result' => 1 ];

        }

        //Return Failed
        return [ 'result' => 0 , 'errors' => [ 'That Affiliate Group Doesn\'t exist' ] ];

    }



}