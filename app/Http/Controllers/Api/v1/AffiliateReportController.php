<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Bican\Roles\Models\Permission;

use App\Models\Report;
use App\Models\Affiliate;
use App\Models\Store;
use App\Models\Attachment;
use App\Models\Notification;
use App\Models\User;

use Input;
use Validator;
use Auth;
use Mail;
use DB;

class AffiliateReportController extends Controller {
  




    //Redirect to Root
    protected $redirectPath = '/';




    /**
     *
     *   getPagedReports
     *       - Loads All of the Affiliate
     *
     *   URL Params:
     *       - limit:     (INT) The Page Limit (Default: 15)
     *       - page:      (INT) Pages to Load (Default: 1)
     *
     *
     *   Returns (JSON):
     *       - total      (INT) count Customers in result
     *       - data       (JSON) The Customers List
     *
     **/
    public function getPagedReports( $limit = 15 , $page = 1 ){

        $query  = Affiliate::select('affiliates.*', DB::raw('COUNT(customers.id) as count_customers'), DB::raw('SUM(customers.spent) as sum_spent'))
                    ->where( 'affiliate_status_id' , 1 )
                    ->leftJoin('customers', 'affiliates.id', '=', 'customers.affiliate_id');

        if( $search = Input::get('q') ){

            $query->where(function($query) use ($search){

                $query->where('affiliates.firstname' , 'like' , '%' . Str::slug( $search ) . '%');

                $query->orwhere('affiliates.lastname' , 'like' , '%' . Str::slug( $search ) . '%');

                $query->orWhere('affiliates.id' , $search );

            });

        }

        $query->where(function( $query ){
            if( Input::get('start') )       $query->where( 'date' , '>=' , Input::get('start') . '00:00:00' );
            if( Input::get('end') )         $query->where( 'date' , '<=' , Input::get('end') . '23:59:59' );
        });

        $total = $query->count();

        $query->groupBy('affiliates.id');

        if( $sort = Input::get('sort') ){

            $direction = 'ASC';

            if(($sort ==='count_customers')||($sort ==='sum_spent')) $direction = 'DESC';

            $query->orderBy(DB::raw($sort), $direction);

        }

        $data   = $query->take( $limit )->skip( ( ( $page - 1 ) * $limit ) )->get();



        //Return the Data
        return [
            'total' => $total,
            'data'  => $data,
            'sql' => $query->toSql(),
        ];

    }

}
